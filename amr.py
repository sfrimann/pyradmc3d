#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def _allocate_more(nmore):
  """
  Helper Function.
  Allocates more Array space for AMR tree
  """
  # ------- Set Global Variables
  global namrtree, amrtree
  
  namrtree += nmore
  amrtree   = np.append(amrtree,np.empty(nmore,dtype=np.int8))
  return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def amrsort(r,dx):
  """
  Sorts Ramses AMR data into format that can be read by Radmc3d.
  Note that the outermost rim of the AMR cube is removed since it may not be filled
  Input:
    r       : data coordinates (ndata,3) numpy array
    dx      : cell sizes (ndata) numpy array
  Returns:
    indices : sorted indices for data
    amrtree : amrtree of ones and zeros which is needed for radmc input files 
  """
  # ------- Set Global Variables
  global indices, pos1, pos2, namrtree, amrtree
  
  # ------- min/max in all directions
  amin = np.argmin(r,axis=0)
  amax = np.argmax(r,axis=0)
  
  minx, maxx = r[amin[0],0]-dx[amin[0]]/2, r[amax[0],0]+dx[amax[0]]/2
  miny, maxy = r[amin[1],1]-dx[amin[1]]/2, r[amax[1],1]+dx[amax[1]]/2
  minz, maxz = r[amin[2],2]-dx[amin[2]]/2, r[amax[2],2]+dx[amax[2]]/2

  # ------- grid size and grid edge of a base grid cell
  gs = np.max(dx) # grid size of base grid
  ge = r[np.argmax(dx),:] + gs/2 # grid edges of one of the base grid cells
  
  # ------- Re-calculate min/max in all directions if necessary
  nbxl, nbxr = (ge[0]-minx)/gs, (maxx-ge[0])/gs #nbinx_left, nbinx_right
  nbyl, nbyr = (ge[1]-miny)/gs, (maxy-ge[1])/gs #nbiny_left, nbiny_right
  nbzl, nbzr = (ge[2]-minz)/gs, (maxz-ge[2])/gs #nbinz_left, nbinz_right
  
  minx = ge[0] - gs*np.ceil(nbxl) if abs(nbxl - round(nbxl)) > 1e-10 else minx
  maxx = ge[0] + gs*np.ceil(nbxr) if abs(nbxr - round(nbxr)) > 1e-10 else maxx
  miny = ge[1] - gs*np.ceil(nbyl) if abs(nbyl - round(nbyl)) > 1e-10 else miny
  maxy = ge[1] + gs*np.ceil(nbyr) if abs(nbyr - round(nbyr)) > 1e-10 else maxy
  minz = ge[2] - gs*np.ceil(nbzl) if abs(nbzl - round(nbzl)) > 1e-10 else minz
  maxz = ge[2] + gs*np.ceil(nbzr) if abs(nbzr - round(nbzr)) > 1e-10 else maxz
  
  # ------- number of bins in xyz
  nbinx     = (maxx-minx)/gs # number of bins
  nbiny     = (maxy-miny)/gs
  nbinz     = (maxz-minz)/gs
  
  # ------- Check if nbin(xyz) are whole numbers
  if abs(nbinx - round(nbinx)) > 1e-10 or \
     abs(nbiny - round(nbiny)) > 1e-10 or \
     abs(nbinz - round(nbinz)) > 1e-10:
    print "nbinx, nbiny, nbinz", nbinx, nbiny, nbinz
    raise ValueError("nbin(xyz) must be whole numbers")
  
  # ------- if any nbin less than 3 the area is not big enough and return empty indices
  if any([nbin < 3 for nbin in [nbinx,nbiny,nbinz]]):
    return [], []

  # ------- Bin edges (strip away outer bins since these may not be completely filled)
  binx = np.linspace(minx,maxx,num=int(round(nbinx))+1)[1:-1]
  biny = np.linspace(miny,maxy,num=int(round(nbiny))+1)[1:-1]
  binz = np.linspace(minz,maxz,num=int(round(nbinz))+1)[1:-1]
  
  # ------- Indices that fall into the bins
  ix = np.searchsorted(binx,r[:,0],side='right')
  iy = np.searchsorted(biny,r[:,1],side='right')
  iz = np.searchsorted(binz,r[:,2],side='right')
  
  # ------- Number of data points
  ndata = np.vstack((ix > 0,ix < len(binx),\
                     iy > 0,iy < len(biny),\
                     iz > 0,iz < len(binz))).all(axis=0)
  ndata = ndata.sum()
  
  # ------- Initialise arrays
  indices  = np.empty(ndata,dtype=np.int32)
  indarr   = np.arange(len(dx))
  pos1     = 0L
  pos2     = 0L
  namrtree = round(10000)
  amrtree  = np.empty(namrtree,dtype=np.int8)
  
  # ------- Base loop
  zz,yy,xx = np.mgrid[1:len(binz),1:len(biny),1:len(binx)]
  for iix,iiy,iiz in zip(xx.ravel(),yy.ravel(),zz.ravel()):
    index = (ix == iix) & (iy == iiy) & (iz == iiz)
    count = np.count_nonzero(index)
    if count == 1:
      indices[pos1] = indarr[index]
      amrtree[pos2] = 0
      pos1 += 1; pos2 += 1
      if pos2 == namrtree: _allocate_more(10000)
    elif count > 7:
      amrtree[pos2] = 1
      pos2 += 1
      if pos2 == namrtree: _allocate_more(10000)
      void = _amrsort(r[index,:],dx[index],indarr[index])
    else:
      raise ValueError("Something happened in the AMR levels. Non-Recursive %i" % count)
  
  amrtree = amrtree[0:pos2] # remove unused space in amrtree
  
  # ------- Assert that we've been through all data
  if pos1 != ndata:
    raise ValueError("pos1 and ndata are not equal. Have we been through all data?")
  
  # ------- Return
  return indices, amrtree
  
def _amrsort(r,dx,indarr):
  """
  Helper function for amrsort. Deals with recursive calls
  """
  # ------- Set Global Variables
  global indices, pos1, pos2, namrtree, amrtree

  # ------- min/max in all directions
  amin = np.argmin(r,axis=0)
  amax = np.argmax(r,axis=0)
  
  minx, maxx = r[amin[0],0]-dx[amin[0]]/2, r[amax[0],0]+dx[amax[0]]/2
  miny, maxy = r[amin[1],1]-dx[amin[1]]/2, r[amax[1],1]+dx[amax[1]]/2
  minz, maxz = r[amin[2],2]-dx[amin[2]]/2, r[amax[2],2]+dx[amax[2]]/2

  grid_size = (maxx-minx)/2
  # ------- Bin edges
  binx = np.linspace(minx,maxx,num=3)
  biny = np.linspace(miny,maxy,num=3)
  binz = np.linspace(minz,maxz,num=3)
    
  # ------- Indices that fall into the bins
  ix = np.searchsorted(binx,r[:,0],side='right')
  iy = np.searchsorted(biny,r[:,1],side='right')
  iz = np.searchsorted(binz,r[:,2],side='right')
  
  # ------- Looping
  zz = np.array([1, 1, 1, 1, 2, 2, 2, 2])
  yy = np.array([1, 1, 2, 2, 1, 1, 2, 2])
  xx = np.array([1, 2, 1, 2, 1, 2, 1, 2])
  nndata = len(dx)
  for iix,iiy,iiz in zip(xx,yy,zz):
    index = (ix == iix) & (iy == iiy) & (iz == iiz)
    count = np.count_nonzero(index)
    if count == 1:
      indices[pos1] = indarr[index]
      amrtree[pos2] = 0
      pos1 += 1; pos2 += 1
      if pos2 == namrtree: _allocate_more(10000)
    elif count > 7:
      amrtree[pos2] = 1
      pos2 += 1
      if pos2 == namrtree: _allocate_more(10000)
      void = _amrsort(r[index,:],dx[index],indarr[index])
    else:
      raise ValueError("Something happened in the AMR levels. Recursive %i" % count)

  return 0