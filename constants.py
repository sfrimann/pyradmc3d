#!/usr/bin/env python
# -*- coding: utf-8 -*-

c_in_km_s = 2.998e5
c_in_m_s  = 2.998e8
c_in_um_s = 2.998e14

rsun2cm            = 6.955e10
cm2rsun            = 1/rsun2cm

m2cm               = 100.
cm2m               = 1/m2cm

au_to_cm           = 1.49597871e13
au2cm              = au_to_cm
cm_to_au           = 1./au_to_cm
cm2au              = cm_to_au
m2au               = 6.68458712e-12
au2m               = 1./m2au

parsec_to_cm       = 3.08567758e18
parsec2cm          = parsec_to_cm
pc2cm              = parsec_to_cm
cm_to_parsec       = 1./parsec_to_cm
cm2parsec          = cm_to_parsec
cm2pc              = cm_to_parsec

parsec_to_meter    = 3.08567758e16
parsec2m           = parsec_to_meter
pc2m               = parsec_to_meter
meter_to_parsec    = 1./parsec_to_meter
m2parsec           = meter_to_parsec
m2pc               = meter_to_parsec

parsec_to_au       = 2.06264806e5
parsec2au          = parsec_to_au
pc2au              = parsec_to_au
au_to_parsec       = 1./parsec_to_au
au2parsec          = au_to_parsec
au2pc              = au_to_parsec

solar_mass_to_kg   = 1.9891e30
msun_to_kg         = solar_mass_to_kg
msun2kg            = solar_mass_to_kg
kg_to_solar_mass   = 1./solar_mass_to_kg
kg_to_msun         = kg_to_solar_mass
kg2msun            = kg_to_solar_mass

solar_mass_to_g    = 1.9891e33
msun_to_g          = solar_mass_to_g
msun2g             = solar_mass_to_g
g_to_solar_mass    = 1./solar_mass_to_g
g_to_msun          = g_to_solar_mass
g2msun             = g_to_solar_mass

second_to_year     = 3.16887646e-8
s2yr               = second_to_year
s2kyr              = s2yr/1000
s2Myr              = s2yr/1000000
year_to_second     = 1./second_to_year
yr2s               = year_to_second
kyr2s              = 1/s2kyr
Myr2s              = 1/s2Myr

inch_to_cm         = 2.54
inch2cm            = inch_to_cm
cm_to_inch         = 1./inch_to_cm
cm2inch            = cm_to_inch

radian_to_arcsecond = 206264.806
rad2arcsec          = radian_to_arcsecond
arcsecond_to_radian = 1./rad2arcsec
arcsec2rad          = arcsecond_to_radian
radian_to_degree    = 57.2957795
degree_to_radian    = 1./radian_to_degree
rad2deg             = radian_to_degree
deg2rad             = degree_to_radian

molecular_mass     = 2.37e0
unit_mass_in_g     = 1.660538922e-24
unit_mass_in_kg    = 1.660538922e-27
massdens2numdens   = 1./unit_mass_in_g/molecular_mass
numdens2massdens   = unit_mass_in_g*molecular_mass

h2_mass_frac       = 0.7057 # solar system value
massdens2h2numdens = h2_mass_frac/unit_mass_in_g/2. # assuming all H is molecular
h2numdens2massdens = 2*unit_mass_in_g/h2_mass_frac