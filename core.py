#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from subprocess import call
from shutil import copyfile

import os
import numpy as np
import matplotlib.pyplot as mp
import warnings

try:
  from astropy.io import fits as pyfits
except:
  import pyfits

import constants as co
from amr import amrsort
from imageroutines import readimage, writefitsimage
from tausurfroutines import readtausurf, writefitstausurf
from sedroutines import readsed, writefitssed
from plotutils import plotimage, plotspectrum
from utils import _rotcoord
from readwriteinp import *

# define RadmcError class
class RadmcError(IOError):
  """
  An IOError subclass for issues with running radmc.
  """
  pass

class Radmc3d(object):
  """
  The Radmc3d class is for setting up and working with radmc3d models. Radmc3d uses cgs
  units as standard so unless stated explicitly otherwise it can be assumed that all units
  are cgs. The only exception to this rule are wavelengths which are in micron by default.
  The object is initialised as:
  
  >>> import pyradmc3d as pyrad
  >>> radmc = pyrad.Radmc3d(phys_model,modeldir='radmc3d_model',**keywords)
  
  which builds a radmc3d model in the directory radmc3d_model.
  
  The variable phys_model is a pyradmc3d model object, itself containing several attributes 
  and methods used in building the radmc3d models. See the models.py file for exampels of 
  how to construct such a model.
  
  Calling Radmc3d for the first time creates a lot of files in the model directory. These 
  are:
    - amr_grid.inp or .binp         :
    - aperture_info.inp             : (only if aperture is True)
    - dust_density.inp or .binp     :
    - dust_temperature.dat or .bdat : (not yet implemented as input)
    - dustkappa_*.inp               :
    - dustopac.inp                  :
    - external_source.inp           : (only if isrf is True)
    - gas_temperature.inp or .binp  : (only if include_lines is True, not yet implemented)
    - gas_velocity.inp or .binp     : (only if include_lines is True)
    - microturbulence.inp or .binp  : (not yet implemented)
    - lines.inp                     : (only if include_lines is True)
    - log.fits                      :
    - molecule_*.inp                : (only if include_lines is True)
    - numberdens_*.inp or .binp     : (only if include_lines is True)
    - radmc3d.inp                   :
    - stars.inp                     :
    - wavelength.micron.inp         :
  
  If you have previously created a pyradmc3d model in a directory (e.g. radmc3d_model)
  you can retrieve this model by initialising the Radmc3d object with no model object:
  
  >>> radmc = pyrad.Radmc3d(modeldir='radmc3d_model',**keywords)
  
  Methods:
  The object contains a number of methods which are used to work with the radmc3d model
    mctherm         :
    image           :
    continuum_image :
    line_image      :
    sedspectrum     :
    sed             :
    spectrum        :
    tausurf         :
    readimage       :
    readsed         :
    readspectrum    :
  
  Attributes:
  The object has a number of attributes which are needed by its methods
    lexe           : Name of the radmc3d executable which needs to be in the system path
    modeldir       : Path to the radmc3d model directory
    grid_size      : Some representative length scale for a single cell size
    minx, maxx     : Grid boundaries in x-direction measured in AU
    miny, maxy     : Grid boundaries in y-direction measured in AU
    minz, maxz     : Grid boundaries in z-direction measured in AU
    mol_names      : List of molecule names (only if present)
    mol_spec_files : List of molecule_*.inp files (only if this list was generated earlier)
    sizeau         : Size scale of entire model in AU
    
  Input:
    phys_model   : model object. See models.py for examples on how to build this
  
  Keywords:
    modeldir      : Path to the radmc3d model (default: 'radmc3d_model')
    datadir       : Alias for modeldir
    lexe          : Name of radmc3d executable (default: 'radmc3d').
    binary        : If True binary files (.binp and .bdat) are written instead of ascii 
                    files (.inp and .dat). Binary files are quicker to read and only take 
                    up a fraction of the space. Ascii files are easier to debug as they 
                    are readable by humans. (Default: True)
    overwrite     : If True any model already existing in the model directory is removed 
                    and an entirely new model put in place. If a model is already present 
                    in modeldir and overwrite is False an exception is thrown (Defaut: 
                    False)
    use_existing  : If True and a model is already existing in modeldir then the old model
                    is updated. For example if only dust modelling files were written the 
                    first time, line modelling files can be added to the model by setting
                    use_existing to True. Also, if the physical model is MHD simulation
                    then amr_grid.inp and dust_density.inp may not change between
                    individual models. Since amr_grid.inp and dust_density.inp can be
                    expensive to generate it makes sense to copy they from an old radmc3d
                    model and generate the remaining model files anew. If both overwrite
                    and use_existing are True use_existing takes precedence (Default:
                    False)
    radmc3d_dict  : Dictionary with key value pairs for radmc3d.inp file not included in
                    normal keywords. (Default: {})
    nphot         : Number of photons to use for mctherm method
    opac          : Dust opacity names. This can either refer to one of the
                    dustkappa_**.inp files located in the datafiles directory in the
                    package directory, where ** refers to the keyword name. Alternatively
                    it can be the path to a user defined dustkappa_**.inp file located
                    elsewhere on the user's system. This input can be a single string in
                    which case one dust species is assumed, or it can be a sequence of
                    strings, where each entry correspond to a dust species. This keyword
                    can also be an attribute of the phys_model object, which then takes
                    precedence. (Default: 'oh5')
    include_lines : If True files for line radiative transfer are written as well as files
                    for dust radiative transfer. (Default: False)
    molecules     : Similar to opac keyword but for molecules. Note that so far only
                    files with input style like the Leiden Atomic and Molecular Database
                    (LAMDA) http://home.strw.leidenuniv.nl/~moldata/, are accepted.
                    (Default: 'c18o')
    lines_mode    : Line radiative transfer mode. Currently only LTE is supported.
                    (Default: 'lte')
    tgas_eq_tdust : If True gas and dust temperatures are assumed to be equal and only
                    dust_temperature.dat or .bdat is needed
    lamb          : Wavelength grid. Should be monotonically increasing array of
                    wavelengths. If not explicitly set a standard grid is used.
                    (Default: None)
    isrf          : If True an interstellar radiation field (ISRF) like that of Black 
                    (1994) is included in the model. (Default: False)
    aperture      : If True a file defining a circular aperture with a diameter of 15as is
                    copied to the modeldir. To use the circular aperture you have to set
                    it explicitly when calculating a sed/spectrum. (Default: False)
  Returns:
    radmc         : radmc3d model object to interact with radiative transfer model
  """
  def __init__(self,*args,**kw):
    # ------- Handle Arguments
    if len(args) == 0:
      new_model = False
    elif len(args) == 1:
      model     = args[0]
      new_model = True
    else:
      raise IOError("Radmc3d -- Wrong number of inputs")
  
    # ------- Handle Keywords
    datadir       = kw.get('datadir','radmc3d_model') # alias
    modeldir      = kw.get('modeldir',datadir)        # name of model directory
    lexe          = kw.get('executable','radmc3d')    # name of radmc3d executable
    binary        = kw.get('binary',True)             # write binary files instead of ascii files
    overwrite     = kw.get('overwrite',False)         # overwrite old model
    use_existing  = kw.get('use_existing',False)      # If true uses existing model directory, and files already in it (useful if you want to make a second model of a radmc3d system, but with some parameters changed)
    radmc3d_dict  = kw.get('radmc3d_dict',{})         # dictionary with additional options for radmc3d.inp file
    nphot         = kw.get('nphot',1000000)           # number of photons for radmc3d mctherm
    opac          = kw.get('opac','oh5')              # dust opacity
    include_lines = kw.get('include_lines',False)     # include lines or not
    molecules     = kw.get('molecules','c18o')        # molecules
    lines_mode    = kw.get('lines_mode','lte')        # only 'lte' support so far
    tgas_eq_tdust = kw.get('tgas_eq_tdust',True)      # assume dust and gas temperatures are the same
    lamb          = kw.get('lamb',None)               # list of wavelengths (if None, a standard table is used)
    isrf          = kw.get('isrf',False)              # include interstellar radiation field
    aperture      = kw.get('aperture',False)          # include aperture file for creation of SEDs
    stellarflux   = kw.get('stellarflux',None)        # the flux of a radiating object in the model, with
                                                      # wavelength grid that matches the input opacity.
    
    # ------- Handle Model Directory
    modeldir = os.path.abspath(os.path.expanduser(modeldir))
    model_exists = os.path.isdir(modeldir)
    curdir_is_modeldir = modeldir == os.getcwd()
    if model_exists and new_model and overwrite and use_existing is False:
      if curdir_is_modeldir:
        raise IOError("Current directory and model directory are the same. Cannot overwrite current directory")
      call(['rm','-r',modeldir])
    if model_exists is True and new_model is True and overwrite is False and use_existing is False:
      raise IOError("Model directory already exists %s" % modeldir)
    elif model_exists is False and new_model is False:
      raise IOError("Cannot find model directory %s" % modeldir)
    
    if new_model is True:
      # ------- create model directory
      if use_existing is False or model_exists is False:
        void = os.mkdir(modeldir)
      
      # ------- location of datafiles
      moduledir, _ = os.path.split(__file__)
      dataFileDir = os.path.join(moduledir,'datafiles')
      
      # -----------------------------------------------
      # ------- Start Writing radmc3d input files -----
      # -----------------------------------------------
      
      # ------- Write radmc3d.inp
      radmc3d_dict['nphot']        = nphot
      radmc3d_dict['iranfreqmode'] = 1
      radmc3d_dict['rto_style']    = 3 if binary else 1
      if include_lines:
        if lines_mode not in ['lte', 1, -1]:
          raise NotImplementedError("Only LTE line radiative transfer implemented so far")
        radmc3d_dict['lines_mode'] = 1 if lines_mode == 'lte' else lines_mode
        radmc3d_dict['tgas_eq_tdust'] = 1 if tgas_eq_tdust else 0
      mode = 'update' if os.path.isfile(os.path.join(modeldir,'radmc3d.inp')) else 'new'
      radmc3d_dict = write_radmc3d_inp(modeldir,radmc3d_dict,mode=mode)
      
      # ------- write amr_grid.inp or amr_grid.binp      
      if not (os.path.isfile(os.path.join(modeldir,'amr_grid.inp')) or os.path.isfile(os.path.join(modeldir,'amr_grid.binp'))):
        ndata = write_amr_grid(modeldir,model,binary=binary)
      else:
        raw_grid = read_amr_grid(modeldir=modeldir,returnraw=True)
        if raw_grid['gridstyle'] == 1:
          ndata = raw_grid['amrtree'].size - raw_grid['amrtree'].sum()
        else:
          ndata = raw_grid['nx']*raw_grid['ny']*raw_grid['nz']
      
      # ------- copy dustkappa_*.inp to modeldir and write dustopac.inp
      if not os.path.isfile(os.path.join(modeldir,'dustopac.inp')):
        dust_opacs = model.opac if hasattr(model,'opac') else opac # use opac keyword or opac from model
    
        # check if dust_opacs is a string or iterable, and if it is a string make it iterable
        if isinstance(dust_opacs,str):
          dust_opacs = [dust_opacs] # if not not already an iterable make it so
    
        dust_opac_files = []
        for dust_opac in dust_opacs:
          if os.path.isfile(dust_opac):
            dust_opac_files.append(dust_opac)
          elif os.path.isfile(os.path.join(dataFileDir,'dustkappa_%s.inp' % dust_opac)):
            dust_opac_files.append(os.path.join(dataFileDir,'dustkappa_%s.inp' % dust_opac))
          else:
            raise IOError("core.py -- Cannot find opacity file belonging to opacity keyword %s" % dust_opac)
    
        dust_names = write_dust_opacity(modeldir,dust_opac_files)
      else:
        dust_names = read_dustopac(modeldir)
    
      # ------- Write dust_density.inp or .binp
      if not (os.path.isfile(os.path.join(modeldir,'dust_density.inp')) or os.path.isfile(os.path.join(modeldir,'dust_density.binp'))):
        ndata_dust, ndust_dust = write_dust_density(modeldir,model,binary=binary)
      else:
        dust_density = read_dust_density(modeldir=modeldir)
        shape = dust_density.shape
        ndust_dust = 1 if len(shape) == 1 else shape[0]
        ndata_dust = shape[0] if len(shape) == 1 else shape[1]
      
      assert ndust_dust == len(dust_names), "number of dust species in dust_density.inp and dustopac.inp do not match %i %i" % (ndust_dust, len(dust_names))
      assert ndata_dust == ndata, "number of data points in dust_density.inp and amr_grid.inp do not match %i %i" % (ndata_dust, ndata)
      
      if include_lines:
        # ------- copy and write lines.inp
        if not os.path.isfile(os.path.join(modeldir,'lines.inp')):
          mol_specs = model.molecules if hasattr(model,'molecules') else molecules # use molecules keyword or molecules from model
    
          # check if mol_specs is a string or iterable, and if it is a string make it iterable
          if isinstance(mol_specs,str):
            mol_specs = [mol_specs] # if not not already an iterable make it so
    
          mol_spec_files = []
          for mol_spec in mol_specs:
            if os.path.isfile(mol_spec):
              mol_spec_files.append(mol_spec)
            elif os.path.isfile(os.path.join(dataFileDir,'molecule_%s.inp' % mol_spec)):
              mol_spec_files.append(os.path.join(dataFileDir,'molecule_%s.inp' % mol_spec))
            else:
              raise IOError("core.py -- Cannot find molecule file belonging to molecule keyword %s" % mol_spec)
          
          mol_names = write_lines_inp(modeldir,mol_spec_files)
        else:
          molecule_list = read_lines_inp(modeldir)
          mol_names = [m['molname'] for m in molecule_list]

        # ------- numderdens_*molecule*.inp
        if not hasattr(model,'get_gas_abundance'):
          raise AttributeError("Radmc3d() -- model object does not have attribute 'get_gas_abundance'")
        for mol_name in mol_names:
          if not (os.path.isfile(os.path.join(modeldir,'numberdens_%s.inp' % mol_name)) or os.path.isfile(os.path.join(modeldir,'numberdens_%s.binp' % mol_name))):
            numberdens = model.get_gas_abundance(mol_name)
            write_molecule_density(modeldir,mol_name,numberdens,binary=binary)
          else:
            numberdens = read_molecule_density(modeldir,mol_name)
            assert ndata == numberdens.size, "number of data points in numberdens_%s.inp and amr_grid.inp do not match %i %i" % (ndata,numberdens.size)

        # ------- gas_velocity.inp
        if not (os.path.isfile(os.path.join(modeldir,'gas_velocity.inp')) or os.path.isfile(os.path.join(modeldir,'gas_velocity.binp'))):
          write_gas_velocity(modeldir,model,binary=binary)
      
      # ------- Write wavelength_micron.inp
      # Setup wavelength grid
      if lamb is None:
#         lamb1 = 0.1*(7.0/0.1)**(np.arange(20)/20.)
#         lamb2 = 7.0*(25./7.0)**(np.arange(100)/100.)
#         lamb3 = 25.*(1.e4/25)**(np.arange(30)/29.)
        lamb1 = np.logspace(np.log10(0.01),np.log10(7),num=70,endpoint=False)
        lamb2 = np.logspace(np.log10(7),np.log10(25),num=100,endpoint=False)
        lamb3 = np.logspace(np.log10(25),np.log10(1e4),num=70,endpoint=True)
        lamb  = np.hstack((lamb1,lamb2,lamb3))
      nlam = len(lamb)
      if not os.path.isfile(os.path.join(modeldir,'wavelength_micron.inp')):
        with open(os.path.join(modeldir,'wavelength_micron.inp'),'w') as f:
          print >> f, len(lamb) # nlambda
          np.savetxt(f,lamb)
      
      # ------- Write stars.inp
      if not os.path.isfile(os.path.join(modeldir,'stars.inp')):
        write_stars(modeldir,model,lamb,flux=stellarflux)
      
      # ------- Write external_source.inp
      if isrf is True and not os.path.isfile(os.path.join(modeldir,'external_source.inp')):
        write_external_source(modeldir,lamb)
      elif os.path.isfile(os.path.join(modeldir,'external_source.inp')):
        isrf = True
        
      # ------- aperture_info.inp
      if aperture is True and not os.path.isfile(os.path.join(modeldir,'aperture_info.inp')):
        write_aperture_info(modeldir,[lamb[0],lamb[-1]],[7.5,7.5]) # hardcoded diameter of 15 arcsec so far
      
      # ------- Calculations for log.fits
      minx, maxx = model.minx*co.cm2au, model.maxx*co.cm2au
      miny, maxy = model.miny*co.cm2au, model.maxy*co.cm2au
      minz, maxz = model.minz*co.cm2au, model.maxz*co.cm2au
      
      grid_size = model.grid_size*co.cm2au
      
      xsize = (maxx-minx)
      ysize = (maxy-miny)
      zsize = (maxz-minz)
      
      # ------- logfile primary header
      p_header = []
      p_header.append(('modelnam', model.modelname      , "Name of model"))
      p_header.append(('ndata'   , ndata                , "Number of data points"))
      
      p_header.append(('nphot'   , radmc3d_dict['nphot'], "Number of photons used for radmc3d mctherm"))
      for i,dust_name in enumerate(dust_names):
        p_header.append(('dustsp%i' % (i+1) , dust_name , "Name of dust species"))
      
      if include_lines:
        p_header.append(('linemode', radmc3d_dict['lines_mode'], "Line mode. See radmc3d manual for more"))
        p_header.append(('tg_eq_td', bool(radmc3d_dict['tgas_eq_tdust']), "Tgas == Tdust"))
        for i,mol_name in enumerate(mol_names):
          p_header.append(('molsp%i' % (i+1) , mol_name , "Molecular species"))
        
      p_header.append(('sizeau_x', xsize                , "Size of cutout x measured in AU"))
      p_header.append(('sizeau_y', ysize                , "Size of cutout y measured in AU"))
      p_header.append(('sizeau_z', zsize                , "Size of cutout z measured in AU"))
      
      p_header.append(('minx'    , minx                 , "Minimum x coordinate measured in AU"))
      p_header.append(('maxx'    , maxx                 , "Maximum x coordinate measured in AU"))
      p_header.append(('miny'    , miny                 , "Minimum y coordinate measured in AU"))
      p_header.append(('maxy'    , maxy                 , "Maximum y coordinate measured in AU"))
      p_header.append(('minz'    , minz                 , "Minimum z coordinate measured in AU"))
      p_header.append(('maxz'    , maxz                 , "Maximum z coordinate measured in AU"))
      
      p_header.append(('lambmin' , lamb[0]              , "Minimum stellar wavelength (micrometer)"))
      p_header.append(('lambmax' , lamb[-1]             , "Maximum stellar wavelength (micrometer)"))
      p_header.append(('nlam'    , nlam                 , "Number of wavelengths"))
      
      p_header.append(('isrf'    , isrf                 , "Interstellar Radiation Field"))
      p_header.append(('modeldir', modeldir))
      
      if not os.path.isfile(os.path.join(modeldir,'log.fits')):
        # ------- Write log file
        hdu = pyfits.PrimaryHDU(None)
        
        for header in p_header:
          if len(header) == 2:
            key, value = header
            hdu.header[key] = value
          elif len(header) == 3:
            key, value, comment = header
            hdu.header[key] = value
            hdu.header.comments[key] = comment
        
        if hasattr(model,'extraheader'):
          for header in model.extraheader:
            if len(header) == 2:
              key, value = header
              hdu.header[key] = value
            elif len(header) == 3:
              key, value, comment = header
              hdu.header[key] = value
              hdu.header.comments[key] = comment
            else:
              raise ValueError("extra header must consist of key, value og key, value, comment.")
        
        # sink table
        c1  = pyfits.Column(name='sink_number',format='I',array=model.sic)
        c2  = pyfits.Column(name='on/off',format='L',array=model.onList)
        c3  = pyfits.Column(name='xpos',unit='AU',format='D',array=model.positionList[:,0]*co.cm2au)
        c4  = pyfits.Column(name='ypos',unit='AU',format='D',array=model.positionList[:,1]*co.cm2au)
        c5  = pyfits.Column(name='zpos',unit='AU',format='D',array=model.positionList[:,2]*co.cm2au)
        c6  = pyfits.Column(name='sink_mass',unit='Msun',format='D',array=model.massList*co.g2msun)
        c7  = pyfits.Column(name='sink_age',unit='kyr',format='D',array=model.ageList*co.s2kyr)
        c8  = pyfits.Column(name='luminosity',unit='Lsun',format='D',array=model.luminosityList)
        c9  = pyfits.Column(name='temperature',unit='K',format='D',array=model.temperatureList)
        c10 = pyfits.Column(name='logdmdt',unit='log(Msun/yr)',format='D',array=model.logdmdtList+np.log10(co.g2msun/co.s2yr))
        
        #fitstable = pyfits.new_table([c1,c2,c3,c4,c5,c6,c7,c8,c9,c10]) # new_table has deprecation warning in pyfits
        fitstable = pyfits.BinTableHDU.from_columns([c1,c2,c3,c4,c5,c6,c7,c8,c9,c10])
        
        hdulist = pyfits.HDUList([hdu,fitstable])
        hdulist.writeto(os.path.join(modeldir,'log.fits'))
      else: # update header
        hdu = pyfits.open(os.path.join(modeldir,'log.fits'),mode='update')
      
        for header in p_header:
          if len(header) == 2:
            key, value = header
            # do not update following keys
            if key in ['modelnam','sizeau_x','sizeau_y','sizeau_z','minx','maxx','miny','maxy','minz','maxz']:
              continue
            hdu[0].header[key] = value
          elif len(header) == 3:
            key, value, comment = header
            # do not update following keys
            if key in ['modelnam','sizeau_x','sizeau_y','sizeau_z','minx','maxx','miny','maxy','minz','maxz']:
              continue
            hdu[0].header[key] = value
            hdu[0].header.comments[key] = comment
        
        hdu.flush()
        hdu.close()
      
    if new_model is False:
    
      # ------- Read log file
      header = pyfits.getheader(os.path.join(modeldir,'log.fits'))
      
      if 'molsp1' in header: # get molecule names (if present)
        mol_names = [header['molsp1']]
        sp = 2
        while 'molsp%i' % sp in header:
          mol_names.append(header['molsp%i'] % sp)
          sp += 1
      
      grid_size = header['gridsize']
      xsize  = header['sizeau_x']
      ysize  = header['sizeau_y']
      zsize  = header['sizeau_z']
      if 'minx' in header:
        minx   = header['minx']
        maxx   = header['maxx']
        miny   = header['miny']
        maxy   = header['maxy']
        minz   = header['minz']
        maxz   = header['maxz']
    
    # ------- Globally available variables
    self.mol_names = mol_names if 'mol_names' in locals() else -1
    self.mol_spec_files = mol_spec_files if 'mol_spec_files' in locals() else -1
    
    self.lexe     = lexe     # name of executable
    self.modeldir = modeldir # model directory
    self.grid_size = grid_size
    if 'minx' in locals():
      self.minx = minx
      self.maxx = maxx
      self.miny = miny
      self.maxy = maxy
      self.minz = minz
      self.maxz = maxz
      self.sizeau = 2*min(abs(minx),abs(maxx),abs(miny),abs(maxy),abs(minz),abs(maxz)) # size of cut-out in AU
    else:
      self.sizeau   = min(xsize,ysize,zsize) # size of cut-out in AU
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
  def line_model_setup(self,model,**kw):
    """
    Set-up line radiative transfer model
    """
    
    # ------- Handle Keywords
    molecule    = kw.get('molecule','c18o') # name of molecule
#     method      = kw.get('method','lte')    # line mode
    overwrite   = kw.get('overwrite',False) # overwrite old model
    use_existing = kw.get('use_existing',False) # If true uses existing model directory, and files already in it (useful if you want to make a second model of a radmc3d system, but with some parameters changed)
    binary       = kw.get('binary',True) # write binary files instead of ascii files
    
    nmol = 1
    
    # ------- radmc3d.inp
    if not os.path.isfile(os.path.join(self.modeldir,'radmc3d.inp')):
      raise IOError("Cannot find radmc3d.inp file")
    radmc3d_dict = read_radmc3d_inp(self.modeldir)
    radmc3d_dict['tgas_eq_tdust'] = 1 # Tgas == Tdust
    radmc3d_dict['lines_mode'] = 1    # LTE
    void = write_radmc3d_inp(self.modeldir,radmc3d_dict)
    
    # ------- Line Files
    if os.path.isfile(os.path.join(self.modeldir,'lines.inp')) and not (overwrite or use_existing):
      raise IOError("File lines.inp already exists in modeldir %s" % self.modeldir)
    if not os.path.isfile(os.path.join(self.modeldir,'lines.inp')):
      void = write_lines_inp(self.modeldir,molecule,nmol)
      self.molecule = molecule
    else:
      molecule_list = read_lines_inp(self.modeldir)
      self.molecule = molecule_list[0]['molname']

    # ------- numderdens_*molecule*.inp
    if (os.path.isfile(os.path.join(self.modeldir,'numberdens_%s.inp' % molecule)) or os.path.isfile(os.path.join(self.modeldir,'numberdens_%s.binp' % molecule))) and not (overwrite or use_existing):
      raise IOError("File numberdens_*molecule*.inp or .binp already exists in modeldir %s" % self.modeldir)
    if not (os.path.isfile(os.path.join(self.modeldir,'numberdens_%s.inp' % molecule)) or os.path.isfile(os.path.join(self.modeldir,'numberdens_%s.binp' % molecule))):
      void = write_molecule_density(self.modeldir,molecule,model,binary=binary)

    # ------- gas_velocity.inp
    if (os.path.isfile(os.path.join(self.modeldir,'gas_velocity.inp')) or os.path.isfile(os.path.join(self.modeldir,'gas_velocity.binp'))) and not (overwrite or use_existing):
      raise IOError("File gas_velocity.inp or .binp already exists in modeldir %s" % self.modeldir)
    if not (os.path.isfile(os.path.join(self.modeldir,'gas_velocity.inp')) or os.path.isfile(os.path.join(self.modeldir,'gas_velocity.binp'))):
      void = write_gas_velocity(self.modeldir,model,binary=binary)

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-  
  def mctherm(self,**kw):
    """
    This method runs 'radmc3d mctherm' which calculates the dust temperature through Monte
    Carlo simulation
    Input:
      None
    Keywords:
      overwrite : If true any already existing dust_temperature.dat or .bdat file is
                  replaced (Default: False)
      silent    : If True the output radmc3d prints to screen is not shown. (Default: False)
    Returns:
      None
    """
    # -------- Handle Keywords
    overwrite = kw.get('overwrite',False)
    silent    = kw.get('silent',False)
    
    current_dir = os.getcwd()   # Current directory
    os.chdir(self.modeldir)     # Change directory to modeldir
    
    try:
      file_exists = os.path.isfile('dust_temperature.dat') or os.path.isfile('dust_temperature.bdat')
      if file_exists is True and overwrite is True:
        if os.path.isfile('dust_temperature.dat'):
          call(['rm','dust_temperature.dat'])
        else:
          call(['rm','dust_temperature.bdat'])
      if file_exists is True and overwrite is False:
        raise IOError("dust_temperature.dat or dust_temperature.bdat already exists.")
      
      # ------- run radmc3d mctherm
      if silent is True: # if silent suppress output
        with open(os.devnull, 'w') as fnull:
          void = call([self.lexe,'mctherm'], stdout=fnull)
      else:
        void = call([self.lexe,'mctherm'])
      
      file_exists = os.path.isfile('dust_temperature.dat') or os.path.isfile('dust_temperature.bdat')
      
      if not file_exists:
        raise IOError("Apparently mctherm didn't produce dust_temperature.dat or dust_temperature.bdat")
    finally:
      os.chdir(current_dir) # change back to old directory nomatter what
    
    return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
  def line_image(self,**kw):
    """
    This method is a wrapper for image method, and sets some default values when making
    line images. Specifically 'noline' keyword is always False. 'lamb' is always None.
    'nostar' and 'nodust' are True by default. 'iline' is 2 by default. All other
    keywords are as in the image method.
    """
    # ------- Handle Keywords
    nostar      = kw.get('nostar',True)          # don't plot the central object
    nodust      = kw.get('nodust',True)          # don't include dust emission in image
    iline       = kw.get('iline',2)              # Line J iline -> iline-1
    
    kw['noline']    = False
    kw['lamb']      = None
    kw['nostar']    = nostar
    kw['nodust']    = nodust
    kw['iline']     = iline
  
    return self.image(**kw)
  
  def continuum_image(self,**kw):
    """
    This method is a wrapper for image method, and sets some default values for making
    continuum images. Specifically 'nodust' keyword is always False, and 'iline' is always
    None. 'nostar' is False by default, and 'noline' is True by default. 'lamb' is 850
    micron by default. All other keywords are as in image method.
    """
    
    # ------- Handle Keywords
    lamb        = kw.get('lamb',850.)            # Wavelength in um
    nostar      = kw.get('nostar',False)         # don't plot the central object
    noline      = kw.get('noline',True)          # don't include line emission in image
    
    kw['nodust'] = False
    kw['iline']  = None
    kw['lamb']   = lamb
    kw['noline'] = noline
    kw['nostar'] = nostar
    
    return self.image(**kw)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
  def image(self,**kw):
    """
    This method runs 'radmc3d image' command. The output of this command is a file
    image.out, which is then typically read and saved as a fits file (although this is not
    mandatory either).
    Input:
      None
    Keywords:
      incl        : These three angle gives the orientation of the image relative to the
      phi           model. See radmc3d manual for a description on how these angles are
      posang        defined. (Default values for all angles: 0)
      dir         : Predefined orientations. Possibilities are ['x'|'y'|'z'|'-x'|'-y'|'-z'
                    |'edge'|'face']. The first six possibilites correspond to looking in
                    along the cartesian axes, either from the positive side, og negative
                    side. 'edge' and 'face' correspond to edge- and face-on directions
                    to a disk. If any of these are chosen, the rotation axis of the system
                    needs to be set using the omega keyword. If dir is set, any values of
                    incl, phi, and posang are overwritten. (Default: None)
      omega       : xyz vector giving the rotation axis of the system.
      sizeau      : size in au of the cross-section of the model. If not given the size
                    of the entire model is used. (Default: None)
      npix        : Number of pixels along the spatial axes. If not given a value is
                    calculated based on the size of the cross section and the size of the
                    cells in the model grid.
      filename    : filename of the output image. The file extension can be either .out or
                    .fits. If no extension is given .fits is assumed. The image is saved
                    in the model directory. (Default: 'image.fits')
      overwrite   : If an image with same filename is already present and overwrite is
                    True this image is overwritten. (Default: False)
      silent      : If True suppresses output written to terminal by radmc3d. (Default: 
                    False)
      secondorder : If True uses secondorder integration. Be careful that in cases where
                    an optically thin cell is next to an optically thick cell this may
                    cause problems with flux conservation. (Default: True)
      lamb        : Wavelength of image in micron. This keyword is mostly useful when
                    making continuum images. For line images it is typically better to
                    use 'iline' keyword. (Default: None)
      iline       : Rotational transition for plotting line emission. The rotational
                    transition plotted is J iline -> iline-1 -- that is, the value of
                    iline specifies the upper quantum number of the transition. If neither
                    'lamb' nor 'iline' is set, the program will set 'lamb' to 850. If both
                    'lamb' and 'iline' is set, and exception is thrown. If 'iline' is not 
                    set all other line related keywords are ignored. (Default: None)
      imolspec    : If several molecules are defined in lines.inp then this keyword
                    specifies which one is should be used. (Default: 1)
      widthkms    : Spectral window in km/s. The spectral window goes from -widthkms to
                    widthkms. (Default: 5)
      linenlam    : Number of wavelength points to use in widthkms spectral window.
                    (Default: 51)
      vkms        : Offset from line centre in km/s. (Default: 0)
      doppcatch   : If True doppler catching method is used. This automatically means that
                    secondorder is also set. See radmc3d manual for more information. 
                    (Default: False)
      nostar      : Depending on what kind of image one wishes to produce it can be useful
      noline        not to include various components. Using these keywords one can turn
      nodust        emission from dust, lines, or star. (Default: all False)
    Returns:
      None
    """
    # ------- Handle Keywords
    lamb        = kw.get('lamb',None)            # Wavelength in um
    incl        = kw.get('incl',0.)              # Inclination
    phi         = kw.get('phi',0.)               # Phi
    posang      = kw.get('posang',0.)            # Position angle
    dir         = kw.get('dir',None)             # Predefined projection direction
    omega       = kw.get('omega',None)           # rotation axis
    sizeau      = kw.get('sizeau',None)          # Size of cut-out in AU (default is data range)
    npix        = kw.get('npix',None)            # size of image
    filename    = kw.get('filename','image.fits')# Image file
    overwrite   = kw.get('overwrite',False)      # Overwrite old images
    silent      = kw.get('silent',False)         # suppress output to prompt
    secondorder = kw.get('secondorder',True)     # use secondorder integration in radmc3d
    
    iline       = kw.get('iline',None)           # Line J iline -> iline-1
    imolspec    = kw.get('imolspec',1)           # Molecule to use (default is the first molecule)
    widthkms    = kw.get('widthkms',5)           # width of window in km/s
    linenlam    = kw.get('linenlam',51)          # Number of wavelength points
    vkms        = kw.get('vkms',0.)              # Offset from line centre in km/s
    doppcatch   = kw.get('doppcatch',False)      # use doppler catching
    
    nostar      = kw.get('nostar',False)         # don't plot the central object
    noline      = kw.get('noline',False)         # don't include line emission in image
    nodust      = kw.get('nodust',False)         # don't include dust emission in image
    
    # -------- Handle directories
    current_dir = os.path.abspath('.')
    os.chdir(self.modeldir)
    
    try:
      # -------- Sanity Checks
      root, extension = os.path.splitext(filename) # check extension of file
      if extension not in ['.out','.fits','']:
        raise ValueError("the extension of the filename should be .out, .fits or empty. It's %s" % extension)
      if extension == '':
        extension = '.fits'
        filename += extension
    
      if os.path.isfile(filename): # check if file already exists
        if overwrite is True:
          os.remove(filename)
          if extension == '.out':
            os.remove('imlog_'+filename)
        else:
          raise IOError("Filename %s already exists" % filename)
      if os.path.isfile('image.out'):
        raise RadmcError("image.out already exists")
      
      if not (os.path.isfile('dust_temperature.dat') or os.path.isfile('dust_temperature.bdat')): # does dust_temperature.dat exist?
        raise RadmcError("Couldn't find dust_temperature.dat. Did you run 'radmc3d mctherm' ?")
      
      if lamb is None and iline is None:
        lamb = 850. # if we have neither line nor single wavelength use single wavelength by default
      
      if lamb is not None and iline is not None:
        raise RadmcError("you cannot both include an iline and lamb option for radmc3d image command")
      
      if iline is not None:
        if not os.path.isfile('lines.inp'):
          raise RadmcError("Could not fine lines.inp. Cannot do line emission")
      
      if noline and nodust:
        raise RadmcError("Doesn't make sense to include neither line nor dust emission")
      
      # -------- Handle predefined projection direction
      if dir != None:
        if not dir in ['x','y','z','-x','-y','-z', 'face', 'edge']:
          raise ValueError("dir must be x y z -x -y -z face or edge")
        if dir is 'x':
          incl = 90. ; phi = -90. ; posang = 0.
        elif dir is 'y':
          incl = 90. ; phi = 180. ; posang = -90.
        elif dir is 'z':
          incl = 0. ; phi = 0. ; posang = 0.
        elif dir is '-x':
          incl = 90. ; phi = 90. ; posang = -90.
        elif dir is '-y':
          incl = 90. ; phi = 0. ; posang = 0.
        elif dir is '-z':
          incl = 180. ; phi = 0. ; posang = 90.
        elif dir in ['edge', 'face']:
          if len(omega) != 3:
            raise ValueError("omega either not defined or it has the wrong length")
          r = np.linalg.norm(omega)
          # align angles according to the vector omega, usually the rotation vector of a protoplanetary disk.
          # face means face on to the disk, and incl and phi are determined from the rotation axis. posang is set to 0
          # face means edge on to the disk. the point of view is along the ascending node. posang is set so that the rotation axis of the disk is along the y-axis
          if dir is 'face':
            incl = np.arccos(omega[2]/r) ; phi = np.arctan2(-omega[0],-omega[1]) ; posang = 0.
          if dir is 'edge':
            vector = _rotcoord(np.array([1.,0.,0.]),omega/r,inverse=True)
            incl = np.arccos(vector[2]) ; phi = np.arctan2(-vector[0],-vector[1]) ; posang = -np.arccos(omega[2]/r)
          incl, phi, posang = incl*180/np.pi, phi*180/np.pi, posang*180/np.pi
      
      # -------- Size of cut-out
      if sizeau is None:
        sizeau = self.sizeau # remove a bit
      else:
        assert sizeau <= self.sizeau, "sizeau must be smaller than or equal to model size"

      # -------- Size of image
      if npix is None:
        npix = 2*sizeau/self.grid_size
        if npix < 500:
          print "radmc3d image. native npix = %i which I think is too little. Raising to 500 pixels" % npix
          npix = 500
        if npix > 1000:
          print "radmc3d image. native npix = %i which I think is too much. Lowering to 1000 pixels" % npix
          npix = 1000
      
      if incl == phi == posang == 0.:
        dir = 'z'
    
      # ------- Parse Command
      cmd  = []
      cmd.extend([self.lexe, 'image'])
      if lamb is not None:
        cmd.extend(['lambda', str(lamb)])
      else:
        cmd.extend(['imolspec', str(imolspec), 'iline', str(iline), 'widthkms', str(widthkms), 'linenlam', str(linenlam), 'vkms', str(vkms)])
      cmd.extend(['incl', str(incl), 'phi', str(phi), 'posang', str(posang)])
      cmd.extend(['sizeau', str(sizeau)])
      cmd.extend(['npix', str(npix)])
      if secondorder and not doppcatch and lamb is None:
        cmd.extend(['secondorder'])
      if doppcatch and lamb is None:
        cmd.extend(['doppcatch'])
      if nostar is True:
        cmd.extend(['nostar'])
      if nodust is True:
        cmd.extend(['nodust'])
      if noline is True:
        cmd.extend(['noline'])

      # ------- Run radmc3d image
      if silent is True: # if silent suppress output
        with open(os.devnull, 'w') as fnull:
          void = call(cmd, stdout=fnull)
      else:
        void = call(cmd)
      
      if not os.path.exists('image.out'):
        raise RadmcError('radmc3d image apparently never produced an image')
      
      if extension == '.fits':
        extraheader = [('command',' '.join(cmd),"radmc3d command string"),\
                       ('incl'   ,incl         ,"Inclination of coordinate system (degree)"),\
                       ('phi'    ,phi          ,"Phi angle of coordinate system (degree)"),\
                       ('posang' ,posang       ,"Position angle of coordinate system (degree)"),\
                       ('dir'    ,dir          ,"Projection of coordinate system"),\
                       ('sizeau' ,sizeau       ,"Size of cut-out in AU"),\
                       ('nostar' ,nostar       ,"Stellar emission turned off?"),\
                       ('nodust' ,nodust       ,"Dust emission turned off?"),\
                       ('noline' ,noline       ,"Line emission turned off?")]
        if dir in ['face', 'edge']:
          extraheader.append(('omegax', omega[0], "x coordinate of omega vector"))
          extraheader.append(('omegay', omega[1], "y coordinate of omega vector"))
          extraheader.append(('omegaz', omega[2], "z coordinate of omega vector"))
        if iline is not None:
          extraheader.append(('molname' , self.mol_names[imolspec-1], "Molecule name"))
          extraheader.append(('imolspec', imolspec                  , "Molecule number (in lines.inp)"))
          extraheader.append(('iline'   , iline                     , "Rotational transition number (upper)"))
          extraheader.append(('vkms'    , vkms                      , "Offset from line centre in km/s"))
          # ------- Try to determine central frequency of line from molecule_*.inp files
          if self.mol_spec_files != -1:
            mol_filename = self.mol_spec_files[imolspec-1]
          else:
            moduledir, _ = os.path.split(__file__)
            dataFileDir = os.path.join(moduledir,'datafiles')
            mol_filename = os.path.join(dataFileDir,'molecule_%s.inp' % self.mol_names[imolspec-1])
          if os.path.isfile(mol_filename):
            with open(mol_filename,'r') as f:
              lines = f.readlines()
            for i,l in enumerate(lines):
              if '!TRANS + UP' in l:
                nu0   = float(lines[i+iline].split()[4]) # GHz
                lamb0 = co.c_in_um_s/(nu0*1e9)
                extraheader.append(('nu0'   , nu0   , "Line rest frequency in GHz"))
                extraheader.append(('lamb0' , lamb0 , "Line rest wavelength in micron"))

        try:
          writecube = False if iline is None else True # write fits cube if plotting lines
          void = writefitsimage('image.out',filename=filename,extraheader=extraheader,writecube=writecube)
        finally:
          os.remove('image.out') # always remove image.out
      elif extension == '.out':
        # ------- Rename file
        if filename != 'image.out':
          os.rename('image.out',filename)
    finally:
      # -------- return to current_dir
      os.chdir(current_dir)
  
    return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
  def spectrum(self,**kw):
    """
    Run radmc3d spectrum command
    """
    return self.sedspectrum('spectrum',**kw)

  def sed(self,**kw):
    """
    Run radmc3d sed command
    """
    
    kw['noline'] = True
    
    return self.sedspectrum('sed',**kw)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
  def sedspectrum(self,cmdstr,**kw):
    """
    Run radmc3d sed or spectrum
    """
    # ------- Handle Keywords
    lambdarange = kw.get('lambdarange',None)         # Lambdarange
    incl        = kw.get('incl',0.)                  # Inclination
    phi         = kw.get('phi',0.)                   # Phi
    dir         = kw.get('dir',None)                 # Predefined projection direction
    omega       = kw.get('omega',None)               # rotation axis (used when setting dir to 'face' or 'edge')
    sizeau      = kw.get('sizeau',None)              # size in au of the cutout
    filename    = kw.get('filename','spectrum.fits') # sed file
    overwrite   = kw.get('overwrite',False)          # Overwrite old images
    silent      = kw.get('silent',False)             # suppress output to prompt
    secondorder = kw.get('secondorder',False)        # use second order integration
    useaperture = kw.get('useaperture',False)        # use aperture information from aperture_info.inp
    dpc         = kw.get('dpc',150)                  # distance in parsec to source

    iline       = kw.get('iline',None)           # Line J iline -> iline-1
    imolspec    = kw.get('imolspec',1)           # Molecule to use (default is the first molecule)
    widthkms    = kw.get('widthkms',5)           # width of window in km/s
    linenlam    = kw.get('linenlam',51)          # Number of wavelength points
    vkms        = kw.get('vkms',0.)              # Offset from line centre in km/s
    doppcatch   = kw.get('doppcatch',False)      # use doppler catching
    
    nostar      = kw.get('nostar',False)         # don't plot the central object
    noline      = kw.get('noline',False)         # don't include line emission in image
    nodust      = kw.get('nodust',False)         # don't include dust emission in image
    
    # -------- Handle directories
    current_dir = os.path.abspath('.')
    os.chdir(self.modeldir)
    
    try:
      # -------- Sanity Checks
      if iline is not None:
        assert cmdstr != 'sed', "Lines not included in sed call"
      
      if lambdarange is not None:
        assert cmdstr == 'spectrum', "lambdarange only allowed for radmc3d spectrum command"
        
      if lambdarange is not None and iline is not None:
        raise RadmcError("you cannot both include an iline and lambdarange option for radmc3d spectrum command")
      
      root, extension = os.path.splitext(filename) # check extension of file
      if extension not in ['.out','.fits','']:
        raise ValueError("the extension of the filename should be .out, .fits or empty. It's %s" % extension)
      if extension == '':
        extension = '.fits'
        filename += extension
      
      if os.path.isfile(filename): # check if file already exists
        if overwrite is True:
          os.remove(filename)
        else:
          raise IOError("Filename %s already exists" % filename)
      if os.path.isfile('spectrum.out'):
        raise RadmcError("spectrum.out already exists")
      
      if not (os.path.isfile('dust_temperature.dat') or os.path.isfile('dust_temperature.bdat')): # does dust_temperature.dat already exist
        raise IOError("Couldn't find dust_temperature.dat or .bdat. Did you run 'radmc3d mctherm' ?")
      
      # -------- Handle predefined projection direction
      if dir != None:
        if not dir in ['x','y','z','-x','-y','-z','face','-face','edge']:
          raise ValueError("dir must be x y z -x -y -z face -face or edge")
        if dir is 'x':
          incl = 90. ; phi = -90.
        elif dir is 'y':
          incl = 90. ; phi = 180.
        elif dir is 'z':
          incl = 0. ; phi = 0.
        elif dir is '-x':
          incl = 90. ; phi = 90.
        elif dir is '-y':
          incl = 90. ; phi = 0.
        elif dir is '-z':
          incl = 180. ; phi = 0.
        elif dir in ['edge', 'face', '-face']:
          if len(omega) != 3:
            raise ValueError("omega either not defined or it has the wrong length")
          r = np.linalg.norm(omega)
          # align angles according to the vector omega, usually the rotation vector of a protoplanetary disk.
          # face means face on to the disk, and incl and phi are determined from the rotation axis
          # face means edge on to the disk. the point of view is along the ascending node.
          if dir is 'face':
            incl = np.arccos(omega[2]/r) ; phi = np.arctan2(-omega[0],-omega[1])
          if dir == '-face':
            incl = np.arccos(-omega[2]/r) ; phi = np.arctan2(omega[0],omega[1])
          if dir is 'edge':
            vector = _rotcoord(np.array([1.,0.,0.]),omega/r,inverse=True)
            incl = np.arccos(vector[2]) ; phi = np.arctan2(-vector[0],-vector[1])
          incl, phi = incl*180/np.pi, phi*180/np.pi
      
      if incl == phi == 0.:
        dir = 'z'
      
      # ------- Parse Commands
      cmd = []
      cmd.extend([self.lexe, cmdstr])
      if lambdarange is not None:
        cmd.extend(['lambdarange', str(lambdarange[0]), str(lambdarange[1])])
        cmd.extend(['nlam', str(lambdarange[2])])
      cmd.extend(['incl', str(incl), 'phi', str(phi)])
      if iline is not None:
        cmd.extend(['imolspec', str(imolspec), 'iline', str(iline), 'widthkms', str(widthkms), 'linenlam', str(linenlam), 'vkms', str(vkms)])
      if useaperture:
        cmd.extend(['useapert dpc', str(dpc)])
      if sizeau is not None:
        cmd.extend(['sizeau', str(sizeau)])
      if secondorder and not doppcatch:
        cmd.extend(['secondorder'])
      if doppcatch and iline is not None:
        cmd.extend(['doppcatch'])
      if nostar is True:
        cmd.extend(['nostar'])
      if noline is True:
        cmd.extend(['noline'])
      if nodust is True:
        cmd.extend(['nodust'])
      # ------- Run radmc3d sed/spectrum
      if silent is True: # if silent suppress output
        with open(os.devnull, 'w') as fnull:
          void = call(cmd, stdout=fnull)
      else:
        void = call(cmd)
      
      if not os.path.exists('spectrum.out'):
        raise RadmcError('radmc3d sed apparently never produced a spectrum')
      
      if extension == '.fits':
        extraheader = [('command',' '.join(cmd),""),\
                       ('incl'   ,incl         ,"Inclination of coordinate system (degree)"),\
                       ('phi'    ,phi          ,"Phi angle of coordinate system (degree)"),\
                       ('dir'    ,dir          ,"Projection of coordinate system"),\
                       ('useaper',useaperture  ,"Has wavelength dependent aperture been used"),\
                       ('nostar' ,nostar       ,"Stellar emission turned off?"),\
                       ('nodust' ,nodust       ,"Dust emission turned off?"),\
                       ('noline' ,noline       ,"Line emission turned off?")]
        if dir in ['face', 'edge']:
          extraheader.append(('omegax', omega[0], "x coordinate of omega vector"))
          extraheader.append(('omegay', omega[1], "y coordinate of omega vector"))
          extraheader.append(('omegaz', omega[2], "z coordinate of omega vector"))
        if iline is not None:
          extraheader.append(('molname' , self.mol_names[imolspec-1], "Molecule name"))
          extraheader.append(('imolspec', imolspec                  , "Molecule number (in lines.inp)"))
          extraheader.append(('iline'   , iline                     , "Rotational transition number (upper)"))
          extraheader.append(('vkms'    , vkms                      , "Offset from line centre in km/s"))
        try:
          unitkms = False if iline is None else True # write line spectrum if iline is not None
          void = writefitssed('spectrum.out',filename=filename,dpc=dpc,extraheader=extraheader,unitkms=unitkms)
        finally:
          os.remove('spectrum.out') # always remove spectrum.out
      elif extension == '.out':
        # ------- Rename file
        if filename != 'spectrum.out':
          os.rename('spectrum.out',filename)
    finally:
      # -------- return to current_dir
      os.chdir(current_dir)
    
    return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
  def tausurf(self,*args,**kw):
    """
    Run radmc3d image
    """
    # ------- Handle Keywords
    depth       = kw.get('depth',1.)             # optical depth
    lamb        = kw.get('lamb',850.)            # Wavelength in um
    incl        = kw.get('incl',0.)              # Inclination
    phi         = kw.get('phi',0.)               # Phi
    posang      = kw.get('posang',0.)            # Position angle
    sizeau      = kw.get('sizeau',None)          # Size of cut-out in AU (default is data range)
    npix        = kw.get('npix',500)             # size of image
    filename    = kw.get('filename','tausurface.out')# Image file
    dir         = kw.get('dir',None)             # Predefined projection direction
    overwrite   = kw.get('overwrite',False)      # Overwrite old images
    silent      = kw.get('silent',False)         # suppress output to prompt
    omega       = kw.get('omega',None)           # rotation axis (used when setting dir to 'face' or 'edge')
  
    # -------- Handle directories
    current_dir = os.path.abspath('.')
    os.chdir(self.modeldir)
  
    try:
      # -------- Sanity Checks
      root, extension = os.path.splitext(filename) # check extension of file
      if extension not in ['.out','.fits','']:
        raise ValueError("the extension of the filename should be .out, .fits or empty. It's %s" % extension)
      if extension == '':
        extension = '.fits'
        filename += extension
    
      if os.path.isfile(filename): # check if file already exists
        if overwrite is True:
          os.remove(filename)
          if extension == '.out':
            os.remove('imlog_'+filename)
        else:
          raise IOError("Filename %s already exists" % filename)
      if os.path.isfile('image.out'):
        raise RadmcError("image.out already exists")
      
      if not (os.path.isfile('dust_temperature.dat') or os.path.isfile('dust_temperature.bdat')): # does dust_temperature.dat exist?
        raise RadmcError("Couldn't find dust_temperature.dat or .bdat. Did you run 'radmc3d mctherm' ?")
    
      # -------- Handle predefined projection direction
      if dir != None:
        if not dir in ['x','y','z','-x','-y','-z','face','edge']:
          raise ValueError("dir must be x y z -x -y -z face or edge")
        if dir is 'x':
          incl = 90. ; phi = -90. ; posang = 0.
        elif dir is 'y':
          incl = 90. ; phi = 180. ; posang = -90.
        elif dir is 'z':
          incl = 0. ; phi = 0. ; posang = 0.
        elif dir is '-x':
          incl = 90. ; phi = 90. ; posang = -90.
        elif dir is '-y':
          incl = 90. ; phi = 0. ; posang = 0.
        elif dir is '-z':
          incl = 180. ; phi = 0. ; posang = 90.
        elif dir in ['edge', 'face']:
          if len(omega) != 3:
            raise ValueError("omega either not defined or it has the wrong length")
          r = np.linalg.norm(omega)
          # align angles according to the vector omega, usually the rotation vector of a protoplanetary disk.
          # face means face on to the disk, and incl and phi are determined from the rotation axis. posang is set to 0
          # face means edge on to the disk. the point of view is along the ascending node. posang is set so that the rotation axis of the disk is along the y-axis
          if dir is 'face':
            incl = np.arccos(omega[2]/r) ; phi = np.arctan2(-omega[0],-omega[1]) ; posang = 0.
          if dir is 'edge':
            vector = _rotcoord(np.array([1.,0.,0.]),omega/r,inverse=True)
            incl = np.arccos(vector[2]) ; phi = np.arctan2(-vector[0],-vector[1]) ; posang = -np.arccos(omega[2]/r)
          incl, phi, posang = incl*180/np.pi, phi*180/np.pi, posang*180/np.pi

      # -------- Size of cut-out
      if sizeau is None:
        sizeau = self.sizeau # remove a bit
      else:
        assert sizeau <= self.sizeau, "sizeau must be smaller than or equal to model size"
    
      # ------- Parse Command
      cmd  = []
      cmd.extend([self.lexe, 'tausurf', str(depth)])
      if lamb == 'allwl':
        cmd.extend([lamb])
      else:
        cmd.extend(['lambda', str(lamb)])
      cmd.extend(['incl', str(incl), 'phi', str(phi), 'posang', str(posang)])
      cmd.extend(['sizeau', str(sizeau)])
      cmd.extend(['npix', str(npix)])
      
      # ------- Run radmc3d image
      if silent is True: # if silent suppress output
        with open(os.devnull, 'w') as fnull:
          void = call(cmd, stdout=fnull)
      else:
        void = call(cmd)
      
      if not os.path.exists('image.out'):
        raise RadmcError('radmc3d tausurf apparently never produced an image')
      
      if extension == '.fits':
        extraheader = [('command',' '.join(cmd),"radmc3d command string"),\
                       ('depth'  ,depth        ,"Optical depth"),\
                       ('incl'   ,incl         ,"Inclination of coordinate system (degree)"),\
                       ('phi'    ,phi          ,"Phi angle of coordinate system (degree)"),\
                       ('posang' ,posang       ,"Position angle of coordinate system (degree)"),\
                       ('dir'    ,dir          ,"Projection of coordinate system"),\
                       ('sizeau' ,sizeau       ,"Size of cut-out in AU")]
        if dir in ['face', 'edge']:
          extraheader.append(('omegax', omega[0], "x coordinate of omega vector"))
          extraheader.append(('omegay', omega[1], "y coordinate of omega vector"))
          extraheader.append(('omegaz', omega[2], "z coordinate of omega vector"))
        try:
          void = writefitstausurf('image.out',filename=filename,extraheader=extraheader)
        finally:
          os.remove('image.out') # always remove image.out
          os.remove('tausurface_3d.out')
      elif extension == '.out':
        # ------- Rename file
        if filename != 'image.out':
          os.rename('image.out',filename)
        os.remove('tausurface_3d.out')
        # ------- Write logfile
        f = open('taulog_'+filename,'w')
        print >> f, "# IMAGE INFO"
        print >> f, "%s : %s" % ('command',' '.join(cmd))
        print >> f, "%s : %f" % ('optical depth', depth)
        print >> f, "%s : %s" % ('filename',filename)
        print >> f, "%s : %s" % ('wavelength',str(lamb))
        print >> f, "%s : %f" % ('incl (degree)',incl)
        print >> f, "%s : %f" % ('phi (degree)',phi)
        print >> f, "%s : %f" % ('posang (degree)',posang)
        print >> f, "%s : %s" % ('dir',dir)
        print >> f, "%s : %i" % ('Image size (pixels)',npix)
        print >> f, "%s : %f" % ('sizeau',sizeau)
        f.close()
    finally:
      # -------- return to current_dir
      os.chdir(current_dir)
  
    return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
  def readimage(self,filename,**kw):
    """
    Wrapper for readfiles.readimage (assumes filename is in modeldir)
    """
    return readimage(os.path.join(self.modeldir,filename),**kw)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
  def readsed(self,filename,**kw):
    """
    Wrapper for readfiles.readsed (assumes filename is in modeldir)
    """
    return readsed(os.path.join(self.modeldir,filename),**kw)

  def readspectrum(self,filename,**kw):
    """
    Wrapper for readfiles.readsed (assumes filename is in modeldir)
    """
    return readsed(os.path.join(self.modeldir,filename),**kw)
