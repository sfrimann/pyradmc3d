#!/usr/bin/env python
# -*- coding: utf-8 -*-

from readwriteinp import read_amr_grid, read_dust_density, read_dust_temperature
from glob import glob
import shutil
import os

try:
  from astropy.io import fits as pyfits
except:
  import pyfits

import numpy as np
import constants as c
from itertools import islice
from scipy.ndimage import map_coordinates
from scipy.optimize import fmin

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def downsample_model(*args,**kw):
  
  # ------- Handle Arguments
  if len(args) == 1:
    old_modeldir = os.path.abspath(os.path.expanduser(args[0]))
  else:
    raise IOError("utils.py downsample_model. Wrong number of inputs")
  
  # ------- Handle Keywords
  new_modeldir = kw.get('modeldir','radmc3d_model_downsampled') # name of model directory
  overwrite    = kw.get('overwrite',False) # overwrite old model
  downlevel    = kw.get('downlevel',-1)
  
  new_modeldir = os.path.abspath(os.path.expanduser(new_modeldir))
  
  # ------- Sanity Checks
  assert os.path.isdir(old_modeldir), "old model directory %s doesn't exist" % old_modeldir
  assert os.path.isfile(os.path.join(old_modeldir,'dust_temperature.dat')), "Cannot find dust_temperature.dat in %s" % old_modeldir
  assert os.path.isfile(os.path.join(old_modeldir,'amr_grid.inp')), "Cannot find amr_grid.inp in %s" % old_modeldir
  assert not os.path.isdir(new_modeldir), "model directory %s already exists" % new_modeldir
  
  assert float(downlevel).is_integer(), "downlevel has to be an integer"
  assert np.abs(downlevel) > 0, "downlevel cannot be zero. You have to downsample at least one level"
  
  # ------- Create new_modeldir and copy content of old_modeldir into new_modeldir (except outputfiles)
  void = os.mkdir(new_modeldir)
  for fname in glob(os.path.join(old_modeldir,'*.inp')):
    shutil.copy(fname,new_modeldir)
  shutil.copy(os.path.join(old_modeldir,'dust_temperature.dat'),new_modeldir)
  shutil.copy(os.path.join(old_modeldir,'logfile'),new_modeldir)
  
  # ------- Read grid file, density file, and temperature file
  r, dx, level = read_amr_grid(modeldir=new_modeldir, full=True)
  temperature  = read_dust_temperature(modeldir=new_modeldir)
  density      = read_dust_density(modeldir=new_modeldir)
  
  assert np.abs(downlevel) <= np.max(level), "you cannot downsample further than base grid"
  
  # ------- Read grid file once more for additional output
  f = open(os.path.join(new_modeldir,'amr_grid.inp'),mode='r')
  iformat                        = int(f.readline().rstrip('\n')) # typically 1
  gridstyle                      = int(f.readline().rstrip('\n')) # 1 is oct-tree
  coordsystem                    = int(f.readline().rstrip('\n')) # <100 is cartesian
  gridinfo                       = int(f.readline().rstrip('\n')) # typically 0
  incl_x, incl_y, incl_z         = map(int,f.readline().rstrip('\n').split()) # include x y z
  nx, ny, nz                     = map(int,f.readline().rstrip('\n').split()) #number of pixels
  levelmax, nleafmax, nbranchmax = map(int,f.readline().rstrip('\n').split())
  
  xedge   = np.genfromtxt(islice(f,nx+1),unpack=True,dtype=np.float64) # coordinate edges
  yedge   = np.genfromtxt(islice(f,ny+1),unpack=True,dtype=np.float64)
  zedge   = np.genfromtxt(islice(f,nz+1),unpack=True,dtype=np.float64)
  amrtree = np.genfromtxt(f,dtype=np.int,unpack=True) # coordiante tree
  f.close()
  
  # ------- Start downsampling (one level at the time)
  for i in range(np.abs(downlevel)):
    lvlmax    = np.max(level)
    lvlindex, = np.where(level == lvlmax) # Indices of maximum levels
    index1of8 = lvlindex[::8] # every 8th entry in lvlindex (to keep)
    
    # assert that we have filled the amr levels
    assert (len(lvlindex)/8.).is_integer(), "Bottom AMR level not filled out. Something is terribly wrong here!"
    
    amrindex, = np.where(amrtree == 0) # indices of amr leafs
    amrtree[amrindex[index1of8]-1] = 0 # remove amr branches
    amrtree = np.delete(amrtree,amrindex[lvlindex]) # remove botton amr leaves
    
    index7of8 = lvlindex.reshape((-1,8))[:,1:].ravel() # every 7 of 8 entries in lvlindex (to throw away)
    
    # ------- Downsample temperature, density, and level arrays 
    #temperature[index1of8] = np.array([map_coordinates(t.reshape((2,2,2)),[[0.5],[0.5],[0.5]])[0] for t in temperature[lvlindex].reshape((-1,8))])
    temperature[index1of8] = temperature[lvlindex].reshape((-1,8)).mean(axis=1)
    density[index1of8]     = density[lvlindex].reshape((-1,8)).mean(axis=1)
    level[index1of8]       = lvlmax - 1
    
    temperature = np.delete(temperature,index7of8)
    density     = np.delete(density,index7of8)
    level       = np.delete(level,index7of8)
  
  # ------- Write new files
  # ------- amr_grid.inp
  void = os.remove(os.path.join(new_modeldir,'amr_grid.inp')) # first remove old gridfile
  f = open(os.path.join(new_modeldir,'amr_grid.inp'),'w')
  print >> f, '1'           # iformat
  print >> f, '1'           # AMR grid style (1=OcTree AMR)
  print >> f, '1'           # Coordinate system (cartesian < 100)
  print >> f, '0'           # gridinfo
  print >> f, '1', '1', '1' # Include x,y,z coordinate
  print >> f, nx, ny, nz
  print >> f, lvlmax-1, len(level), len(amrtree)
  np.savetxt(f,xedge,delimiter=' ')
  np.savetxt(f,yedge,delimiter=' ')
  np.savetxt(f,zedge,delimiter=' ')
  np.savetxt(f,amrtree,fmt='%i')
  f.close()
  
  # ------- Write dust_density.inp
  void = os.remove(os.path.join(new_modeldir,'dust_density.inp')) # first remove old density file
  f = open(os.path.join(new_modeldir,'dust_density.inp'),'w')
  print >> f, '1'   # iformat
  print >> f, len(level) # number of cells
  print >> f, '1'   # number od dust species
  np.savetxt(f,density)
  f.close()
  
  # ------- Write dust_temperature.dat
  void = os.remove(os.path.join(new_modeldir,'dust_temperature.dat')) # first remove old temperature file
  f = open(os.path.join(new_modeldir,'dust_temperature.dat'),'w')
  print >> f, '1'   # iformat
  print >> f, len(level) # number of cells
  print >> f, '1'   # number od dust species
  np.savetxt(f,temperature)
  f.close()
  
  return 0
#########################################

# temperature at 1 AU as function of luminosity in Lsun
# temp = 186 Kelvin/Lsun * L**(0.2)
# temperature as function of radius
# temp = (temp at 1 AU) * r**(-0.35)

def temperature_fraction_distance(temperatures,fractions,mode='mass',modeldir='.'):
  """
  calculate fractions of material withingin a certain radius, evaulated by volume or mass
  """
  
  assert mode in ['mass','volume'], 'mode keyword must be mass or volume. It is %s' % mode
  
  # ------- read grid, density and temperature files
  grid, dx, level = read_amr_grid(modeldir=modeldir,full=True)
  temp = read_dust_temperature(modeldir=modeldir)
  if mode == 'mass':
    mass = read_dust_density(modeldir=modeldir) * dx**3

  radii = np.sqrt((grid**2).sum(axis=1))*c.cm2au
  dx   *= c.cm2au
  
  def volume_fun(r,goal_fraction,index_t):
    index_r = radii <= r
    fraction = (dx[index_r & index_t]**3).sum()/(dx[index_r]**3).sum()
    return (goal_fraction - fraction)**2
    
  def mass_fun(r,goal_fraction,index_t):
    index_r = radii <= r
    fraction = (mass[index_r & index_t]).sum()/(mass[index_r]).sum()
    return (goal_fraction - fraction)**2
  
  temperatures = [temperatures] if isinstance(temperatures,(int,float)) else temperatures
  fractions    = [fractions] if isinstance(fractions,(int,float)) else fractions
  
  initial_guess = 800. #AU
  result = []
  for t in temperatures:
    iindex_t = temp >= t
    for f in fractions:
      if mode == 'volume':
        result.append(fmin(volume_fun,initial_guess,args=(f,iindex_t))[0])
      else:
        result.append(fmin(mass_fun,initial_guess,args=(f,iindex_t))[0])
  
  return result

def temperature_fraction(t,modeldir='.',rrange=None,rmin=None, rmax=None,num=300,mode='mass'):
  """
  Calculate the fraction of a volume within a given radius that is above a certain temperature
  """
  
  assert mode in ['mass','volume'], 'mode keyword must be mass or volume. It is %s' % mode
  
  # ------- read grid and temperature file
  grid, dx, level = read_amr_grid(modeldir=modeldir,full=True)
  temp = read_dust_temperature(modeldir=modeldir)
  if mode == 'mass':
    mass = read_dust_density(modeldir=modeldir) * dx**3
  
  radii = np.sqrt((grid**2).sum(axis=1))*c.cm2au
  dx *= c.cm2au
  
  if rrange == None:
    if (rmin == None) & (rmax == None):
      rrange = np.linspace(radii.min(),radii.max(),num=num+1)[1:]
    elif rmin == None:
      rrange = np.linspace(radii.min(),rmas,num=num+1)[1:]
    elif rmax == None:
      rrange = np.linspace(rmin,radii.max(),num=num)
  
  fraction = np.empty(rrange.size)
  
  index_t = temp >= t
  for i in range(rrange.size):
    index_r = radii <= rrange[i]
    if mode == 'volume':
      fraction[i] = (dx[index_r & index_t]**3).sum()/(dx[index_r]**3).sum()
    else:
      fraction[i] = (mass[index_r & index_t]).sum()/(mass[index_r]).sum()

  return fraction

def temperature_profile(modeldir='.',rrange=None,rmin=None, rmax=None,num=300,weights='mass'):
  """
  """
  
  assert weights in ['mass','volume'], 'mode keyword must be mass or volume. It is %s' % weights
  
  # ------- read grid and temperature file
  grid, dx, level = read_amr_grid(modeldir=modeldir,full=True)
  temp = read_dust_temperature(modeldir=modeldir)
  if weights == 'mass':
    mass = read_dust_density(modeldir=modeldir) * dx**3
  
  radii = np.sqrt((grid**2).sum(axis=1))*c.cm2au
  dx *= c.cm2au
  
  if rrange == None:
    if (rmin == None) & (rmax == None):
      rrange = np.linspace(radii.min(),radii.max(),num=num+1)[1:]
    elif rmin == None:
      rrange = np.linspace(radii.min(),rmas,num=num+1)[1:]
    elif rmax == None:
      rrange = np.linspace(rmin,radii.max(),num=num)
   
  temperature = np.empty(rrange.size)
   
  for i in range(rrange.size):
    index_r = radii <= rrange[i]
    if weights == 'volume':
      temperature[i] = np.average(temp[index_r],weights=dx[index_r]**3)
    else:
      temperature[i] = np.average(temp[index_r],weights=mass[index_r])
  
  return temperature