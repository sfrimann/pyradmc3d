#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import os
import constants as co

class BindingEnergy(dict):
    """
    A derived dictionary to store the binding energies for the different
    neutral molecules used in Aikawa et al. (1997).

    The values are given as :math:`E_{\mathrm{b}, i} / k`, where
    :math:`E_{\mathrm{b}, i}` is the binding energy and :math:`k` is
    Boltzmann's constant.  The units are K.

    """

    def __init__(self, filename=None):
        if filename is None:
          filename = os.path.join(os.path.dirname(os.path.realpath(__file__)),'datafiles/aikawaetal1997_table1_adsorptionenergy.txt')
        self.read_bindingenergy_data(filename)

    def read_bindingenergy_data(self, filename):
        """
        Read binding energies from a file.
        The file should be in the form of `name`, `ebind`, with spaces
        for delimiters.  Anything beyond the first two columns is ignored.

        """

        with open(filename, 'r') as f:
            for line in f:
                x = line.split()
                if len(x) == 0: continue
                if x[0] == "#": continue

                if not self.has_key(x[0]):
                    self[x[0]] = np.float(x[1])
                else:
                    warnings.warn("{0} already exists in the dictionary...")

class AtomicData(dict):
    """
    A derived dictionary which contains the atomic mass and fractional
    isotopic composition of the elements, including the most common isotopes.
    
    If an isotope is not specified (using the mass number as key), then
    the most common isotope is used.

    """

    def __init__(self, filename=None):
        if filename is None:
          filename = os.path.join(os.path.dirname(os.path.realpath(__file__)),'datafiles/nist_atomicdata.txt')
        self.read_nist_atomic_data(filename)

    class SingleAtomData(dict):
        """Data strucutre for a single atom (but possibly with multiple isotopes)."""

        def __init__(self, mnumber, amass=None, isocomp=None):
            self[mnumber] = self.SingleIsotopeData(amass=amass, isocomp=isocomp)
            self._mostlikely = mnumber

        class SingleIsotopeData(object):
            """Data structure for a single isotope of a single atom."""

            def __init__(self, amass=None, isocomp=None):
                self.amass = amass
                self.isocomp = isocomp

        def _add(self, mnumber, amass=None, isocomp=None):
            """
            Add an isotope to an existing entry.
            Also establishes/updates which is the most common isotope.

            """

            self[mnumber] = self.SingleIsotopeData(amass=amass, isocomp=isocomp)
            self._mostlikely = max(self.iterkeys(), key=(lambda key: self[key].isocomp))

        @property
        def mnumber(self):
            return self._mostlikely

        @property
        def amass(self):
            return self[self._mostlikely].amass

        @property
        def isocomp(self):
            return self[self._mostlikely].isocomp            

    def read_nist_atomic_data(self, filename):
        """
        Read the atomic data stored in `filename`.
        Assumes that `filename` follows the format of the linearised ASCII
        data table found on the NIST website  (http://www.nist.gov/pml/data/comp.cfm).

        """

        nlines = 8
        with open(filename, 'r') as f:
            #skip header
            f.next()
            f.next()
            f.next()

            # read data table in 8 line blocks (matching the assumed format)
            # uses the grouper pattern, e.g., http://stackoverflow.com/a/6335876
            for block in itertools.izip_longest(*[f] * nlines):
                symbol = block[1].split()[3]
                mnumber = np.int(block[2].split()[3])
                try: 
                    amass = np.float(block[3].split()[4].split('(')[0])
                except:
                    amass = None
                try:
                    isocomp = np.float(block[4].split()[3].split('(')[0])
                except:
                    isocomp = None

                if not self.has_key(symbol):
                    self[symbol] = self.SingleAtomData(mnumber, amass=amass, isocomp=isocomp)
                else:
                    self[symbol]._add(mnumber, amass=amass, isocomp=isocomp)

class FreezeoutSublimationIterator(object):
  
  def __init__(self,mu,max_abund,min_abund=None,Tevap=None,Eb=None,kcrd=0.,equilibrium=False):
    self.mu    = mu
    self.Tevap = Tevap
    self.kcrd  = kcrd
    self.Eb    = Eb
    self.max_abund = max_abund
    self.kdep  = self.freezeout_rate
    self.ksub  = self.evaporation_rate
    self.equilibrium = equilibrium
    
    self.min_abund = self.max_abund/100 if min_abund is None else min_abund
    
    if (self.Eb is None) & (self.Tevap is None):
      raise ValueError('Tevap or Eb has to be given')

  def freezeout_rate(self, Tgas, nH2):
    """
    Follows Rodgers & Charnley (2003, ApJ, 585, 355) with atomic weights
    given by NIST (http://www.nist.gov/pml/data/comp.cfm).
    See the discussion surrouding eq. (3) of Rodgers & Charnley for
    more information

    `Tgas` in K;
    `mu` is the molecular weight;
    `nH2` is the number density of H2;

    """
    
#     return 1/(1e4*co.yr2s * np.sqrt(10./Tgas) * 1e6/nH2)
    return 4.55e-18 * np.sqrt(Tgas / self.mu) * 2*nH2

  def evaporation_rate(self, Tdust, nu_vibr=2.0e12):
    """
    Calculate the thermal evaporation time scale following Rodgers &
    Charnley (2003, ApJ, 585, 355); the time scale is simply the
    reciprocal of the evaporation rate (eq. 4 of Rodgers & Charnley).

    The default value for the vibrational frequency of the input molecule
    (`nu_vibr`) also comes from Rodgers & Charnley.
    The units of the binding energy (`Eboverk`) are assumed to be Kelvin.
    `T` is the dust temperature.

    The result has units of seconds.

    """
    
    if self.Eb is None:
      self.Eb = -self.Tevap*np.log(co.s2yr/nu_vibr)
    if self.Tevap is None:
      self.Tevap = -self.Eb/np.log(co.s2yr/nu_vibr)
    
    return nu_vibr * np.exp(-self.Eb/Tdust)
  
  def initialise_abundances(self,nH2,Tdust,Tgas):
    
    if self.equilibrium:
      abundance = (self.kcrd+self.ksub(Tdust))/(self.kdep(Tgas,nH2) + self.kcrd + self.ksub(Tdust))*self.max_abund
    else:
      abundance = np.where(self.ksub(Tdust) > co.s2yr, self.max_abund, self.min_abund)
#     abundance = np.where(self.ksub(Tdust) > co.s2yr, self.max_abund, self.kcrd/(self.kdep(Tgas,nH2) + self.kcrd)*self.max_abund)
    try:
      abundance[abundance < self.min_abund] = self.min_abund
    except:
      if abundance < self.min_abund:
        abundance = self.min_abund
    
    return abundance
  
  def abundance_step(self,abundance_in,nH2,Tdust,Tgas,dt):
    
    abundance_out = abundance_in - abundance_in*self.kdep(Tgas,nH2)*dt
    
    if self.equilibrium:
      abundance_out = abundance_out + (self.max_abund - abundance_out)*(self.kcrd+self.ksub(Tdust))*dt
    else:
      abundance_out = np.where(self.ksub(Tdust) > co.s2yr, self.max_abund, abundance_out)
    
    try:
      abundance_out[abundance_out > self.max_abund] = self.max_abund
      abundance_out[abundance_out < self.min_abund] = self.min_abund
    except:
      if abundance_out > self.max_abund:
        abundance_out = self.max_abund
      elif abundance_out < self.min_abund:
        abundance_out = self.min_abund

    return abundance_out
