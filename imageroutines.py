#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import numpy as np

try:
  from astropy.io import fits as pyfits
except:
  import pyfits

import os
import constants as co

from scipy.signal import fftconvolve
from scipy.signal.signaltools import _centered
from scipy.optimize import leastsq
from scipy import ndimage
from scipy.integrate import simps

from copy import deepcopy

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- TOP LEVEL READING ROUTINES (ASCII FITS COMBI) -#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def readimage(*args,**kw):
  """
  Top level image reading routine
  """
  # ------- Handle arguments
  if len(args) == 0:
    filename = 'image.fits'
  elif len(args) == 1:
    filename = args[0]
  else:
    raise IOError("readfiles.py. readimage. Wrong number of inputs")
  
  root, extension = os.path.splitext(filename) # check extension of file
  if extension not in ['.out','.fits','']:
    raise ValueError("the extension of the filename should be .out, .fits or empty. It's %s" % extension)
  if extension == '':
    extension = '.fits'
    filename += extension
  if extension == '.fits':
    return readfitsimage(filename,**kw)
  elif extension == '.out':
    return readasciiimage(filename,**kw)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- READ ASCII FILES #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def readasciiimage(*args,**kw):
  """
  Read image.out file produced by radmc3d
  Input:
    filename : filename to be read (default: image.out)
  Keywords:
    dpc      : Distance to object in parsec (default: 1 parsec)
  Returns:
    dictionary with keys:
      flux    : flux in Jansky/pixel
      pixnum  : [nx, ny]. Number of pixels in x and y
      pixsize : [sx, sy]. Pixel size in cm
      nlam    : number of wavelengths in image
      lambdas : wavelenghts in image
      xrange  : x coordinates of image in cm
      yrange  : y coordinates of image in cm
      dpc     : distance to source in parsec
      filename : filename of source file
  """
  # ------- Handle arguments
  if len(args) == 0:
    filename = 'image.out'
  elif len(args) == 1:
    filename = args[0]
  else:
    raise IOError("readfiles.py. readimage. Wrong number of inputs")
  
  # ------- Keywords
  dpc = kw.get('dpc',150.) #distance to object in parsec
    
  # ------- Constants
  pc2cm = 3.08567758e18
  
  # ------- Read file
  f = open(filename,mode='r')
  iformat = int(f.readline().rstrip('\n'))
  nx, ny  = map(int,f.readline().rstrip('\n').split()) #number of pixels
  nlam    = int(f.readline().rstrip('\n')) #number of lambdas
  sx, sy  = map(float,f.readline().rstrip('\n').split()) #pixel size in cm
  lambdas = [float(f.readline().rstrip('\n')) for i in range(nlam)] #lambdas in um
  lambdas = lambdas[0] if nlam == 1 else lambdas
  data    = np.genfromtxt(f,unpack=True)
  f.close()
  
  # ------- Check data
  if iformat != 1:
    raise ValueError("Only iformat == 1 supported at present")
  
  # ------- Reshape data
  data    = np.asarray(data,dtype=np.float)
  data    = np.squeeze(np.reshape(data,(nlam,ny,nx)))
  
  # ------- Write x and y coordinates
  xrange  = sx*(nx-1)
  yrange  = sy*(ny-1)
  xrange  = np.linspace(-xrange/2.,xrange/2.,num=nx)
  yrange  = np.linspace(-yrange/2.,yrange/2.,num=ny)
  
  # ------- convert data to Jansky/pixel
  sx_rad, sy_rad = sx/(dpc*pc2cm), sy/(dpc*pc2cm)
  factor         = 1e23*sx_rad*sy_rad
  data          *= factor #converting to Jansky/pixel
  
  return {'flux':data,'pixnum':[nx,ny],'pixsize':[sx,sy],'nlam':nlam,\
          'lambdas':lambdas,'xrange':xrange,'yrange':yrange,'dpc':dpc,'filename':filename}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- READ/WRITE FITS FILES -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def readfitsimage(*args,**kw):
  """
  Read image.out file produced by radmc3d
  Input:
    filename : filename to be read (default: image.fits)
  Keywords:
    dpc      : Distance to object in parsec (default: 150 parsec)
  Returns:
    dictionary with keys:
      flux    : flux in Jansky/pixel
      pixnum  : [nx, ny]. Number of pixels in x and y
      pixsize : [sx, sy]. Pixel size in cm
      nlam    : number of wavelengths in image
      lambdas : wavelenghts in image
      xrange  : x coordinates of image in cm
      yrange  : y coordinates of image in cm
      dpc     : distance to source in parsec
      filename : filename of source file
  """
  # ------- Handle arguments
  if len(args) == 0:
    filename = 'image.fits'
  elif len(args) == 1:
    filename = args[0]
  else:
    raise IOError("readfiles.py. readfitsimage. Wrong number of inputs")
  
  # ------- Keywords
  dpc = kw.get('dpc',150.) #distance to object in parsec

  # ------- Read fits file
  flux   = pyfits.getdata(filename,1)
  header = pyfits.getheader(filename,1)
  
  factor = 1. if header['cunit1'] == 'cm' else header['dpc']*co.pc2cm*co.deg2rad
  
  nx, ny = header['naxis1'], header['naxis2']
  sx, sy = header['cdelt1']*factor, header['cdelt2']*factor
  x0, y0 = header['crval1']*factor, header['crval2']*factor
  
  xrange  = np.arange(nx)*sx + x0
  yrange  = np.arange(ny)*sy + y0
  
  if 'naxis3' in header:
    nz = header['naxis3']
    sz = header['cdelt3']
    z0 = header['crval3']
    zrange = np.arange(nz)*sz + z0
  
  nlam    = 1
  lambdas = header['lambda']
  
  flux   *= header['dpc']**2/dpc**2
  
  if 'naxis3' in header:
    return {'flux':flux,'pixnum':[nx,ny,nz],'pixsize':[sx,sy,sz],'nlam':nz,\
            'lambdas':lambdas,'xrange':xrange,'yrange':yrange,'zrange':zrange,'dpc':dpc,'filename':filename}
  else:
    return {'flux':flux,'pixnum':[nx,ny],'pixsize':[sx,sy],'nlam':nlam,\
            'lambdas':lambdas,'xrange':xrange,'yrange':yrange,'dpc':dpc,'filename':filename}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def writefitsimage(image,**kw):
  """
  Write image fits file
  """
  # ------- Keywords
  dpc      = kw.get('dpc',150.) #distance to object in parsec
  logfile  = kw.get('logfile','log.fits') # logfile filename
  filename = kw.get('filename',None)
  extrahead = kw.get('extraheader',[])
  radec    = kw.get('radec',True) # Write angular coordinates
  writecube = kw.get('writecube',True) # if several wavelenghts write a fits cube
  prependlog = kw.get('prependlog',True) # first extension is log.fits
  
  # ------- Image file
  if isinstance(image,str):
    if filename is None:
      filename = os.path.splitext(image)[0] + '.fits'
    image = readasciiimage(image,dpc=dpc)
  elif isinstance(image,dict):
    dpc = image['dpc']
  else:
    raise ValueError("writefitsimage() * input image must be either string or dictionary")
  
  factor = co.rad2deg/(dpc*co.pc2cm) if radec is True else 1.
  
  filename = 'image.fits' if filename is None else filename
  
  # -------- Primary logfile
  if os.path.exists(logfile):
    logfile = pyfits.open(logfile)
    sinklog = logfile[1] if len(logfile) == 2 else None
    logfile = logfile[0]
  else:
    print "writefitsimage() * Warning * Couldn't find logfile %s. Putting bare header into primary HDU" % logfile
    logfile = pyfits.PrimaryHDU(None)
    sinklog = None
  
  if prependlog:
    fitsimages = [logfile] # start list of fitsimages
  else:
    fitsimages = []
  nlam = image['nlam']
  
  loopvariable = range(1) if nlam == 1 or writecube else range(nlam)
  
  if nlam > 1 and writecube:
    median_lambda = np.median(image['lambdas'])
    velocities    = co.c_in_km_s * (np.array(image['lambdas']) - median_lambda)/median_lambda
  
  for i in loopvariable:
    # -------- New fits image
    if nlam == 1 or writecube:
      fitsimage = pyfits.ImageHDU(image['flux']) if len(fitsimages) > 0 else pyfits.PrimaryHDU(image['flux'])
    else:
      fitsimage = pyfits.ImageHDU(image['flux'][i,:,:]) if len(fitsimages) > 0 else pyfits.PrimaryHDU(image['flux'][i,:,:])

    # -------- Start writing header
    fitsimage.header['FILENAME'] = filename
    fitsimage.header['BUNIT']  = 'JY/BEAM' if 'beam_fwhm' in image else 'JY/PIXEL'
    fitsimage.header['BZERO']  = 0.
    fitsimage.header['BSCALE'] = 1.

    fitsimage.header['CUNIT1'] = 'deg' if radec is True else 'cm'
    fitsimage.header['CUNIT2'] = 'deg' if radec is True else 'cm'
    if writecube and nlam > 1:
      fitsimage.header['CUNIT3'] = 'km/s'
    
    fitsimage.header['CDELT1'] = image['pixsize'][0] * factor
    fitsimage.header['CDELT2'] = image['pixsize'][1] * factor
    if writecube and nlam > 1:
      fitsimage.header['CDELT3'] = np.median(np.diff(velocities))
    
    fitsimage.header['CRPIX1'] = 1
    fitsimage.header['CRPIX2'] = 1
    if writecube and nlam > 1:
      fitsimage.header['CRPIX3'] = 1
    
    fitsimage.header['CRVAL1'] = image['xrange'][0] * factor
    fitsimage.header['CRVAL2'] = image['yrange'][0] * factor
    if writecube and nlam > 1:
      fitsimage.header['CRVAL3'] = velocities[0]
    
    if radec is True:
      fitsimage.header['CTYPE1'] = 'RA---TAN'
      fitsimage.header['CTYPE2'] = 'DEC--TAN'
    if writecube and nlam > 1:
      fitsimage.header['CTYPE3'] = 'VELO-LSR'
    
    if nlam == 1 or not writecube:
      fitsimage.header['lambda'] = image['lambdas'] if nlam == 1 else image['lambdas'][i]
    else:
      fitsimage.header['lambda'] = median_lambda
    fitsimage.header.comments['lambda'] = "Image wavelength in micrometer"
    
    if nlam == 1 or not writecube:
      fitsimage.header['freq'] = co.c_in_um_s/image['lambdas']/1e9 if nlam == 1 else co.c_in_um_s/image['lambdas'][i]/1e9
    else:
      fitsimage.header['freq'] = co.c_in_um_s/median_lambda/1e9
    fitsimage.header.comments['freq'] = "Image frequency in GHz"

    fitsimage.header['dpc'] = image['dpc']
    fitsimage.header.comments['dpc'] = "Distance to source in Parsec"

    if 'beam_fwhm' in image:
      fitsimage.header['beamsize'] = image['beam_fwhm'] * factor
      fitsimage.header.comments['beamsize'] = 'Size (FWHM) of beam'
      fitsimage.header['beamunit'] = image['beam_unit']
      fitsimage.header.comments['beamunit'] = 'Unit of beamsize'

    # ------- More header
    for header in extrahead:
      if len(header) == 2:
        key, value = header
        fitsimage.header[key] = value
      elif len(header) == 3:
        key, value, comment = header
        fitsimage.header[key] = value
        fitsimage.header.comments[key] = comment
      else:
        raise ValueError("extra header must consist of key, value og key, value, comment.")
    
    fitsimages.append(fitsimage)
  
  if sinklog is not None:
    fitsimages.append(sinklog)
  # ------- Save file
  hdulist = pyfits.HDUList(fitsimages)
  hdulist.writeto(filename)
  
  return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- IMAGE UTILITY FUNCTIONS -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def _convol(im,fwhm,fwhmy=None,theta=0.,beamcorrection=False):
  """
  core convolution function
  im is image to be convolved
  fwhm is full width half maximum of the gaussian kernel measured in pixels
  Note: fftconvolve is zero-padding
  """
  
  fwhmy = fwhm if fwhmy is None else fwhmy
  
  sigx, sigy = fwhm/2.35482, fwhmy/2.35482
  
  theta *= np.pi/180.
  
  assert fwhm > 1 and fwhmy > 1, "_convol: beamsize should be larger than pixel size or convolution does not make sense. fwhmx = %.2f fwhmy = %.2f" % (fwhm,fwhmy)
  
  # ------- ratio of pixel area to beam area
  factor = 1/(2*np.pi*sigx*sigy) if beamcorrection is True else 1.
  
  x = np.arange(0,5*sigx)
  x = np.concatenate((-x[-1:0:-1],x))
  y = np.arange(0,5*sigy)
  y = np.concatenate((-y[-1:0:-1],y))
  
  xx, yy = np.meshgrid(x,y)
  
  a = np.cos(theta)**2/(2*sigx**2) + np.sin(theta)**2/(2*sigy**2)
  b = np.sin(2*theta)/(4*sigx**2)  - np.sin(2*theta)/(4*sigy**2)
  c = np.sin(theta)**2/(2*sigx**2) + np.cos(theta)**2/(2*sigy**2)

  g = np.exp(-(a*xx**2 + 2*b*xx*yy + c*yy**2)) # kernel function
  
  if im.shape >= g.shape:
    cimage = fftconvolve(im*factor,g,mode='same')
  else:
    cimage = fftconvolve(im*factor,g,mode='full') # this is because in older versions of scipy (<0.13 fftconvolve with mode 'same' does not produce desired result)
    cimage = _centered(cimage,im.shape)
  
  # ------- ensure remove marginally negative (due to floating point errors) values 
  index = np.isclose(cimage,np.zeros(cimage.shape))
  cimage[index] = 0
  
  return cimage
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def primaryBeamCorr(im,antennadiameter):
  """
  input: image dictionary
         antenna diameter in m
  """
  
  imp = deepcopy(im)
  
  fwhm_cm = imp['lambdas']*1e-6/antennadiameter * imp['dpc'] * co.pc2cm
  sig_cm  = fwhm_cm/2.355
  xx, yy = np.meshgrid(imp['xrange'],imp['yrange'],sparse=True)
  
  imp['flux'] *= np.exp(-(xx**2 + yy**2)/2/sig_cm**2)
  
  return imp
  
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def convolveimage(im,fwhm,unit='arcsec',beamcorrection=False,reshape=True):
  """
  Convolve image with Gauss function
  """

  assert unit in ['arcsec','cm','au','parsec'], 'unit keyword not in list of allowed keywords'

  # ------- Constants
  pc2cm = 3.08567758e18
  
  if unit is 'arcsec':
    factor = 1./3600.*np.pi/180.*im['dpc']*pc2cm # arcsec to cm
  
  sx, sy = im['pixsize']
  
  cimage = _convol(im['flux'],fwhm*factor/sx,fwhmy=fwhm*factor/sy,beamcorrection=beamcorrection)
  
  assert im['flux'].shape == cimage.shape, "Original shape and colvolved shape are not the same"
  
  imc = deepcopy(im)
  
  imc['flux']      = cimage
  imc['beam_fwhm'] = fwhm
  imc['beam_unit'] = unit

  if reshape:
    xs, ys = int(fwhm*factor/sx), int(fwhm*factor/sy)
    
    imc['flux']   = imc['flux'][xs:-xs,ys:-ys]
    imc['pixnum'] = list(imc['flux'].shape)[::-1]
    imc['xrange'] = imc['xrange'][xs:-xs]
    imc['yrange'] = imc['yrange'][ys:-ys]

  return imc
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def _image2uv(data,xdelta,ydelta=None,fftsize=None,reverse=False):
  """
  fourier transform image data
  Input
  data      : Continuum image
  xdelta    : Sample spacing in the x direction (measured in radian)
  ydelta    : Sample spacing in the y direction (measured in radian)
  fftsize   : Size of the fftarray. Two element tuble (ysize, xsize)
              If data array is smaller than fftsize it is zero padded
              If None the resulting array is the same size as the data array
  Returns
  fft_d     : fourier transformed image
  urange    : u-vector (measured in wavelengths)
  vrange    : v-vector (measured in wavelengths)
  """

  def custom_ifftshift(x,dd,axes=None):
    tmp = np.asarray(x)
    ndim = len(tmp.shape)
    try:
      integer_types = np.compat.integer_types + (np.core.integer,)
    except:
      integer_types = (int, np.integer)
    if axes is None:
        axes = list(range(ndim))
    elif isinstance(axes, integer_types):
        axes = (axes,)
    y = tmp
    for k in axes:
        n1 = tmp.shape[k]
        n = dd.shape[k]
        p2 = n-(n+1)//2
        mylist = np.concatenate((np.arange(p2, n1), np.arange(p2)))
        y = np.take(y, mylist, k)
    return y
  
  # copy data
  d = data.copy()
  
  # length of each dimension in d
  yl, xl = d.shape
  
  # is ydelta the same as xdelta
  ydelta = xdelta if ydelta is None else ydelta
  
  # zero pad if asked
  if fftsize != None:
    assert len(fftsize) == 2, "fftsize must have two entries"
    d = np.pad(d,((0,fftsize[0]-yl),(0,fftsize[1]-xl)),'constant',constant_values=0)
  
  if reverse:
    fft_d = np.fft.ifft2(custom_ifftshift(d,data)) # run fft
    fft_d = np.fft.fftshift(fft_d) # shift zero frequency to centre  
  else:
    fft_d = np.fft.fft2(custom_ifftshift(d,data)) # run fft
    fft_d = np.fft.fftshift(fft_d) # shift zero frequency to centre
  
  # uv axes
  urange = np.fft.fftshift(np.fft.fftfreq(fft_d.shape[1],d=xdelta))
  vrange = np.fft.fftshift(np.fft.fftfreq(fft_d.shape[0],d=ydelta))
  
  return fft_d, urange, vrange

def _image2uv_uvlist(data,xrange,yrange,uvalue,vvalue):
  """
  fourier transform image data
  Input
  data      : Continuum image
  xrange    : x-coordinates in image plane (measured in radian)
  yrange    : y-coordinates in image plane (measured in radian)
  uvalue    : u-baselines (measured in wavelenghts)
  vvalue    : v-baselines (measured in wavelenghts)
  Returns
  vis       : visibilities at u and v baselines
  """
  
  rvis = np.empty(uvalue.size,dtype=np.float64)
  ivis = np.empty(uvalue.size,dtype=np.float64)

  for ibl in range(len(uvalue)):
    u, v = uvalue[ibl], vvalue[ibl]
    
    xx, yy = np.meshgrid(xrange,yrange,sparse=True)

    phase    = 2.*np.pi * (u*xx + v*yy)
    rvis[ibl] = (data*np.cos(phase)).sum()
    ivis[ibl] = (-data*np.sin(phase)).sum()
  
#     rdum = np.empty(len(yrange),dtype=np.float64)
#     idum = np.empty(len(yrange),dtype=np.float64)
#     for iy in range(len(yrange)):
#       phase    = 2.*np.pi * (u*xrange + v*yrange[iy])
#       rdum[iy] = (data[iy,:]*np.cos(phase)).sum()
#       idum[iy] = (-data[iy,:]*np.sin(phase)).sum()
# #       rdum[iy] = simps(data[iy,:]*np.cos(phase),xrange)
# #       idum[iy] = simps(-data[iy,:]*np.sin(phase),xrange)
#     rvis[ibl] = rdum.sum()
#     ivis[ibl] = idum.sum()
# #     rvis[ibl] = simps(rdum,yrange)
# #     ivis[ibl] = simps(idum,yrange)
  
    vis = rvis + 1j*ivis
  
  return vis
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def image2uv(im,fftsize=None,uvalue=None,vvalue=None):
  
  if uvalue is not None:
    xrange, yrange = im['xrange'].copy(), im['yrange'].copy()
    xrange *= co.cm2au/im['dpc']*co.arcsec2rad
    yrange *= co.cm2au/im['dpc']*co.arcsec2rad
    
    vis = _image2uv_uvlist(im['flux'],xrange,yrange,uvalue,vvalue)
    
    return {'visibility':vis,'nlam':im['nlam'],'lambdas':im['lambdas'],'ulist':uvalue,\
            'vlist':vvalue,'dpc':im['dpc'],'filename':im['filename']}
  else:
    xdelta, ydelta = np.median(np.diff(im['xrange'])), np.median(np.diff(im['yrange']))
    xdelta *= co.cm2au/im['dpc']*co.arcsec2rad
    ydelta *= co.cm2au/im['dpc']*co.arcsec2rad
  
    fft, urange, vrange = _image2uv(im['flux'],xdelta,ydelta=ydelta,fftsize=fftsize,reverse=False)
  
    nv, nu = fft.shape
    su, sv = np.median(np.diff(urange)), np.median(np.diff(vrange))

    return {'visibility':fft,'pixnum':[nv,nu],'pixsize':[su,sv],'nlam':im['nlam'],\
            'lambdas':im['lambdas'],'urange':urange,'vrange':vrange,'dpc':im['dpc'],'filename':im['filename']}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def uv2image(vis):
  
  udelta, vdelta = np.median(np.diff(vis['urange'])), np.median(np.diff(vis['vrange']))
  udelta *= co.cm2au/vis['dpc']*co.arcsec2rad
  vdelta *= co.cm2au/vis['dpc']*co.arcsec2rad
  
  ifft, xrange, yrange = _image2uv(vis['visibility'],udelta,ydelta=vdelta,reverse=True)
  
  ny, nx = ifft.shape
  sx, sy = np.median(np.diff(xrange)), np.median(np.diff(yrange))

  return {type:'momentmap','flux':np.absolute(ifft),'pixnum':[ny,nx],'pixsize':[sx,sy],'order':0,'nlam':1,'lambdas':vis['lambdas'],\
          'xrange':xrange,'yrange':yrange,'dpc':vis['dpc'],'filename':vis['filename']}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def uvamp(uv,uvrange=None,offset=[0.0,0.0]):
  """
  uv amplitude function
  """
  
  # setup uv bins
  if uvrange is None:
    if 'urange' in uv:
      uvrange = np.arange(0,max(uv['urange'].max()/1000.,uv['vrange'].max())/1000.,5)
    else:
      uvrange = np.arange(0,max(uv['ulist'].max()/1000.,uv['vlist'].max())/1000.,5)
  else:
    uvrange = np.asarray(uvrange)
  
  nbin = uvrange.size - 1
  
  uvamp, sigmean, sn, expect, npoint = np.empty(nbin), np.empty(nbin), np.empty(nbin), np.empty(nbin), np.empty(nbin)
  
  if 'urange' in uv:
    uu, vv = np.meshgrid(uv['urange']/1000.,uv['vrange']/1000.,sparse=True)
  else:
    uu, vv = uv['ulist']/1000., uv['vlist']/1000.
  
  vis = uv['visibility']
  
  vis = vis * np.exp(-2*np.pi*1j*(offset[0]*uu+offset[1]*vv)*co.arcsec2rad*1000)
  
  for i in range(nbin):
    index     = (uu**2 + vv**2 >= uvrange[i]**2) & (uu**2 + vv**2 < uvrange[i+1]**2)
    
    npoint[i] = index.sum()
    if npoint[i] == 0:
      uvamp[i], sigmean[i], sn[i], expect[i] = 0, 0, 0, 0
      continue
    
    uvamp[i]   = np.absolute(np.mean(vis[index]))
    varr2      = ((vis[index].real**2).sum() - npoint[i]*np.mean(vis[index].real)**2)/(npoint[i]-1)
    vari2      = ((vis[index].imag**2).sum() - npoint[i]*np.mean(vis[index].imag)**2)/(npoint[i]-1)
    sigtot     = vis[index].real.mean()**2/uvamp[i]**2*varr2 + vis[index].imag.mean()**2/uvamp[i]**2*vari2
    sigmean[i] = np.sqrt(sigtot/(npoint[i]-2))
    sn[i]      = uvamp[i]/sigmean[i]
    expect[i]  = np.sqrt(np.pi/2)*sigmean[i]
  
  uvpoints = 0.5*(uvrange[1:]+uvrange[:-1])
  
  return {'uvrange':uvpoints,'uvamp':uvamp,'sigma':sigmean,'sn':sn,'expect':expect,'npoint':npoint}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def subtract_background(im):
  
  bim = im.copy()
  
  limit = np.percentile(bim['flux'].ravel(),3)
  
  bim['flux'] -= limit
  
  bim['flux'][bim['flux'] <= 0] = bim['flux'][bim['flux'] > 0].min()
  
  return bim
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def moment(cube,n=0):
  """
  Calculate moment map from image cube
  """
  
  moment = (cube['flux']*(cube['zrange'][:,np.newaxis,np.newaxis]**n)).sum(axis=0)*cube['pixsize'][2]
  
  return {type:'momentmap','flux':moment,'pixnum':cube['pixnum'][:-1],'pixsize':cube['pixsize'][:-1],'order':n,'nlam':1,'lambdas':cube['lambdas'],\
          'xrange':cube['xrange'],'yrange':cube['yrange'],'dpc':cube['dpc'],'filename':cube['filename']}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def gauss(x,y=None,amp=1.,fwhmx=1.,fwhmy=None,x0=0.,y0=0.,theta=0.,fluxdens=True):
  """
  two dimensional gaussian
  theta is in degree
  """
  
  theta *= np.pi/180
#   fwhmx *= co.arcsec2rad
#   x0    *= co.arcsec2rad
#   y0    *= co.arcsec2rad
  
  y = x if y is None else y
  
  if (x.ndim == 1) & (y.ndim == 1):
    xx, yy = np.meshgrid(x,y,sparse=True)
  elif x.shape == y.shape:
    xx, yy = x, y
  else:
    raise ValueError("x and y coordinate vectors must have same shape")
  
  fwhmy = fwhmx if fwhmy is None else fwhmy
  
  sigx, sigy = fwhmx/(2*np.sqrt(2*np.log(2))), fwhmy/(2*np.sqrt(2*np.log(2)))
  
  a =   np.cos(theta)**2/(2*sigx**2) + np.sin(theta)**2/(2*sigy**2)
  b = - np.sin(2*theta)/(4*sigx**2)  + np.sin(2*theta)/(4*sigy**2)
  c =   np.sin(theta)**2/(2*sigx**2) + np.cos(theta)**2/(2*sigy**2)

  amp = amp / (2*np.pi*sigx*sigy) if fluxdens else amp

  return amp * np.exp(-(a*(xx-x0)**2 + 2*b*(xx-x0)*(yy-y0) + c*(yy-y0)**2))
  
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def uvgauss(u,v=None,amp=1.,fwhmx=1.,fwhmy=None,x0=0.,y0=0.,theta=0.,offset=0.,fluxdens=True,make_meshgrid=True):
  """
  two dimensional gaussian in uv-plane
  theta is in degree
  """
  
  v = u if v is None else v
  
  theta *= np.pi/180
  fwhmx *= co.arcsec2rad
  x0    *= co.arcsec2rad
  y0    *= co.arcsec2rad
  
  if make_meshgrid:
    uu, vv = np.meshgrid(u,v,sparse=True)
  else:
    uu, vv = u, v
  
  fwhmy = fwhmx if fwhmy is None else fwhmy*co.arcsec2rad
  
  sigx, sigy = fwhmx/(2*np.sqrt(2*np.log(2))), fwhmy/(2*np.sqrt(2*np.log(2)))
  Sigx, Sigy = 1./(2*np.pi*sigx), 1./(2*np.pi*sigy)
  
  a =   np.cos(theta)**2/(2*Sigx**2) + np.sin(theta)**2/(2*Sigy**2)
  b = - np.sin(2*theta)/(4*Sigx**2)  + np.sin(2*theta)/(4*Sigy**2)
  c =   np.sin(theta)**2/(2*Sigx**2) + np.cos(theta)**2/(2*Sigy**2)
  
  amp = amp / (2*np.pi*sigx*sigy) if fluxdens else amp
  
  return 2 * np.pi * amp * sigx * sigy * np.exp(-(a*uu**2 + 2*b*uu*vv + c*vv**2)) * np.exp(-2*np.pi*1j*(x0*uu+y0*vv)) + offset
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def uvfit(vis,fix='',spar=None,uvmin=10,uvmax=100,weights=None):
  """
  Fit functions in the uv-plane
  """
  
  # default starting guesses
  parname = ['amp','x','y','fwhmx','fwhmy','theta','offset']
  p0      = np.array([np.absolute(vis['visibility']).max(),0.,0.,10.,10.,0.,0.])
  
  # list of starting parameters
  if spar is None:
    spar = [None,None,None,None,None,None,None]
  
  # replace default starting parameters
  for i,sp in enumerate(spar):
    if sp is not None:
      p0[i] = sp
  
  fixList = np.array([False,False,False,False,False,False,False])
  if 'x' in fix:
    fixList[1] = True
  if 'y' in fix:
    fixList[2] = True
  if 'c' in fix:
    fixList[4] = True
    fixList[5] = True
    p0[4]      = None
  if 'o' in fix:
    fixList[6] = True
  
  if 'urange' in vis:
    uu, vv = np.meshgrid(vis['urange']/1000.,vis['vrange']/1000.,sparse=True)
  else:
    uu, vv = vis['ulist']/1000., vis['vlist']/1000.
  
  r = np.sqrt(uu**2 + vv**2)
  index = (r >= uvmin) & (r <= uvmax)
  
  def err(p):
    p0[~fixList] = p
    amp, x0, y0, fwhmx, fwhmy, theta, offset = p0
    amp = np.abs(amp)
    fwhmx = np.abs(fwhmx)
    if np.isnan(p0[4]):
      fwhmy = None
    else:
      fwhmy = np.abs(fwhmy)
    offset = np.abs(offset)
    fit = uvgauss(uu*1000.,v=vv*1000.,amp=amp,fwhmx=fwhmx,fwhmy=fwhmy,x0=x0,y0=y0,theta=theta,offset=offset,make_meshgrid=False)
    
    if weights is None:
      w = 1.#/(2*np.pi*np.sqrt(uu**2 + vv**2)[index])
    else:
      w = weights[index]/(2*np.pi*np.sqrt(uu**2 + vv**2)[index])
    
    return np.hstack((vis['visibility'].real[index],vis['visibility'].imag[index])) - np.hstack((fit.real[index],fit.imag[index]))
  
  par, pcov, infodict, mesg, ier = leastsq(err,p0[~fixList],full_output=True)
  ss = err(par)**2
  s_sq = ss.sum()/((ss > 1e-7).sum()-len(par))
  p0[~fixList] = par
  p0[0]  = np.abs(p0[0])
  p0[3]  = np.abs(p0[3])
  p0[4]  = np.abs(p0[4]) if 'c' not in fix else p0[3]
  p0[-1] = np.abs(p0[-1])
  
  #print par
  #print pcov
  #print s_sq
  #print mesg
  #print ier
  
  if pcov is None:
    pcov = np.zeros((len(par),len(par)))+np.inf
  
  sigma = np.zeros(p0.size,dtype=np.float)
  sigma[~fixList] = np.sqrt(np.diag(pcov)*s_sq)
  
  return p0, sigma

def block_mean(ar, fact):
    assert isinstance(fact, int), type(fact)
    sx, sy = ar.shape
    X, Y = np.ogrid[0:sx, 0:sy]
    regions = sy//fact * (X//fact) + Y//fact
    res = ndimage.mean(ar, labels=regions, index=np.arange(regions.max() + 1))
    res.shape = (sx/fact, sy/fact)
    return res

def downsampleImage(im,factor=2):
  
  imn = deepcopy(im)
  
  imn['flux'] = block_mean(imn['flux'],factor)
  imn['xrange'] = imn['xrange'].reshape((-1,factor)).mean(axis=1)
  imn['yrange'] = imn['yrange'].reshape((-1,factor)).mean(axis=1)
  imn['pixsize'] = [p*factor for p in imn['pixsize']]
  
  return imn