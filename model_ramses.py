#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

import numpy as np

from readwriteinp import read_dust_temperature
from amr import amrsort
from ice_freezeout_sublimation import FreezeoutSublimationIterator
import constants as co
from glob import glob
import os
import re

class RamsesCell(object):
  """
  Model input object for pyradmc3d.
  The idea of the model objects is to have an easy way of inputting different models to pyradmc3d.
  If you do not find a model suitable for your needs you can simply write your own.
  
  This model object is for MHD cutouts from the RAMSES code. It takes as input a cell dictionary from
  the pyramses module.
  """
  def __init__(self,cell,**kw):
    
    # ------- Handle Keywords
    sink_numbers = kw.get('sink_numbers',None)
    luminosity   = kw.get('luminosity',1.)      # luminosity of central object (units of Lsun)
    temperature  = kw.get('temperature',1000.)  # temperature of central object (K)
    opac         = kw.get('opac','oh5')
    kcrd         = kw.get('kcrd',0.)
    molecules    = kw.get('molecules','c18o')
    Eb           = kw.get('Eb',None)
    gas2dust     = kw.get('gas2dust',100.)
    modeldir     = kw.get('modeldir','.')
    
    # ------- Initialize some instance variables with dummy values
    self.modelname  = 'RamsesCell' # model object name
    self.amrindices = -1
    self.amrtree    = -1
    self.molecules  = molecules
    self.kcrd       = kcrd
    self.opac       = opac
    self.modeldir   = modeldir
    self.gas2dust   = gas2dust
    
    self.datadir    = str(cell['datadir'])
    self.nout       = int(cell['nout'])
    self.snapshot_time = float(cell['time'])
    
    # ------- Set molecule parameters
    if molecules == 'c18o':
      self.Eb        = 1307. if Eb is None else Eb
      self.max_abund = 2e-7
      self.min_abund = 1e-2*self.max_abund
      self.mu        = 30.
    else:
      raise IOError("models.py RamsesCell. I don't recognize molecule keyword %s" % molecule)
    
    # ------- Begin with handling the stars inside the cutout
    #         If sink_numbers is None only the central star is turned on
    sic = cell['sinks_inside_cutout']
    if sic is False:
      raise RadmcError('No stars in cutout')
    
    # -------- Do some sanity checks on the star(s)
    if sink_numbers == None or isinstance(sink_numbers, (int,float)): # turn on only one star
      sink_numbers = int(cell['sink_number']) if sink_numbers == None else sink_numbers
      
      if sink_numbers is False:
        raise RadmcError("No sink in centre")
      if isinstance(luminosity, (list,tuple,np.ndarray)):
        raise RadmcError("When only central star is turned on luminosity keyword must be a number")
      if isinstance(temperature, (list,tuple,np.ndarray)):
        raise RadmcError("When only central star is turned on temperature keyword must be a number")
      if not sink_numbers in sic:
        raise RadmcError("sink %i not in cutout" % sink_numbers)
      
    elif isinstance(sink_numbers, (list,tuple,np.ndarray)) or sink_numbers == 'all':
      sink_numbers = sic if sink_numbers == 'all' else sink_numbers
      
      if len(np.intersect1d(sink_numbers,sic)) != len(sink_numbers):
        raise RadmcError("Not all sink numbers are in the cutout")
      if isinstance(luminosity,(int,float)):
        luminosity = np.repeat(luminosity,len(sink_numbers))
      elif len(luminosity) != len(sink_numbers):
        raise RadmcError("len(luminosity) != len(sink_numbers)")
      if isinstance(temperature,(int,float)):
        temperature = np.repeat(temperature,len(sink_numbers))
      elif len(temperature) != len(sink_numbers):
        raise RadmcError("len(temperature) != len(sink_numbers)")
    
    # ------- Original (unsorted) physical variables
    self.r_orig           = cell['r'].copy()       # position (cm)
    self.dx_orig          = cell['dx'].copy()      # cell size (cm)
    self.gas_density_orig = cell['density'].copy() # gas density (g/cm^3)
    self.level_orig       = cell['level'].copy()   # AMR levels
    self.velocity_orig    = cell['velocity'].copy()# velocities (cm/s)
    
    # ------- If amrindices is saved in cell dictionary get them, otherwise run amrsort
    if 'amrindices' in cell:
      self.amrindices = cell['amrindices'].copy()
    else:
      self.run_amrsort()
    
    # ------- apply amrsort to the physical variables
    self.r           = self.r_orig[self.amrindices,:]
    self.dx          = self.dx_orig[self.amrindices]
    self.gas_density = self.gas_density_orig[self.amrindices]
    self.level       = self.level_orig[self.amrindices]
    self.velocity    = self.velocity_orig[self.amrindices,:]
    self.velocity    = self.velocity - cell['sink_velocity'][cell['sink_number']]
    
    # ------- find minima and maxima in grid
    amin = np.argmin(self.r,axis=0)
    amax = np.argmax(self.r,axis=0)
    
    self.minx, self.maxx = self.r[amin[0],0]-self.dx[amin[0]]/2, self.r[amax[0],0]+self.dx[amax[0]]/2
    self.miny, self.maxy = self.r[amin[1],1]-self.dx[amin[1]]/2, self.r[amax[1],1]+self.dx[amax[1]]/2
    self.minz, self.maxz = self.r[amin[2],2]-self.dx[amin[2]]/2, self.r[amax[2],2]+self.dx[amax[2]]/2
        
    self.grid_size = self.dx.min() # mininum cell size in cm
    self.mass = (self.gas_density*self.dx**3).sum()*co.g2msun # gas mass in cutout in Msun
    
    # ------- stellar parameters. These needs to be available and arrays even if you only have on star in the model
    self.sic              = sic                                      # list of sink numbers in cutout
    if isinstance(sink_numbers,(int,float)):
      self.onList         = np.where(sink_numbers == sic,True,False) # stars that are turned on
    else:
      self.onList         = np.array([s in sink_numbers for s in sic]) # stars that are turned on
    self.positionList     = cell['sinkr'][sic]                       # stellar positions in cm
    self.massList         = cell['sink_mass'][sic]                   # stellar masses in g
    self.logdmdtList      = cell['logdmdt'][sic]                     # log accretion rates in log(gram/second)
    self.ageList          = cell['sink_age'][sic]                    # stellar ages in s
    self.luminosityList   = np.zeros(len(self.sic)) ; self.luminosityList[self.onList] = luminosity   # stellar luminosities in Lsun
    self.temperatureList  = np.zeros(len(self.sic)) ; self.temperatureList[self.onList] = temperature # stellar temperatures in K
    
    if isinstance(sink_numbers, (list,tuple,np.ndarray)):
      distances = np.sqrt((cell['sinkr'][sic,:]**2).sum(axis=1))
      index     = np.argsort(distances)
    else:
      index     = [0]
    
    # ------- extra data for log.fits
    self.extraheader = [('datadir',self.datadir                ,""),\
                       ('lvlmin'  ,np.min(self.level)          ,"Mininum AMR level (if applicable)"),\
                       ('lvlmax'  ,np.max(self.level)          ,"Maximum AMR level (if applicable)"),\
                       ('nout'    ,self.nout                   ,"Snapshot Number"),\
                       ('time'    ,self.snapshot_time*co.s2kyr ,"Snapshot Time in kyr"),\
                       ('gas2dust',self.gas2dust               ,"Gas to dust mass ratio"),\
                       ('cutmass' ,self.mass                   ,"Gas mass in cutout"),\
                       ('gridsize',self.grid_size*co.cm2au     ,"Min grid size in AU"),\
                       ('kcrd'    ,self.kcrd                   ,"Cosmic ray desorption rate in 1/s"),\
                       ('Eb'      ,self.Eb                     ,"Binding energy of ice to dust grains in K")]
    if 'accretion_window' in cell:
      self.extraheader.append(['accsize', float(cell['accretion_window']),"Accretion window in years"])
    for i,(snumb,sage,temp,lum,smass,logdmdt) in enumerate(zip(sic[self.onList][index],
                                                               self.ageList[self.onList][index],
                                                               self.temperatureList[self.onList][index],
                                                               self.luminosityList[self.onList][index],
                                                               self.massList[self.onList][index],
                                                               self.logdmdtList[self.onList][index])):
      self.extraheader.append(['sinknum%i' % (i+1) , snumb           , "Sink Number"])
      self.extraheader.append(['sinkage%i' % (i+1) , sage*co.s2kyr   , "Age of sink particle measured in kyr"])
      self.extraheader.append(['temp%i' % (i+1)    , temp            , "Stellar Temperature in Kelvin"])
      self.extraheader.append(['lum%i' % (i+1)     , lum             , "Stellar Luminosity measured in Lsun"])
      self.extraheader.append(['smass%i' % (i+1)   , smass*co.g2msun , "Stellar mass in Msun"])
      if logdmdt < -50:
        self.extraheader.append(['logdmdt%i' % (i+1), -99. , "Stellar accretion in log(Msun/yr)"])
      else:
        self.extraheader.append(['logdmdt%i' % (i+1), logdmdt+np.log10(co.g2msun/co.s2yr) , "Stellar accretion in log(Msun/yr)"])
#     if self.onList.sum() == 1:
#       self.extraheader.append(['sinknumb', int(sic[self.onList])                      , "Sink Number"])
#       self.extraheader.append(['sinkage' , float(self.ageList[self.onList]*co.s2kyr)  , "Age of sink particle measured in kyr"])
#       self.extraheader.append(['temp'    , float(self.temperatureList[self.onList])   , "Stellar Temperature in Kelvin"])
#       self.extraheader.append(['lum'     , float(self.luminosityList[self.onList])    , "Stellar Luminosity measured in Lsun"])
#       self.extraheader.append(['smass'   , float(self.massList[self.onList]*co.g2msun), "Stellar mass in Msun"])
#       if float(self.logdmdtList[self.onList]) < -50:
#         self.extraheader.append(['logdmdt', -99. , "Stellar accretion in log(Msun/yr)"])
#       else:
#         self.extraheader.append(['logdmdt', float(self.logdmdtList[self.onList]+np.log10(co.g2msun/co.s2yr)) , "Stellar accretion in log(Msun/yr)"])
  
  def run_amrsort(self):
    """
    run amrsort 
    """
    
    self.amrindices, self.amrtree = amrsort(self.r_orig,self.dx_orig)
    
    if len(self.amrindices) == 0:
      raise RadmcError("radmc3d. Aparrently the density grid is empty")
  
  def make_grid(self):
    """
    set grid variables to be put into amr_grid.inp or amr_grid.binp
    """
    
    max_grid_size = np.max(self.dx_orig)
    
    if isinstance(self.amrtree,int):
      void = self.run_amrsort()
    
    # ------- Grid parameters for amr_grid.inp or *.binp
    self.gridstyle   = 1 # gridstyle: 1 = OCT style AMR
    self.coordsystem = 1 # coordinate system: Cartesian < 100
    self.include_x, self.include_y, self.include_z = 1, 1, 1
    
    self.ngridx = int(round((self.maxx-self.minx)/max_grid_size))
    self.ngridy = int(round((self.maxy-self.miny)/max_grid_size))
    self.ngridz = int(round((self.maxz-self.minz)/max_grid_size))
    
    self.gridx = np.linspace(self.minx,self.maxx,num=self.ngridx+1)
    self.gridy = np.linspace(self.miny,self.maxy,num=self.ngridy+1)
    self.gridz = np.linspace(self.minz,self.maxz,num=self.ngridz+1)
    
    self.nbranch     = self.amrtree.size  # Number of branches in tree
    self.nleaf       = self.amrtree.size - self.amrtree.sum()  # number of leaf cells
    self.level_range = np.maximum(1,np.max(self.level_orig) - np.min(self.level_orig)) # range of AMR levels
  
  def get_dust_density(self):
    """
    return dust densities in g/cm^3
    """
    
    return self.gas_density/self.gas2dust
  
  def get_gas_density(self):
    """
    return gas density in g/cm^3
    """
    
    return self.gas_density
  
  def get_gas_velocity(self):
    """
    return velocities in cm/s
    """
    
    return self.velocity
  
  def get_gas_abundance(self,molecule):
  
    # initialise freezeout/sublimation class
    molecule_iterator = FreezeoutSublimationIterator(self.mu,self.max_abund,min_abund=self.min_abund,Eb=self.Eb,kcrd=self.kcrd)
    
    if os.path.exists(os.path.join(self.modeldir,'history')):
      molecule_density = self.gas_density*co.massdens2h2numdens * self.iterator(molecule_iterator)
    else:
      temperatures = read_dust_temperature(modeldir=self.modeldir)
      molecule_density = self.gas_density*co.massdens2h2numdens * molecule_iterator.initialise_abundances(self.gas_density*co.massdens2h2numdens,temperatures,temperatures)
    
    return molecule_density
  
  def iterator(self,iterator_object):
    
    temperature_files = sorted(glob(os.path.join(self.modeldir,'history','dust_temperature*.bdat')))
    lumgrid           = np.array([float(re.search('_lum(.+?)_',t).group(1)) for t in temperature_files])
    groups            = re.search('_i(.+?)_(.+?).bdat',temperature_files[0])
    index_min         = int(groups.group(1))
    index_max         = int(groups.group(2))
    indices           = np.arange(index_min,index_max+1)
 
    lumsort = np.argsort(lumgrid)
    lumgrid = lumgrid[lumsort]
    temperature_files = np.asarray(temperature_files)[lumsort]
    
    savefile = glob(os.path.join(self.modeldir,'history','*.npz'))
    assert len(savefile) == 1, "There needs to be one savefile for evolution data %i" % (len(savefile))
    
    data = np.load(savefile[0])
    index, age, snapshot_times, mass, logdmdt, luminosity, Teff = data['index'], data['age'], data['snapshot_times'], data['mass'], data['logdmdt'], data['luminosity'], data['Teff']
    
#     kcrd = 1e-15               # cosmic ray desorption rate (1/s)
#     kdep = self.depletion_rate # depletion rate (function of T and density) in (1/s)
    
    used_ages         = age[indices]
    used_luminosities = luminosity[indices]
    
    temp_file_index = np.argmin(np.abs(np.log10(used_luminosities[0])-np.log10(lumgrid)))
    temperature_init = read_dust_temperature(filename=temperature_files[temp_file_index])*(used_luminosities[0]/lumgrid[temp_file_index])**0.2
    
    gas_density = self.gas_density*co.massdens2h2numdens
    
    # ------- initialise gas phase abundances
    abund = iterator_object.initialise_abundances(gas_density,temperature_init,temperature_init)
    
    # iterator
    for i in xrange(1,len(used_ages)):
      temp_file_index = np.argmin(np.abs(np.log10(used_luminosities[i])-np.log10(lumgrid)))
      temperature     = read_dust_temperature(filename=temperature_files[temp_file_index])*(used_luminosities[i]/lumgrid[temp_file_index])**0.2
      dt              = (used_ages[i]-used_ages[i-1])*co.kyr2s
      abund           = iterator_object.abundance_step(abund,gas_density,temperature,temperature,dt)
    
    return abund
