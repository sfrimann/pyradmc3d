#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

import numpy as np
from readwriteinp import read_dust_temperature
from ice_freezeout_sublimation import FreezeoutSublimationIterator
import constants as co
import os

class Spher1d(object):
  """
  Model input object for pyradmc3d.
  The idea of the model objects is to have an easy way of inputting different models to pyradmc3d.
  If you do not find a model suitable for your needs you can simply write your own.
  
  This model object is for spherical envelopes. The model is 1D
  """
  def __init__(self,**kw):
    """
    Spherical Envelope model object
    """

    # ------- Handle Keywords
    rin         = kw.get('rin', 20) # inner radius in AU
    rout        = kw.get('rout', 8000) # outer radius in AU
    prho        = kw.get('prho', 1.5) # power law index
    mass        = kw.get('mass', 1.) # envelope mass in Msun
    luminosity  = kw.get('luminosity',1.) # luminosity of central object (units of Lsun)
    temperature = kw.get('temperature',1000.) # temperature of central object (K)
    ngrid       = kw.get('ngrid',500) # number of grid points
    spacing     = kw.get('spacing','log')
    opac        = kw.get('opac','oh5')
    molecules   = kw.get('molecules','c18o')
    gas2dust    = kw.get('gas2dust',100.)
    modeldir    = kw.get('modeldir','.')
    kcrd        = kw.get('kcrd',0.)
    Eb          = kw.get('Eb',None)
    
    assert spacing in ['log','linear'], "Grid spacing must be either 'log' or 'linear'"

    # ------- Set molecule parameters
    if molecules == 'c18o':
      self.Eb        = 1307. if Eb is None else Eb
      self.max_abund = 2e-7
      self.min_abund = 1e-2*self.max_abund
      self.mu        = 30.
    else:
      raise IOError("models_spher1d.py. I don't recognize molecule keyword %s" % molecule)

    # ------- attributes
    self.modelname  = 'Spher1d'
    self.rin        = rin
    self.rout       = rout
    self.prho       = prho
    self.mass       = mass
    self.ngrid      = ngrid
    self.molecules  = molecules
    self.opac       = opac
    self.modeldir   = modeldir
    self.gas2dust   = gas2dust
    self.kcrd       = kcrd
    
    # ------- Stellar Parameters. Need to be there for Radmc3d object
    self.sic              = np.array([0])
    self.onList           = np.array([True])
    self.positionList     = np.array([[0,0,0]],dtype=np.float)
    self.massList         = np.array([0.])
    self.logdmdtList      = np.array([-99.])
    self.ageList          = np.array([0.])
    self.luminosityList   = np.array([luminosity])
    self.temperatureList  = np.array([temperature])
    
    self.minx, self.maxx = -rout*co.au2cm, rout*co.au2cm
    self.miny, self.maxy = -rout*co.au2cm, rout*co.au2cm
    self.minz, self.maxz = -rout*co.au2cm, rout*co.au2cm
    
    if spacing == 'log':
      self.gridx = np.logspace(np.log10(self.rin*co.au2cm),np.log10(self.rout*co.au2cm),num=self.ngrid+1)
    else:
      self.gridx = np.linspace(self.rin*co.au2cm,self.rout*co.au2cm,num=self.ngrid+1)
    
    self.grid_size = np.median(np.diff(self.gridx))
    
    self.extraheader = [('rin'    ,rin         , "Inner radius in AU"),\
                       ('rout'    ,rout        , "Outer radius in AU"),\
                       ('prho'    ,prho        , "Power law index"),\
                       ('mass'    ,mass        , "Model mass in Msun"),\
                       ('Eb'      ,self.Eb     , "Binding energy of ice to dust grains in K"),\
                       ('kcrd'    ,self.kcrd   , "Cosmic ray desorption rate in s-1"),\
                       ('ngrid'   ,ngrid       , "Number of grid points"),\
                       ('Teff'    ,temperature , "Stellar Temperature in Kelvin"),\
                       ('lum'     ,luminosity  , "Stellar Luminosity measured in Lsun"),\
                       ('gridsize',self.grid_size*co.cm2au ,"Grid size in AU")]

  def make_grid(self):
    """
    set grid variables to be put into amr_grid.inp or amr_grid.binp
    """
    
    # ------- Grid parameters for amr_grid.inp or *.binp
    self.gridstyle   = 0 # gridstyle: 0 = Regular
    self.coordsystem = 100 # coordinate system: 100 <= spherical < 200
    self.include_x, self.include_y, self.include_z = 1, 0, 0
    
    self.ngridx = self.ngrid
    self.ngridy = 1
    self.ngridz = 1
    
    self.gridx = self.gridx # I know stupid, but good for remembering that you always need this
    self.gridy = np.array([0, 0], dtype=np.float)
    self.gridz = np.array([0, 0], dtype=np.float)

  def get_dust_density(self):
    """
    return dust densities in g/cm^3
    """
    
    return self.get_gas_density()/self.gas2dust
  
  def get_gas_density(self):
    """
    return gas density in g/cm^3
    """
    
    xc   = np.interp(np.arange(self.ngrid)+0.5,np.arange(self.ngrid+1),self.gridx)
    
    rho0 = 1/(4*np.pi*self.rin**3/(self.mass*(3-self.prho)) * ( (self.rout/self.rin)**(3-self.prho) - 1**(3-self.prho)))
    
    gas_density = rho0 * co.msun2g/co.au2cm**3 * (xc/(self.rin*co.au2cm))**(-self.prho)
   
    return gas_density
    
  def get_gas_velocity(self):
    """
    return velocities in cm/s
    """
    
    # random velocities with 0.5 km/s sigma
    return np.random.randn(3*self.ngrid).reshape(self.ngrid,3)*50000
  
  def get_gas_abundance(self,molecule):
  
    # initialise freezeout/sublimation class
    molecule_iterator = FreezeoutSublimationIterator(self.mu,self.max_abund,min_abund=self.min_abund,Eb=self.Eb,kcrd=self.kcrd)
    
    temperatures = read_dust_temperature(modeldir=self.modeldir)
    molecule_density = self.get_gas_density()*co.massdens2h2numdens * molecule_iterator.initialise_abundances(self.get_gas_density()*co.massdens2h2numdens,temperatures,temperatures)
    
    return molecule_density