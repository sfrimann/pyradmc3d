#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

import numpy as np

from readwriteinp import read_dust_temperature
from amr import amrsort
import constants as co
from glob import glob
import os
import re

class RamsesCell(object):
  """
  Model input object for pyradmc3d.
  The idea of the model objects is to have an easy way of inputting different models to pyradmc3d.
  If you do not find a model suitable for your needs you can simply write your own.
  
  This model object is for MHD cutouts from the RAMSES code. It takes as input a cell dictionary from
  the pyramses module.
  """
  def __init__(self,cell,**kw):
    
    # ------- Handle Keywords
    sink_numbers = kw.get('sink_numbers',None)
    luminosity   = kw.get('luminosity',1.)      # luminosity of central object (units of Lsun)
    temperature  = kw.get('temperature',1000.)  # temperature of central object (K)
    opac         = kw.get('opac','oh5')
    molecules    = kw.get('molecules','c18o')
    gas2dust     = kw.get('gas2dust',100.)
    modeldir     = kw.get('modeldir','.')
    
    # ------- Initialize some instance variables with dummy values
    self.modelname  = 'RamsesCell' # model object name
    self.amrindices = -1
    self.amrtree    = -1
    self.molecules  = molecules
    self.opac       = opac
    self.modeldir   = modeldir
    self.gas2dust   = gas2dust
    
    self.datadir    = str(cell['datadir'])
    self.nout       = int(cell['nout'])
    self.snapshot_time = float(cell['time'])
    
    
    # ------- Begin with handling the stars inside the cutout
    #         If sink_numbers is None only the central star is turned on
    sic = cell['sinks_inside_cutout']
    if sic is False:
      raise RadmcError('No stars in cutout')
    
    # -------- Do some sanity checks on the star(s)
    if sink_numbers == None or isinstance(sink_numbers, (int,float)): # turn on only one star
      sink_numbers = int(cell['sink_number']) if sink_numbers == None else sink_numbers
      
      if sink_numbers is False:
        raise RadmcError("No sink in centre")
      if isinstance(luminosity, (list,tuple,np.ndarray)):
        raise RadmcError("When only central star is turned on luminosity keyword must be a number")
      if isinstance(temperature, (list,tuple,np.ndarray)):
        raise RadmcError("When only central star is turned on temperature keyword must be a number")
      if not sink_numbers in sic:
        raise RadmcError("sink %i not in cutout" % sink_numbers)
      
    elif isinstance(sink_numbers, (list,tuple,np.ndarray)) or sink_numbers == 'all':
      sink_numbers = sic if sink_numbers == 'all' else sink_numbers
      
      if len(np.intersect1d(sink_numbers,sic)) != len(sink_numbers):
        raise RadmcError("Not all sink numbers are in the cutout")
      if isinstance(luminosity,(int,float)):
        luminosity = np.repeat(luminosity,len(sink_numbers))
      elif len(luminosity) != len(sink_numbers):
        raise RadmcError("len(luminosity) != len(sink_numbers)")
      if isinstance(temperature,(int,float)):
        temperature = np.repeat(temperature,len(sink_numbers))
      elif len(temperature) != len(sink_numbers):
        raise RadmcError("len(temperature) != len(sink_numbers)")
    
    # ------- Original (unsorted) physical variables
    self.r_orig           = cell['r'].copy()       # position (cm)
    self.dx_orig          = cell['dx'].copy()      # cell size (cm)
    self.gas_density_orig = cell['density'].copy() # gas density (g/cm^3)
    self.level_orig       = cell['level'].copy()   # AMR levels
    self.velocity_orig    = cell['velocity'].copy()# velocities (cm/s)
    
    # ------- If amrindices is saved in cell dictionary get them, otherwise run amrsort
    if 'amrindices' in cell:
      self.amrindices = cell['amrindices'].copy()
    else:
      self.run_amrsort()
    
    # ------- apply amrsort to the physical variables
    self.r           = self.r_orig[self.amrindices,:]
    self.dx          = self.dx_orig[self.amrindices]
    self.gas_density = self.gas_density_orig[self.amrindices]
    self.level       = self.level_orig[self.amrindices]
    self.velocity    = self.velocity_orig[self.amrindices,:]
    self.velocity    = self.velocity - cell['sink_velocity'][cell['sink_number']]
    
    # ------- find minima and maxima in grid
    amin = np.argmin(self.r,axis=0)
    amax = np.argmax(self.r,axis=0)
    
    self.minx, self.maxx = self.r[amin[0],0]-self.dx[amin[0]]/2, self.r[amax[0],0]+self.dx[amax[0]]/2
    self.miny, self.maxy = self.r[amin[1],1]-self.dx[amin[1]]/2, self.r[amax[1],1]+self.dx[amax[1]]/2
    self.minz, self.maxz = self.r[amin[2],2]-self.dx[amin[2]]/2, self.r[amax[2],2]+self.dx[amax[2]]/2
        
    self.grid_size = self.dx.min() # mininum cell size in cm
    self.mass = (self.gas_density*self.dx**3).sum()*co.g2msun # gas mass in cutout in Msun
    
    # ------- stellar parameters. These needs to be available and arrays even if you only have on star in the model
    self.sic              = sic                                      # list of sink numbers in cutout
    if isinstance(sink_numbers,(int,float)):
      self.onList         = np.where(sink_numbers == sic,True,False) # stars that are turned on
    else:
      self.onList         = np.array([s in sink_numbers for s in sic]) # stars that are turned on
    self.positionList     = cell['sinkr'][sic]                       # stellar positions in cm
    self.massList         = cell['sink_mass'][sic]                   # stellar masses in g
    self.logdmdtList      = cell['logdmdt'][sic]                     # log accretion rates in log(gram/second)
    self.ageList          = cell['sink_age'][sic]                    # stellar ages in s
    self.luminosityList   = np.zeros(len(self.sic)) ; self.luminosityList[self.onList] = luminosity   # stellar luminosities in Lsun
    self.temperatureList  = np.zeros(len(self.sic)) ; self.temperatureList[self.onList] = temperature # stellar temperatures in K
    
    if isinstance(sink_numbers, (list,tuple,np.ndarray)):
      distances = np.sqrt((cell['sinkr'][sic,:]**2).sum(axis=1))
      index     = np.argsort(distances)
    else:
      index     = [0]
    
    # ------- extra data for log.fits
    self.extraheader = [('datadir',self.datadir                ,""),\
                       ('lvlmin'  ,np.min(self.level)          ,"Mininum AMR level (if applicable)"),\
                       ('lvlmax'  ,np.max(self.level)          ,"Maximum AMR level (if applicable)"),\
                       ('nout'    ,self.nout                   ,"Snapshot Number"),\
                       ('time'    ,self.snapshot_time*co.s2kyr ,"Snapshot Time in kyr"),\
                       ('gas2dust',self.gas2dust               ,"Gas to dust mass ratio"),\
                       ('cutmass' ,self.mass                   ,"Gas mass in cutout"),\
                       ('gridsize',self.grid_size*co.cm2au     ,"Min grid size in AU")]
    if 'accretion_window' in cell:
      self.extraheader.append(['accsize', float(cell['accretion_window']),"Accretion window in years"])
    for i,(snumb,sage,temp,lum,smass,logdmdt) in enumerate(zip(sic[self.onList][index],
                                                               self.ageList[self.onList][index],
                                                               self.temperatureList[self.onList][index],
                                                               self.luminosityList[self.onList][index],
                                                               self.massList[self.onList][index],
                                                               self.logdmdtList[self.onList][index])):
      self.extraheader.append(['sinknum%i' % (i+1) , snumb           , "Sink Number"])
      self.extraheader.append(['sinkage%i' % (i+1) , sage*co.s2kyr   , "Age of sink particle measured in kyr"])
      self.extraheader.append(['temp%i' % (i+1)    , temp            , "Stellar Temperature in Kelvin"])
      self.extraheader.append(['lum%i' % (i+1)     , lum             , "Stellar Luminosity measured in Lsun"])
      self.extraheader.append(['smass%i' % (i+1)   , smass*co.g2msun , "Stellar mass in Msun"])
      if logdmdt < -50:
        self.extraheader.append(['logdmdt%i' % (i+1), -99. , "Stellar accretion in log(Msun/yr)"])
      else:
        self.extraheader.append(['logdmdt%i' % (i+1), logdmdt+np.log10(co.g2msun/co.s2yr) , "Stellar accretion in log(Msun/yr)"])
#     if self.onList.sum() == 1:
#       self.extraheader.append(['sinknumb', int(sic[self.onList])                      , "Sink Number"])
#       self.extraheader.append(['sinkage' , float(self.ageList[self.onList]*co.s2kyr)  , "Age of sink particle measured in kyr"])
#       self.extraheader.append(['temp'    , float(self.temperatureList[self.onList])   , "Stellar Temperature in Kelvin"])
#       self.extraheader.append(['lum'     , float(self.luminosityList[self.onList])    , "Stellar Luminosity measured in Lsun"])
#       self.extraheader.append(['smass'   , float(self.massList[self.onList]*co.g2msun), "Stellar mass in Msun"])
#       if float(self.logdmdtList[self.onList]) < -50:
#         self.extraheader.append(['logdmdt', -99. , "Stellar accretion in log(Msun/yr)"])
#       else:
#         self.extraheader.append(['logdmdt', float(self.logdmdtList[self.onList]+np.log10(co.g2msun/co.s2yr)) , "Stellar accretion in log(Msun/yr)"])
  
  def run_amrsort(self):
    """
    run amrsort 
    """
    
    self.amrindices, self.amrtree = amrsort(self.r_orig,self.dx_orig)
    
    if len(self.amrindices) == 0:
      raise RadmcError("radmc3d. Aparrently the density grid is empty")
  
  def make_grid(self):
    """
    set grid variables to be put into amr_grid.inp or amr_grid.binp
    """
    
    max_grid_size = np.max(self.dx_orig)
    
    if isinstance(self.amrtree,int):
      void = self.run_amrsort()
    
    # ------- Grid parameters for amr_grid.inp or *.binp
    self.gridstyle   = 1 # gridstyle: 1 = OCT style AMR
    self.coordsystem = 1 # coordinate system: Cartesian < 100
    self.include_x, self.include_y, self.include_z = 1, 1, 1
    
    self.ngridx = int(round((self.maxx-self.minx)/max_grid_size))
    self.ngridy = int(round((self.maxy-self.miny)/max_grid_size))
    self.ngridz = int(round((self.maxz-self.minz)/max_grid_size))
    
    self.gridx = np.linspace(self.minx,self.maxx,num=self.ngridx+1)
    self.gridy = np.linspace(self.miny,self.maxy,num=self.ngridy+1)
    self.gridz = np.linspace(self.minz,self.maxz,num=self.ngridz+1)
    
    self.nbranch     = self.amrtree.size  # Number of branches in tree
    self.nleaf       = self.amrtree.size - self.amrtree.sum()  # number of leaf cells
    self.level_range = np.maximum(1,np.max(self.level_orig) - np.min(self.level_orig)) # range of AMR levels
  
  def get_dust_density(self):
    """
    return dust densities in g/cm^3
    """
    
    return self.gas_density/self.gas2dust
  
  def get_gas_density(self):
    """
    return gas density in g/cm^3
    """
    
    return self.gas_density
  
  def get_gas_velocity(self):
    """
    return velocities in cm/s
    """
    
    return self.velocity
  
  def get_gas_abundance(self,molecule):
    
    if molecule == 'c18o':
      Tevap    = 30.
      ntot     = 2e-7
      fraction = 1/100.
    else:
      raise IOError("models.py RamsesCell. I don't recognize molecule keyword %s" % molecule)
    
    if os.path.exists(os.path.join(self.modeldir,'history')):
      molecule_density = self.gas_density*co.massdens2numdens * self.iterator(molecule)
    else:
      temperatures = read_dust_temperature(modeldir=self.modeldir)
      molecule_density = self.gas_density*co.massdens2numdens * np.where(temperatures >= Tevap, ntot, fraction*ntot)
    
    return molecule_density
    
  def depletion_rate(self,T,dens):
    """
    T in kelvin
    density in cm-3
    depletion rate in 1/s
    """
    return 1./(2e4*3.15569e7 *np.sqrt(10./T) * 1e6/dens)
  
  def iterator(self,molecule):
    if molecule == 'c18o':
      Tevap    = 30.
      ntot     = 2e-7
      fraction = 1/100.
    else:
      raise IOError("models.py RamsesCell. I don't recognize molecule keyword %s" % molecule)
    
    temperature_files = sorted(glob(os.path.join(self.modeldir,'history','dust_temperature*.bdat')))
    lumgrid           = np.array([float(re.search('_lum(.+?)_',t).group(1)) for t in temperature_files])
    groups            = re.search('_i(.+?)_(.+?).bdat',temperature_files[0])
    index_min         = int(groups.group(1))
    index_max         = int(groups.group(2))
    indices           = np.arange(index_min,index_max+1)
    
    lumsort = np.argsort(lumgrid)
    lumgrid = lumgrid[lumsort]
    temperature_files = np.asarray(temperature_files)[lumsort]
    
    savefile = glob(os.path.join(self.modeldir,'history','*.npz'))
    assert len(savefile) == 1, "There needs to be one savefile for evolution data %i" % (len(savefile))
    
    data = np.load(savefile[0])
    index, age, snapshot_times, mass, logdmdt, luminosity, Teff = data['index'], data['age'], data['snapshot_times'], data['mass'], data['logdmdt'], data['luminosity'], data['Teff']
    
    kcrd = 1e-15               # cosmic ray desorption rate (1/s)
    kdep = self.depletion_rate # depletion rate (function of T and density) in (1/s)
    
    dt = np.diff(age[indices])*co.kyr2s
    used_luminosities = luminosity[indices]
    
    temp_file_index = np.argmin(np.abs(np.log10(used_luminosities[0])-np.log10(lumgrid)))
    temperature_init = read_dust_temperature(filename=temperature_files[temp_file_index])*(used_luminosities[0]/lumgrid[temp_file_index])**0.2
    
    gas_density = self.gas_density*co.massdens2numdens
    
    # ------- initialise gas phase abundances
    abun_nocosmic = np.where(temperature_init > Tevap, ntot, fraction*ntot)
    #abun_cosmic   = np.where(temperature_init > Tevap, ntot, kcrd/(kdep(temperature_init,gas_density) + kcrd)*ntot)
    
    # iterator
    for l,ddt in zip(used_luminosities[1:],dt):
      temp_file_index = np.argmin(np.abs(np.log10(l)-np.log10(lumgrid)))
      temperature     = read_dust_temperature(filename=temperature_files[temp_file_index])*(l/lumgrid[temp_file_index])**0.2
      abun_nocosmic   = abun_nocosmic - abun_nocosmic*kdep(temperature,gas_density)*ddt
      #abun_cosmic   = abun_cosmic   - abun_cosmic*kdep(temperature,gas_density)*ddt + (ntot - abun_cosmic)*kcrd*ddt
      
      abun_nocosmic = np.where(abun_nocosmic < fraction*ntot,fraction*ntot,abun_nocosmic)
      index = temperature > Tevap
      abun_nocosmic[index] = ntot
      #abun_cosmic[index] = ntot
    
    return abun_nocosmic

class Spher1d(object):
  """
  Model input object for pyradmc3d.
  The idea of the model objects is to have an easy way of inputting different models to pyradmc3d.
  If you do not find a model suitable for your needs you can simply write your own.
  
  This model object is for spherical envelopes. The model is 1D
  """
  def __init__(self,**kw):
    """
    Spherical Envelope model object
    """

    # ------- Handle Keywords
    rin         = kw.get('rin', 20) # inner radius in AU
    rout        = kw.get('rout', 8000) # outer radius in AU
    prho        = kw.get('prho', -1.5) # power law index
    mass        = kw.get('mass', 1.) # envelope mass in Msun
    Tevap       = kw.get('Tevap',30)
    fraction    = kw.get('fraction',0.01)
    luminosity  = kw.get('luminosity',1.) # luminosity of central object (units of Lsun)
    temperature = kw.get('temperature',1000.) # temperature of central object (K)
    ngrid       = kw.get('ngrid',500) # number of grid points
    spacing     = kw.get('spacing','log')
    opac        = kw.get('opac','oh5')
    molecules   = kw.get('molecules','c18o')
    gas2dust    = kw.get('gas2dust',100.)
    modeldir    = kw.get('modeldir','.')
    
    assert spacing in ['log','linear'], "Grid spacing must be tierh 'log' or 'linear'"
    
    # ------- attributes
    self.modelname  = 'Spher1d'
    self.rin        = rin
    self.rout       = rout
    self.prho       = prho
    self.mass       = mass
    self.Tevap      = Tevap
    self.fraction   = fraction
    self.ngrid      = ngrid
    self.molecules  = molecules
    self.opac       = opac
    self.modeldir   = modeldir
    self.gas2dust   = gas2dust
    
    # ------- Stellar Parameters. Need to be there for Radmc3d object
    self.sic              = np.array([0])
    self.onList           = np.array([True])
    self.positionList     = np.array([[0,0,0]],dtype=np.float)
    self.massList         = np.array([0.])
    self.logdmdtList      = np.array([-99.])
    self.ageList          = np.array([0.])
    self.luminosityList   = np.array([luminosity])
    self.temperatureList  = np.array([temperature])
    
    self.minx, self.maxx = -rout*co.au2cm, rout*co.au2cm
    self.miny, self.maxy = -rout*co.au2cm, rout*co.au2cm
    self.minz, self.maxz = -rout*co.au2cm, rout*co.au2cm
    
    if spacing == 'log':
      self.gridx = np.logspace(np.log10(self.rin*co.au2cm),np.log10(self.rout*co.au2cm),num=self.ngrid+1)
    else:
      self.gridx = np.linspace(self.rin*co.au2cm,self.rout*co.au2cm,num=self.ngrid+1)
    
    self.grid_size = np.median(np.diff(self.gridx))
    
    self.extraheader = [('rin'    ,rin         , "Inner radius in AU"),\
                       ('rout'    ,rout        , "Outer radius in AU"),\
                       ('prho'    ,prho        , "Power law index"),\
                       ('mass'    ,mass        , "Model mass in Msun"),\
                       ('Tevap'   ,Tevap       , "Evaporation Temperature in K"),\
                       ('fraction',fraction    , "abundance fraction below Tevap"),\
                       ('ngrid'   ,ngrid       , "Number of grid points"),\
                       ('temp'    ,temperature , "Stellar Temperature in Kelvin"),\
                       ('lum'     ,luminosity  , "Stellar Luminosity measured in Lsun"),\
                       ('gridsize',self.grid_size*co.cm2au ,"Grid size in AU")]

  def make_grid(self):
    """
    set grid variables to be put into amr_grid.inp or amr_grid.binp
    """
    
    # ------- Grid parameters for amr_grid.inp or *.binp
    self.gridstyle   = 0 # gridstyle: 0 = Regular
    self.coordsystem = 100 # coordinate system: 100 <= spherical < 200
    self.include_x, self.include_y, self.include_z = 1, 0, 0
    
    self.ngridx = self.ngrid
    self.ngridy = 1
    self.ngridz = 1
    
    self.gridx = self.gridx # I know stupid, but good for remembering that you always need this
    self.gridy = np.array([0, 0], dtype=np.float)
    self.gridz = np.array([0, 0], dtype=np.float)

  def get_dust_density(self):
    """
    return dust densities in g/cm^3
    """
    
    return self.get_gas_density()/self.gas2dust
  
  def get_gas_density(self):
    """
    return gas density in g/cm^3
    """
    
    xc   = np.interp(np.arange(self.ngrid)+0.5,np.arange(self.ngrid+1),self.gridx)
    
    rho0 = 1/(4*np.pi*self.rin**3/(self.mass*(3+self.prho)) * ( (self.rout/self.rin)**(3+self.prho) - 1**(3+self.prho)))
    
    gas_density = rho0 * co.msun2g/co.au2cm**3 * (xc/(self.rin*co.au2cm))**self.prho
   
    return gas_density
    
  def get_gas_velocity(self):
    """
    return velocities in cm/s
    """
    
    # random velocities with 0.5 km/s sigma
    return np.random.randn(3*self.ngrid).reshape(self.ngrid,3)*50000
  
  def get_gas_abundance(self,molecule):
    
    if molecule == 'c18o':
      Tevap    = 30.
      ntot     = 2e-7
      fraction = 1/100.
    else:
      raise IOError("models.py RamsesCell. I don't recognize molecule keyword %s" % molecules)
    
    temperatures = read_dust_temperature(modeldir=self.modeldir)
    molecule_density = self.get_gas_density()*co.massdens2numdens * np.where(temperatures >= self.Tevap, ntot, self.fraction*ntot)
    
    return molecule_density

class PPDisk(object):
  """
  Model input object for pyradmc3d.
  The idea of the model objects is to have an easy way of inputting different models to pyradmc3d.
  If you do not find a model suitable for your needs you can simply write your own.
  
  This model object is for a 2d disk in spherical coordinates.
  """
  def __init__(self,**kw):
    """
    2D disk in spherical coordinates
    """

    # ------- Handle Keywords
    rin         = kw.get('rin',0.1)       # alias
    xmin        = kw.get('xmin',rin)      # inner radius in AU
    rout        = kw.get('rout',400.)     # alias
    xmax        = kw.get('xmax',rout)     # outer radius in AU
    ngridr      = kw.get('ngridr',150)    # alias
    nx          = kw.get('nx',ngridr)     # number of grid points
    thetamin    = kw.get('thetamin',45.)  # alias
    ymin        = kw.get('ymin',thetamin) # min theta (deg)
    thetamax    = kw.get('thetamax',135.)
    ymax        = kw.get('ymax',thetamax)
    ngridtheta  = kw.get('ngridtheta',180)
    ny          = kw.get('ny',ngridtheta)

    p           = kw.get('p',1.5)        # power law index
    gamma       = kw.get('gamma',1.125)
    mass        = kw.get('mass', 3e-6)    # Disk mass in Msun (gas mass)
#     Tevap       = kw.get('Tevap',30)
#     fraction    = kw.get('fraction',0.01)
    luminosity  = kw.get('luminosity',1.) # luminosity of central object (units of Lsun)
    temperature = kw.get('temperature',4000.) # temperature of central object (K)
#     spacing     = kw.get('spacing','log')
    opac        = kw.get('opac','oh5')
#     molecules   = kw.get('molecules','c18o')
    gas2dust    = kw.get('gas2dust',100.)
    modeldir    = kw.get('modeldir','.')
    
    # ------- attributes
    self.modelname  = 'PPDisk'
    self.xmin       = xmin
    self.xmax       = xmax
    self.ymin       = ymin
    self.ymax       = ymax
    self.p          = p
    self.gamma      = gamma
    self.mass       = mass
    self.nx         = nx
    self.ny         = ny
    self.opac       = opac
    self.modeldir   = modeldir
    self.gas2dust   = gas2dust
    
    # ------- Stellar Parameters. Need to be there for Radmc3d object
    self.sic              = np.array([0])
    self.onList           = np.array([True])
    self.positionList     = np.array([[0,0,0]],dtype=np.float)
    self.massList         = np.array([0.])
    self.logdmdtList      = np.array([-99.])
    self.ageList          = np.array([0.])
    self.luminosityList   = np.array([luminosity])
    self.temperatureList  = np.array([temperature])
    
    self.minx, self.maxx = -xmax*co.au2cm, xmax*co.au2cm
    self.miny, self.maxy = -xmax*co.au2cm, xmax*co.au2cm
    self.minz, self.maxz = -xmax*np.cos(ymin*np.pi/180.)*co.au2cm, -xmax*np.cos(ymax*np.pi/180.)*co.au2cm
    
    self.gridx = np.logspace(np.log10(self.xmin*co.au2cm),np.log10(self.xmax*co.au2cm),num=self.nx+1)
    self.gridy = np.linspace(self.ymin*np.pi/180.,self.ymax*np.pi/180.,num=self.ny+1)
    
    self.xc    = np.interp(np.arange(self.nx)+0.5,np.arange(self.nx+1),self.gridx)
    self.yc    = np.interp(np.arange(self.ny)+0.5,np.arange(self.ny+1),self.gridy)
    
    self.grid_size = np.median(np.diff(self.gridx))
    
    self.extraheader = [('rin'    ,rin         , "Inner radius in AU"),\
                       ('rout'    ,rout        , "Outer radius in AU"),\
                       ('thetamin',ymin        , "Minimum theta in degree"),\
                       ('thetamax',ymax        , "Maximum theta in degree"),\
                       ('p'       ,p           , "Power law index"),\
                       ('gamma'   ,gamma       , "Gamma index"),\
                       ('mass'    ,mass        , "Model mass in Msun"),\
                       ('nx'      ,nx          , "Number of grid points"),\
                       ('ny'      ,ny          , "Number of grid points"),\
                       ('temp'    ,temperature , "Stellar Temperature in Kelvin"),\
                       ('lum'     ,luminosity  , "Stellar Luminosity measured in Lsun"),\
                       ('gridsize',self.grid_size*co.cm2au ,"Grid size in AU")]

  def make_grid(self):
    """
    set grid variables to be put into amr_grid.inp or amr_grid.binp
    """
    
    # ------- Grid parameters for amr_grid.inp or *.binp
    self.gridstyle   = 0 # gridstyle: 0 = Regular
    self.coordsystem = 100 # coordinate system: 100 <= spherical < 200
    self.include_x, self.include_y, self.include_z = 1, 1, 0
    
    self.ngridx = self.nx
    self.ngridy = self.ny
    self.ngridz = 1
    
    self.gridx = self.gridx # I know stupid, but good for remembering that you always need this
    self.gridy = self.gridy
    self.gridz = np.array([0, 0], dtype=np.float)

  def get_dust_density(self):
    """
    return dust densities in g/cm^3
    """
    
    return self.get_gas_density()/self.gas2dust
  
  def get_gas_density(self):
    """
    return gas density in g/cm^3
    """
    
    floor = 1.e-99
    s0 = 100. # AU
    z0 = 10.  # AU
    
    s = np.abs(self.xc[np.newaxis,:] * np.sin(self.yc[:,np.newaxis]))
    z = -self.xc[np.newaxis,:] * np.cos(self.yc[:,np.newaxis])
    
    Sigma0 = self.mass * co.msun2g * (2-self.p) / (2 * np.pi * (s0*co.au2cm)**2 * ((self.xmax/s0)**(2-self.p) - (self.xmin/s0)**(2-self.p)))
    Hz     = z0 * co.au2cm * (s/s0/co.au2cm)**self.gamma
    rho    = Sigma0 * (s/s0/co.au2cm)**(-self.p)/(Hz * np.sqrt(2*np.pi)) * np.exp(-0.5 * (z/Hz)**2) 
    rho[rho < floor] = floor
    
#     self.s = s
#     self.z = z
    
    return rho.ravel()
