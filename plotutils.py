#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import numpy as np
import matplotlib.pyplot as mp
import matplotlib.cm as cm
import constants as c

from utils import stellar_flux
from sedroutines import tbol
from math import ceil
from matplotlib.colors import LogNorm, Normalize
from matplotlib.patches import Rectangle, Ellipse
from matplotlib.offsetbox import AnchoredOffsetbox, AuxTransformBox, VPacker, TextArea, DrawingArea
from matplotlib import rc

#rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
#rc('text', usetex=True)

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def plotimage_core(*args, **kw):
  """
  Description:
    core function for plotting radmc images
  Calling Sequence:
    ax, colorbar = plotimage_core(ax,image,[text],center=True,cmap='jet',hideaxes=False,
                                  fill=True,norm='log',unit_l='au',vmin=None,vmax=None)
  Input:
    ax        :  matplotlib axes instance
    image     :  image dictionary from readimage
    text      :  string giving text to display in plot window
  Keywords:
    center    :  boolean. Show location of star
    cmap      :  matplotlib.cm instance. matplotlib colormap to use. Default jet
    hideaxes  :  hide axes
    fill      :  fill vmin/max under/overflow with vmin/max value
    fontsize  :  fontsize of text
    norm      :  'log' or 'linear'. normalisation of plot
    unit_l    :  'cm' 'au' 'parsec' or 'arcsec'. length unit of axis
    vmin      :  lower cut-off value of data
    vmax      :  upper cut-off value of data
  Returns:
    ax        :  axes instance
    colorbar  :  legend object
  """
  # ------- Arguments
  if len(args) == 2:
    ax = args[0]
    im = args[1]
    text = None
  elif len(args) == 3:
    ax = args[0]
    im = args[1]
    text = args[2]
  else:
    raise IOError("plotutils.py. plotimage_core. Wrong number of arguments")  

  # ------- Keywords
  center    = kw.get('center',True)
  cmap      = kw.get('cmap',cm.cubehelix)
  hideaxes  = kw.get('hideaxes',False)
  fill      = kw.get('fill',True)
  fontsize  = kw.get('fontsize',12)
  norm      = kw.get('norm','log')
  unit_l    = kw.get('unit_l','au')
  vmin      = kw.get('vmin',None)
  vmax      = kw.get('vmax',None)
  sizeau    = kw.get('sizeau',None)
  plotcircle = kw.get('plotcircle',0)
  
  # ------- vmin/vmax - if not set minimum non-zero and maximum value of data is used
  vmin = im['flux'][np.nonzero(im['flux'])].min() if vmin == None else vmin
  vmax = im['flux'].max() if vmax == None else vmax
  
  # ------- Fill - if fill is true replace data outside vmin/vmax range with vmin/vmax
  flux = im['flux'].copy()
  if fill is True:
    index = np.where(flux < vmin)
    flux[index] = vmin
    index = np.where(flux > vmax)
    flux[index] = vmax

  # ------- size of plotting area and units
  sx, sy = im['pixsize']
  extent = np.array([im['xrange'][0]-sx/2,im['xrange'][-1]+sx/2,im['yrange'][0]-sy/2,im['xrange'][-1]+sy/2])
  assert unit_l in ['cm','au','parsec','arcsec'], "unit_l has to be in cm, au, parsec or arcsec. It's %s" % unit_l
  if unit_l == 'parsec':
    extent *= c.cm2pc
    plotcircle = plotcircle*c.au2pc if plotcircle > 0 else plotcircle
  elif unit_l == 'au':
    extent *= c.cm2au
  elif unit_l == 'arcsec':
    extent *= c.cm2pc/im['dpc']*c.rad2arcsec
    plotcircle = plotcircle*c.au2pc/im['dpc']*c.rad2arcsec if plotcircle > 0 else plotcircle
  
  # ------- norm and colormap
  norm = LogNorm(vmin=vmin,vmax=vmax) if norm is 'log' else Normalize(vmin=vmin,vmax=vmax)
  
  # ------- plot image
  cb = ax.imshow(flux,extent=extent,origin='lower',norm=norm,cmap=cmap)
  
  if center: #x in centre (marking location of protostar)
    ax.plot(0,0,'k*',markersize=10,markerfacecolor='w')#fillstyle='none')
    ax.axis(extent)
  if text: # print text in upper left corner
    # perhaps consider making this robust in the same fashion as the scalebar and ellipse
    ax.text(0.1,0.9,text,fontsize=fontsize,color='white',transform=ax.transAxes,va='top')
  if plotcircle > 0:
    theta = np.linspace(0,2*np.pi,num=100)
    ax.plot(plotcircle*np.cos(theta),plotcircle*np.sin(theta),'w')
  
  if sizeau is not None:
    factor = np.select(unit_l == np.array(['cm','au','parsec','arcsec']),\
            [c.au2cm,1.,c.au2pc,c.au2pc/im['dpc']*c.rad2arcsec])
    ax.set_xlim(-factor*sizeau/2,factor*sizeau/2)
    ax.set_ylim(-factor*sizeau/2,factor*sizeau/2)
  
  # ------- beam size maker
  if 'beam_fwhm' in im:
    if im['beam_unit'] == 'arcsec':
      beamsize = im['dpc']*im['beam_fwhm']*c.arcsec2rad #beamsize in parsec
    else:
      raise NotImplementedError("the beam unit is not yet implemented")
    if unit_l == 'parsec':
      beamsize *= 1.
    elif unit_l == 'au':
      beamsize *= c.pc2au #parsec to au
    elif unit_l == 'cm':
      beamsize *= c.pc2cm
    elif unit_l == 'arcsec':
      beamsize = im['beam_fwhm']
    
    add_ellipse(ax,beamsize,color='white') # add ellipse
  
  # ------- add scale bar
  if unit_l == 'arcsec':
    arcsec2au = im['dpc']
    add_sizebar(ax,factor=arcsec2au,color='white')
  
  if hideaxes:
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)

  return ax, cb
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def plotimage(images, **kw):
  """
  Description:
    Function for plotting radmc images
  Calling Sequence:
    void = plotimage(images,extension='pdf',unit_l='au',grid=None,hideaxes=False,width=30,
                     height=None,colorbar='vertical',filename=None,title=None,text=None,
                     show=False)
  Input:
    images    :  list of pyradmc3d image dictionaries
  Keywords:
    extension :  file extension (pdf og png)
    unit_l    :  Axis units. cm, au, parsec, or arcsec. Default: 'au'
    grid      :  plotting grid (x,y). Automatically setup by default
    hideaxes  :  hide axes. Default: False
    width     :  Figure width in cm. Default: 30
    height    :  Figure height in cm. Auto determined by default
    colorbar  :  None for no colorbar. 'vertical' or 'horizontal' orientation. Default: 'vertical'
    filename  :  Filename. Grapped from image dictionary by default
    title     :  Figure Title. Default: None
    text      :  List of strings for text for each panel. Default: None
    show      :  Show plot instead of making a file. Default: False
  Returns:
    Nothing
  """
  
  # ------- Keywords
  extension = kw.get('extension','pdf')     # extension of file (pdf or png)
  unit_l    = kw.get('unit_l','au')         # unit for axes (cm, au, parsec, arcsec. default au)
  grid      = kw.get('grid',None)           # subplot grid (x,y)
  hideaxes  = kw.get('hideaxes',False)      # Hide axes
  width     = kw.get('width',30.)           # figure width in cm
  height    = kw.get('height',None)         # figure height in cm
  colorbar  = kw.get('colorbar','vertical') # Plot colorbar (vertical, horizontal)
  filename  = kw.get('filename',None)       # filename
  title     = kw.get('title',None)          # figure title
  text      = kw.get('text', None)          # text for figure
  show      = kw.get('show',False)          # show instead of saving file
  maptype   = kw.get('type','continuum')    # type of image (continuum,moment0,moment1)
  plotcircle = kw.get('plotcircle',None)
  
  # ------- Constants
  fig_aspect = 0.97
  
  # ------- Decide size of plotting grid
  if grid is None:
    if isinstance(images,dict):
      nx, ny = 1, 1
      ntot   = 1
    else:
      ntot   = len(images)
      assert ntot > 1, "if you only plot one map, don't put it in a list"
      if ntot == 2:
        nx, ny = 2, 1
      elif ntot == 3:
        nx, ny = 3, 1
      else:
        nx, ny = 3, int(ceil(ntot/3.))
  else:
    nx, ny = grid
    ntot   = 1 if isinstance(images,dict) else len(images)
    assert nx*ny >= ntot, 'not large enough grid to plot all images %i < %i' % (nx*ny,ntot)
  
  # ------- Size of axes
  imx, imy = images['pixnum'] if ntot == 1 else images[0]['pixnum']
  
  # ------- Are the images convoluted
  if ntot > 1:
    assert all([('beam_fwhm' in image) == ('beam_fwhm' in images[0]) for image in images]), "don't mix convolved and non-convolved images"
    convoluted = 'beam_fwhm' in images[0]
  else:
    convoluted = 'beam_fwhm' in images
  
  # ------- Filename
  if filename is None:
    if ntot == 1:
      filename  = images['filename'].split('.')[0]
    else:
      filename = 'image'
  if len(filename.split('.')) == 1:
    assert extension in ['png','pdf'], "extension %s not in list of allowed extensions, (png, pdf)" % extension
    filename += '.' + extension
  
  # ------- Canvas dimensioning
  img_aspect = imx/imy
  figwidth   = width/c.inch2cm
  aspect     = (fig_aspect)*(img_aspect)*(ny/nx)
  figheight  = figwidth*aspect if height is None else height/c.inch2cm
  
  # ------- Axis units
  if unit_l == 'parsec':
    unit = 'Parsec'
  elif unit_l == 'au':
    unit = 'AU'
  elif unit_l == 'cm':
    unit = 'cm'
  elif unit_l == 'arcsec':
    unit = 'Arcsecond'
  
  # ------- vmax/vmin
  if (ntot > 1) & ('vmin' not in kw):
    vmin = min([image['flux'][np.nonzero(image['flux'])].min() for image in images])
    kw['vmin'] = vmin
  if (ntot > 1) & ('vmax' not in kw):
    vmax = max([image['flux'].max() for image in images])
    kw['vmax'] = vmax
  
  # ------- Figure object
  fig = mp.figure(figsize=(figwidth,figheight))
  fig.subplots_adjust(wspace=0,hspace=0)
  
  # ------- Base axis
  ax0 = fig.add_subplot(1,1,1)
  
  # ------- Start plotting
  if (ntot == 1): # only one figure in window
    args = (ax0, images) if text is None else (ax0, images, text)
    ax0, cb = plotimage_core(*args, **kw)
  else: # several figures in window
    ax0.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
    ax0.set_frame_on(False)
    for i, image in enumerate(images): # several images
      ax = fig.add_subplot(ny,nx,i+1)
      args = (ax, image) if text is None else (ax, image, text[i])
      kw['plotcircle'] = 0 if plotcircle is None else plotcircle[i]
      ax, cb = plotimage_core(*args, **kw)
      if not ((i+1) % nx) == 1:
        ax.set_yticklabels([])
      if i < ntot-nx:
        ax.set_xticklabels([])
  ax0.set_xlabel(unit)
  ax0.set_ylabel(unit)
  if title is not None:
    ax0.set_title(title)
  
  if hideaxes:
    ax0.xaxis.set_visible(False)
    ax0.yaxis.set_visible(False)
  
  # ------- Colorbar
  if colorbar is not None:
    cbar_ax = fig.add_axes([0.95, 0.15, 0.02, 0.7])
    fig.colorbar(cb, cax=cbar_ax, orientation=colorbar)
    if maptype == 'continuum':
      unit = r'Jy beam$^{-1}$' if convoluted is True else r'Jy pixel$^{-1}$'
    elif maptype == 'moment0':
      unit = r'Jy beam$^{-1}$ km s$^{-1}$' if convoluted is True else r'Jy pixel$^{-1}$ km s$^{-1}$'
    elif maptype == 'moment1':
      unit = 'km s$^{-1}$'
    cbar_ax.set_ylabel(unit)
  
  if show is True:
    mp.show()
  else:
    mp.savefig(filename,dpi=300,bbox_inches='tight')
  
  # ------ We're done with the figure. Close it
  mp.close()
  
  return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def plotsed_core(*args, **kw):
  """
  Wrapper for plotspectrum_core
  """
  return plotspectrum_core(*args, **kw)

def plotspectrum_core(*args, **kw):
  """
  core function for plotting spectra/seds
  Calling Sequence:
    plotspectrum_core(ax,spec,[text],top=None,bottom=1e-6,left=None,right=None,
                      fontsize=12,style='k',textcolor='red')
  Input:
    ax        :  matplotlib axes instance
    spec      :  spectrum dictionary from readspectrum, og list of spectrum dictionaries
    text      :  string giving text to display in plot window
  Keywords:
    top       :  Top y limit
    bottom    :  Bottom y limit
    left      :  Left x limit
    right     :  Right x limit
    fontsize  :  fontsize of text in window
    style     :  linestyle or list of linestyles
    textcolor :  color of text
  Returns:
    ax        :  axes instance
    legend    :  legend object
  """
  # ------- Arguments
  if len(args) == 2:
    ax   = args[0]
    spec = args[1]
    text = None
  elif len(args) == 3:
    ax   = args[0]
    spec = args[1]
    text = args[2]
  else:
    raise IOError("plotutils.py. plotspectrum_core. Wrong number of arguments")  
  # ------- Keywords
  top       = kw.get('top',None)         # top limit
  bottom    = kw.get('bottom',1e-6)      # lower limit
  left      = kw.get('left',None)        # left limit
  right     = kw.get('right',None)       # right limit
  fontsize  = kw.get('fontsize',12)      # font size
  style     = kw.get('style','k')        # linestyle or list of linestyles
  plotstar  = kw.get('plotstar',True)     # plot stellar sed
  textcolor = kw.get('textcolor','black')# textcolor
  
  # ------- plot SED(s)
  if isinstance(spec,list): # plot several spectra in one window
    ls = []
    if not isinstance(style,list):
      style = [style] * len(spec)
    for i in range(len(spec)):
      aspec, astyle = spec[i], style[i]
      lls, = ax.plot(aspec['lambda'],aspec['flux'],astyle)
      ls.append(lls)
  else: # one spectrum in window
    ls, = ax.plot(spec['lambda'],spec['flux'],style)
  if plotstar is True:
    if isinstance(spec,list):
      s = spec[0]
      style = style[0]
    else:
      s = spec
    temp, lum, dpc = s['temperature'], s['luminosity'], s['dpc']
    if any([temp,lum,dpc]) is None:
      print "Warning plotspectrum_core. I don't have temperature and/or luminosity and/or distance information for spectrum. I will not plot stellar spectrum."
    else:
      ax.plot(s['lambda'],stellar_flux(s['lambda'],temp,luminosity=lum,dpc=dpc),style+'--')
  
  # ------- set scales and limits
  ax.set_xscale('log')
  ax.set_yscale('log')
  ax.set_ylim(bottom=bottom,top=top)
  ax.set_xlim(left=left,right=right)
  # ------- text
  if text: # print text in upper left corner
    ax.text(0.1,0.9,text,fontsize=fontsize,color=textcolor,transform=ax.transAxes,va='top')

  return ax, ls
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def plotsed(*args, **kw):
  """
  Wrapper for plotspectrum
  """
  return plotspectrum(*args, **kw)

def plotspectrum(spectra, **kw):
  """
  plot radmc3d spectrum/sed
  """
  # ------- Handle Keywords
  grid     = kw.get('grid',(1,1))          # subplot grid (x,y)
  width    = kw.get('width',15.)           # figure width in cm
  height   = kw.get('height',None)         # figure height in cm
  filename = kw.get('filename','spectrum.pdf')# filename
  title    = kw.get('title',None)          # figure title
  text     = kw.get('text', None)          # text for figure
  legend   = kw.get('legend',None)         # legend text
  png      = kw.get('png',False)           # Save png (default pdf)
  show     = kw.get('show',False)          # show plot instead of saving file
  linespec = kw.get('linespec',False)
  
  # ------- Constants
  fig_aspect = 0.97
  inch2cm    = 2.54
  cm2au      = 6.68458712e-14
  cm2pc      = 3.24077929e-19
  
  # ------- Setup grid of plots
  nx, ny   = grid
  ntot     = nx*ny
  
  # ------- Filename
  ext = '.png' if png is True else '.pdf'
  if len(filename.split('.')) == 1:
    filename += ext
  
  # ------- Canvas dimensioning
  img_aspect = 0.618 # golden ratio
  figwidth   = width/inch2cm
  aspect     = (fig_aspect)*(img_aspect)*(ny/nx)
  figheight  = figwidth*aspect if height is None else height/inch2cm
  
  # ------- top
  if (ntot > 1) & ('top' not in kw):
    top = max([spectrum['flux'].max() for spectrum in spectra])
    kw['top'] = top
  
  # ------- Figure object
  fig = mp.figure(figsize=(figwidth,figheight))
  fig.subplots_adjust(wspace=0.1,hspace=0.1)
  
  # ------- Base axis
  ax0 = fig.add_subplot(1,1,1)
  
  # ------- Start plotting
  if (ntot == 1): # one plot
    args = (ax0, spectra) if text is None else (ax0, spectra, text)
    if linespec:
      ax0, ls = plotLineSpectrumCore(*args, **kw)
    else:
      ax0, ls = plotspectrum_core(*args, **kw)
  else: # several subplots
    ax0.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
    ax0.set_frame_on(False)
    for i, spectrum in enumerate(spectra): # several spectra
      ax   = fig.add_subplot(ny,nx,i+1)
      args = (ax, spectrum) if text is None else (ax, spectrum, text[i])
      if linespec:
        ax, ls = plotLineSpectrumCore(*args, **kw)
      else:
        ax, ls = plotspectrum_core(*args, **kw)
      handles, labels = ax.get_legend_handles_labels()
      if not ((i+1) % nx == 1) and nx > 1:
        ax.set_yticklabels([])
      if i < ntot-nx:
        ax.set_xticklabels([])
  if linespec:
    ax0.set_xlabel(r'Velocity (km s$^{-1}$)')
    if 'normalise' in kw:
      if kw['normalise'] is True:
        ax0.set_ylabel(r'I/I$_\mathrm{max}$')
    else:
      ax0.set_ylabel(r'F$_\nu$ (Jy)')
  else:
    ax0.set_xlabel(r'Wavelength ($\mu$m)')
    ax0.set_ylabel(r'F$_\nu$ (Jy)')
  if title is not None:
    ax0.set_title(title)
  
  # ------- Add legend?
  if legend is not None:
    ax0.legend(ls,legend,loc='upper left',bbox_to_anchor=(1.,1.))
  
  # ------- Show or Save
  if show is True:
    mp.show()
  else:
    mp.savefig(filename,dpi=300,bbox_inches='tight')
  
  # ------ We're done with the figure. Close it
  mp.close()
  
  return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def plotspectrum_super(spectra,**kw):
  """
  plot radmc3d spectrum/sed with Tbol and stellar flux
  """
  # ------- Handle Keywords
  temperature = kw.get('temp',None)
  luminosity  = kw.get('lum',None)
  dpc         = kw.get('dpc',None)
  lsublbol    = kw.get('lsublbol',False)
  
  if isinstance(spectra,list):
    text = []
    for i,spectrum in enumerate(spectra):
      sflux = stellar_flux(spectrum['lambda'],temperature,luminosity=luminosity,dpc=dpc)
      spectra[i] = [spectrum,{'lambda':spectrum['lambda'],'flux':sflux}]
      if lsublbol is True:
        text.append('LsubLbol = %.0f' % spectrum['cumsed'](350)/spectrum['cumsed'](spectrum['lambda'][0]))
      else:
        text.append('Tbol = %.0f' % spectrum['Tbol'])
  else:
    sflux   = stellar_flux(spectra['lambda'],temperature,luminosity=luminosity,dpc=dpc)
    if lsublbol is True:
      text    = 'Lsub/Lbol = %.3f' % (spectra['cumsed'](350)/spectra['cumsed'](spectra['lambda'][0]))
    else:
      text    = 'Tbol = %.0f' % spectra['Tbol']
    spectra = [spectra,{'lambda':spectra['lambda'],'flux':sflux}]
  
  kw['text'] = text
  kw['style'] = ['k', 'k--']
  
  return plotspectrum(spectra,**kw)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def plotLineSpectrumCore(*args, **kw):
  """
  core function for plotting line spectrum
  Calling Sequence:
    plotLineSpectrumCore(ax,cube,[text],**keywords)
  Input:
    ax        :  matplotlib axes instance
    spec      :  spectrum dictionary from readspectrum, og list of spectrum dictionaries
    text      :  string giving text to display in plot window
  Keywords:
    top       :  Top y limit
    bottom    :  Bottom y limit
    left      :  Left x limit
    right     :  Right x limit
    fontsize  :  fontsize of text in window
    style     :  linestyle or list of linestyles
    normalise :  normalise to max value (default: False)
    textcolor :  color of text
  Returns:
    ax        :  axes instance
    legend    :  legend object
  """
  # ------- Arguments
  if len(args) == 2:
    ax   = args[0]
    spec = args[1]
    text = None
  elif len(args) == 3:
    ax   = args[0]
    spec = args[1]
    text = args[2]
  else:
    raise IOError("plotutils.py. plotLineSpectrumCore. Wrong number of arguments")  
  # ------- Keywords
  top       = kw.get('top',None)         # top limit
  bottom    = kw.get('bottom',0)         # lower limit
  left      = kw.get('left',None)        # left limit
  right     = kw.get('right',None)       # right limit
  fontsize  = kw.get('fontsize',12)      # font size
  style     = kw.get('style','k')        # linestyle or list of linestyles
  normalise = kw.get('normalise',False)  # normalise to max value
  textcolor = kw.get('textcolor','black')# textcolor
  
  # ------- plot SED(s)
  if isinstance(spec,list): # plot several spectra in one window
    ls = []
    if not isinstance(style,list):
      style = [style] * len(spec)
    for i in range(len(spec)):
      aspec, astyle = spec[i], style[i]
      if normalise:
        lls, = ax.step(aspec['zrange'],aspec['flux'].sum(axis=(1,2))/aspec['flux'].sum(axis=(1,2)).max(),astyle,where='mid')
      else:
        lls, = ax.step(aspec['zrange'],aspec['flux'].sum(axis=(1,2)),astyle,where='mid')
      ls.append(lls)
  else: # one spectrum in window
    if normalise:
      ls, = ax.step(spec['zrange'],spec['flux'].sum(axis=(1,2))/spec['flux'].sum(axis=(1,2)).max(),style,where='mid')
    else:
      ls, = ax.step(spec['zrange'],spec['flux'].sum(axis=(1,2)),style,where='mid')
  
  # ------- set scales and limits
  ax.set_ylim(bottom=bottom,top=top)
  ax.set_xlim(left=left,right=right)
  # ------- text
  if text: # print text in upper left corner
    ax.text(0.1,0.9,text,fontsize=fontsize,color=textcolor,transform=ax.transAxes,va='top')

  return ax, ls
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#- Matplotlib auxiliaries #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
class AnchoredSizeBar(AnchoredOffsetbox):
  def __init__(self, transform, size, label, loc,
               pad=0.1, borderpad=0.1, sep=2, prop=None, frameon=True, color='white'):
      """
      Draw a horizontal bar with the size in data coordinate of the give axes.
      A label will be drawn underneath (center-aligned).

      pad, borderpad in fraction of the legend font size (or prop)
      sep in points.
      """
      self.size_bar = AuxTransformBox(transform)
      self.size_bar.add_artist(Rectangle((0,0), size, 0, fc="none",edgecolor=color))

      self.txt_label = TextArea(label, minimumdescent=False,textprops=dict(color=color))

      self._box = VPacker(children=[self.txt_label,self.size_bar],
                          align="center",
                          pad=0, sep=sep)

      AnchoredOffsetbox.__init__(self, loc, pad=pad, borderpad=borderpad,
                                 child=self._box,
                                 prop=prop,
                                 frameon=frameon)

def add_sizebar(ax,factor=1.,color='white'):
  """
  add sizebar to axes. It calculates the length scale automatically
  """

  # distance between tick marks in arc second
  def f(axis):
    l = axis.get_majorticklocs()
    return len(l)>1 and (l[1] - l[0])

  dx    = f(ax.xaxis)*factor # distance between tick marks in AU
  dx    = round(dx,-int(np.log10(dx))) # round to nearest "nice number" eg. 10, 20, 200, 3000
  dx    = int(dx) if dx >= 1 else dx
  label = "%i AU" % dx # create nice label string
  dx   /= factor # convert back into arc second

  asb = AnchoredSizeBar(ax.transData, dx, label, loc=4, pad=0.2, borderpad=0.2, sep=5,
                        frameon=False,color=color)
  ax.add_artist(asb)

class AnchoredEllipse(AnchoredOffsetbox):
  def __init__(self, transform, width, height, angle, loc,
               pad=0.5, borderpad=0.4, prop=None, frameon=True, color='white'):
    """
    Draw an ellipse the size in data coordinate of the give axes.

    pad, borderpad in fraction of the legend font size (or prop)
    """
    self._box = AuxTransformBox(transform)
    self.ellipse = Ellipse((0,0), width, height, angle, edgecolor=color, hatch='////',fc='none')
    self._box.add_artist(self.ellipse)

    AnchoredOffsetbox.__init__(self, loc, pad=pad, borderpad=borderpad,
                               child=self._box,
                               prop=prop,
                               frameon=frameon)

def add_ellipse(ax,size,color='white'):
  ae = AnchoredEllipse(ax.transData, width=size, height=size, angle=0.,
                       loc=3, pad=0.2, borderpad=0.2, frameon=False,color=color)
  
  ax.add_artist(ae)
