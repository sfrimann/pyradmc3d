#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from itertools import islice
from shutil import copyfile
from glob import glob

import constants as co

import numpy as np
import os
import re

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- radmc3d.inp -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_radmc3d_inp(modeldir,pardict,mode='new'):
  """
  Function to write radmc3d.inp file
  Input:
    modeldir : radmc3d model directory (type: str)
    pardict  : dictionary of key value pairs for radmc3d.inp file. Values are written to
               the file as lines of the format "key = value"
  Keywords:
    mode     : ['new'|'overwrite'|'update']
               If 'new' old radmc3d.inp file is removed and replaced by new
               If 'overwrite' all old values in radmc3d.inp are overwritten but values
               not in new pardict are kept
               If 'update' all old values in old radmc3d.inp file are kept, and only new
               values in pardict are added
  Returns:
    pardict : Returns pardict with values reflecting those in the new radmc3d.inp file
  """
  
  # ------- if update is True read already existing file and replace new values with old ones
  if mode in ['update','overwrite']:
    old_pardict = read_radmc3d_inp(modeldir)
    if mode == 'update':
      pardict.update(old_pardict) # old_pardict replaces same keys in pardict
    elif mode == 'overwrite':
      old_pardict.update(pardict) # pardict replaces same keys in old_pardict
      pardict = old_pardict
  elif mode != 'new':
    raise ValueError("write_radmc3d_inp() -- mode keyword must be 'new' 'overwrite' or 'update'. It is %s" % mode)
  
  # ------- write radmc3d.inp
  with open(os.path.join(modeldir,'radmc3d.inp'),'w') as f:
    for key in pardict:
      print >> f, key, '=', pardict[key]
  
  return pardict

def read_radmc3d_inp(modeldir):
  """
  Function to read radmc3d.inp file
  Input:
    modeldir : radmc3d model directory (type: str)
  Keywords:
    None
  Returns:
    pardict : dictionary with key value pairs of the content of radmc3d.inp file
              Note that so far I assume that all values in radmc3d.inp file are integers
  """
  
  pardict = {}
  
  with open(os.path.join(modeldir,'radmc3d.inp'),'r') as f:
    for line in f:
      key, value = line.rstrip('\n').split(' = ')
      # Note that most, but not all, possible inputs to radmc3d.inp file are integers.
      # So far I only use input that are integers, but if one adds an input that isn't,
      # we need to add code to catch parameters that aren't integers.
      pardict[key] = int(value)
  
  return pardict

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- dust opacities #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_dust_opacity(modeldir,opac):
  """
  Function to copy dustkappa_*.inp files to model directory and write dustopac.inp
  Input:
    modeldir : radmc3d model directory (type: str)
    opac     : opacities. Must be either a string (for one dust species) or a sequence
               of strings (for multiple dust species). The string should be the path
               to a dustkappa_*.inp file which is then copied to the model directory
  Keywords:
    None
  Returns:
    names    : list of dust species names
  """
    
  # ------- iterate over opacities and copy the dustkappa_*.inp files to the modeldir
  names = []
  for o in opac:
    if not os.path.isfile(o):
      raise ValueError("write_dust_opacity. Could not find opacity file %s" % o)
    _, filename = os.path.split(o)
    copyfile(o,os.path.join(modeldir,filename))
    try:
      names.append(re.search('dustkappa_(.+?).inp',filename).group(1))
    except:
      raise ValueError("write_dust_opacity -- dust opacity file (%s) does not appear to follow dustkapp_*.inp naming convension" % filename)
  
  # ------- Write dustopac.inp
  ndust = len(opac)
  with open(os.path.join(modeldir,'dustopac.inp'),'w') as f:
    print >> f, '2               Format number of this file'
    print >> f, '%s               Nr of dust species' % str(ndust)
    print >> f, '============================================================================'
    for name in names:
      print >> f, '1               Way in which this dust species is read'
      print >> f, '0               0=Thermal grain'
      print >> f, '%s        Extension of name of dustkappa_***.inp file' % name
      print >> f, '----------------------------------------------------------------------------'
  
  return names

def read_dustopac(modeldir):
  """
  Function to read dustopac.inp
  Input:
    modeldir : radmc3d model directory (type: str)
  Keywords:
    None
  Returns:
    names    : list of dust species names
  """
  
  with open(os.path.join(modeldir,'dustopac.inp'),'r') as f:
    iformat      = int(f.readline().rstrip('\n').split()[0]) # format
    ndust        = int(f.readline().rstrip('\n').split()[0]) # number of dust species
    f.readline()
    names = []
    for i in range(ndust):
      inputstyle = int(f.readline().rstrip('\n').split()[0]) # inputstyle
      iquantum   = int(f.readline().rstrip('\n').split()[0]) # iquantum
      name       = f.readline().rstrip('\n').split()[0]      # dust name
      names.append(name)
  
  return names

def readdustkappa(filename):
  """
  read dustkappa_*name*.inp
  """

  # ------- Read file
  with open(filename,mode='r') as f:
    iformat      = int(f.readline().rstrip('\n')) # format
    nlam         = int(f.readline().rstrip('\n')) # number of wavelengths
    if iformat == 1:
      wavelength, kappa_abs = np.genfromtxt(f,unpack=True)
    elif iformat == 2:
      wavelength, kappa_abs, kappa_scat = np.genfromtxt(f,unpack=True)
    elif iformat == 3:
      wavelength, kappa_abs, kappa_scat, g = np.genfromtxt(f,unpack=True)
    else:
      raise ValueError("readdustkappa(). iformat must be equal to 1, 2, or 3. iformat = %i" % iformat)
  
  if iformat == 1:
    return {'lambda':wavelength,'kappa_abs':kappa_abs}
  if iformat == 2:
    return {'lambda':wavelength,'kappa_abs':kappa_abs,'kappa_scat':kappa_scat}
  if iformat == 3:
    return {'lambda':wavelength,'kappa_abs':kappa_abs,'kappa_scat':kappa_scat,'g':g}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- amr_grid.inp and amr_grid.binp #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_amr_grid(modeldir,model,binary=False):
  """
  Function to write amr_grid.inp given model directory and radmc3d model object
  Input:
    modeldir : radmc3d model directory (type: str)
    model    : pyradmc3d model object. Should contain the method 'make_grid' which
               initializes all variables to be written to amr_grid.inp
  Keywords:
    binary   : if true writes a binary file instead of an ascii file (Default: False)
  Returns:
    ndata    : number of grid points
  """
  
  if not hasattr(model,'make_grid'):
    raise AttributeError("write_amr_grid -- model object does not have attribute 'make_grid'")
  
  model.make_grid()
  
  iformat  = 1
  gridinfo = 0
  
  # ------- write amr_grid.inp or .binp
  if binary:
    with open(os.path.join(modeldir,'amr_grid.binp'),'wb') as f:
      np.array(iformat,dtype='i8').tofile(f)           # iformat (typically 1)
      np.array(model.gridstyle,dtype='i8').tofile(f)   # grid style (0=Regular,1=OcTree AMR,10=LayerStyle AMR)
      np.array(model.coordsystem,dtype='i8').tofile(f) # Coordinate system (cartesian < 100,100 <= Spherical < 200, 200 <= Cylindrical < 300)
      np.array(gridinfo,dtype='i8').tofile(f)          # gridinfo (typically 0)
      np.array(model.include_x,dtype='i8').tofile(f)   # include x,y,z coordinate
      np.array(model.include_y,dtype='i8').tofile(f)
      np.array(model.include_z,dtype='i8').tofile(f)
      np.array(model.ngridx,dtype='i8').tofile(f)      # number of (root) grid points in each direction
      np.array(model.ngridy,dtype='i8').tofile(f)
      np.array(model.ngridz,dtype='i8').tofile(f)
      if model.gridstyle == 1:
        np.array(model.level_range,dtype='i8').tofile(f)
        np.array(model.nleaf,dtype='i8').tofile(f)
        np.array(model.nbranch,dtype='i8').tofile(f)
      np.array(model.gridx,dtype='f8').tofile(f)      # root grid
      np.array(model.gridy,dtype='f8').tofile(f)
      np.array(model.gridz,dtype='f8').tofile(f)
      if model.gridstyle == 1:
        np.array(model.amrtree,dtype='i1').tofile(f)
  else:
    with open(os.path.join(modeldir,'amr_grid.inp'),'w') as f:
      print >> f, str(iformat)           # iformat (typically 1)
      print >> f, str(model.gridstyle)   # grid style (0=Regular,1=OcTree AMR,10=LayerStyle AMR)
      print >> f, str(model.coordsystem) # Coordinate system (cartesian < 100,100 <= Spherical < 200, 200 <= Cylindrical < 300)
      print >> f, str(gridinfo)          # gridinfo (typically 0)
      print >> f, str(model.include_x), str(model.include_y), str(model.include_z) # include x,y,z coordinate
      print >> f, model.ngridx, model.ngridy, model.ngridz
      if model.gridstyle == 1:
        print >> f, model.level_range, model.nleaf, model.nbranch
      np.savetxt(f,model.gridx,delimiter=' ')
      np.savetxt(f,model.gridy,delimiter=' ')
      np.savetxt(f,model.gridz,delimiter=' ')
      if model.gridstyle == 1:
        np.savetxt(f,model.amrtree,fmt='%i')
  
  ndata = model.amrtree.size - model.amrtree.sum() if model.gridstyle == 1 else model.ngridx*model.ngridy*model.ngridz
  
  return ndata
  
def read_amr_grid(**kw):
  """
  Read amr_grid.inp or .binp file used by radmc3d
  Input:
    None
  Keywords:
    modeldir   : directory in which to look for file (Default: '.')
    datadir    : alias for modeldir
    base_level : base AMR level (Default: 0)
    full       : if True return both grid, cell sizes, and levels. Otherwise only return
                 grid. (Default: False)
    returnraw  : if True the grid is not build and the raw data from amr_grid.inp or .binp
                 is returned directly
  Returns:
    r          : Array of shape (ndata,3) containing (x,y,z) coordinates
  """
  
  # ------- Keywords
  datadir    = kw.get('datadir','.')      # alias
  modeldir   = kw.get('modeldir',datadir) # directory to look in
  base_level = kw.get('base_level',0)     # base amr_level in cutout
  full       = kw.get('full',False)       # return full outout (coordinates, dx, level)
  returnraw  = kw.get('returnraw',False)  # if True only return raw content of amr_grid.inp or .binp (do not build grid)
  
  modeldir = os.path.abspath(os.path.expanduser(modeldir))
  
  # ------- Determine if we are reading a binary or ascii file. If both are present
  #         the binary file is read
  if os.path.isfile(os.path.join(modeldir,'amr_grid.binp')):
    filename = os.path.join(modeldir,'amr_grid.binp')
    binary = True
  elif os.path.isfile(os.path.join(modeldir,'amr_grid.inp')):
    filename = os.path.join(modeldir,'amr_grid.inp')
    binary = False
  else:
    raise IOError("Cannot find amr_grid.inp or amr_grid.binp in directory %s" % modeldir)
  
  
  # ------- Read file
  if binary:
    with open(filename,mode='rb') as f:
      iformat, gridstyle, coordsystem, gridinfo, incl_x, incl_y, incl_z, nx, ny, nz = np.fromfile(f,dtype='i8',count=10)
      if gridstyle == 1: #OctTree
        levelmax, nleafmax, nbranchmax = np.fromfile(f,dtype='i8',count=3)
      xedge = np.fromfile(f,dtype='f8',count=nx+1)
      yedge = np.fromfile(f,dtype='f8',count=ny+1)
      zedge = np.fromfile(f,dtype='f8',count=nz+1)
      if gridstyle == 1: #OctTree
        amrtree = np.fromfile(f,dtype='i1')
  else:
    with open(filename,mode='r') as f:
      iformat                        = int(f.readline().rstrip('\n')) # typically 1
      gridstyle                      = int(f.readline().rstrip('\n')) # 1 is oct-tree
      coordsystem                    = int(f.readline().rstrip('\n')) # <100 is cartesian
      gridinfo                       = int(f.readline().rstrip('\n')) # typically 0
      incl_x, incl_y, incl_z         = map(int,f.readline().rstrip('\n').split()) # include x y z
      nx, ny, nz                     = map(int,f.readline().rstrip('\n').split()) #number of pixels

      if gridstyle == 1: # OctTree
        levelmax, nleafmax, nbranchmax = map(int,f.readline().rstrip('\n').split())
      xedge   = np.genfromtxt(islice(f,nx+1),unpack=True,dtype=np.float64) # coordinate edges
      yedge   = np.genfromtxt(islice(f,ny+1),unpack=True,dtype=np.float64)
      zedge   = np.genfromtxt(islice(f,nz+1),unpack=True,dtype=np.float64)
      if gridstyle == 1: # OctTree
        amrtree = np.genfromtxt(f,dtype=np.int,unpack=True) # coordiante tree
  
  if returnraw:
    if gridstyle == 1:
      return {'iformat':iformat, 'gridstyle':gridstyle, 'coordsystem':coordsystem,
              'gridinfo':gridinfo, 'incl_x':incl_x, 'incl_y':incl_y, 'incl_z':incl_z,
              'nx':nx, 'ny':ny, 'nz':nz, 'levelmax':levelmax, 'nleafmax':nleafmax,
              'nbranchmax':nbranchmax, 'xedge':xedge, 'yedge':yedge, 'zedge':zedge,
              'amrtree':amrtree}
    else:
      return {'iformat':iformat, 'gridstyle':gridstyle, 'coordsystem':coordsystem,
              'gridinfo':gridinfo, 'incl_x':incl_x, 'incl_y':incl_y, 'incl_z':incl_z,
              'nx':nx, 'ny':ny, 'nz':nz, 'xedge':xedge, 'yedge':yedge, 'zedge':zedge}
  
  if gridstyle == 0:
    x = 0.5*(xedge[1:] + xedge[:-1])
    y = 0.5*(yedge[1:] + yedge[:-1])
    z = 0.5*(zedge[1:] + zedge[:-1])
  
  # ------- Check data
  if iformat != 1:
    raise ValueError("Only iformat == 1 supported at present")
  if gridstyle != 1:
    raise ValueError("Only gridstyle == 1 supported at present")
  if coordsystem >= 100:
    raise ValueError("Only Cartesian coordinate system supported. coordsystem < 100")

  ndata = len(amrtree) - amrtree.sum() # number of data points

  # ------- Initialize arrays
  r     = np.empty((ndata,3),dtype=np.float64)
  dx    = np.empty(ndata,dtype=np.float64)
  level = np.empty(ndata,dtype=np.int)

  # ------- Set up grid
  x = np.interp(np.arange(nx)+0.5,np.arange(nx+1),xedge)
  y = np.interp(np.arange(ny)+0.5,np.arange(ny+1),yedge)
  z = np.interp(np.arange(nz)+0.5,np.arange(nz+1),zedge)

  zz, yy, xx = np.meshgrid(z,y,x,indexing='ij') # base grid

  # ------- positional indices
  pos1 = 0        # beginning index of data
  pos2 = nx*ny*nz # end index of data

  # ------- read base grid into arrays
  r[pos1:pos2,:]   = np.vstack((xx.ravel(),yy.ravel(),zz.ravel())).T
  dx[pos1:pos2]    = np.median(np.diff(x)) # cell length
  level[pos1:pos2] = base_level

  # ------- refinement array
  refine = np.array([[-0.25, 0.25,-0.25, 0.25,-0.25, 0.25,-0.25, 0.25],\
                     [-0.25,-0.25, 0.25, 0.25,-0.25,-0.25, 0.25, 0.25],\
                     [-0.25,-0.25,-0.25,-0.25, 0.25, 0.25, 0.25, 0.25]]).T

  # ------- step through all data. Upon refinement level shift data ahead in array
  # and refine the cell.
  for a in amrtree:
    if a == 0:
      pos1 += 1 # no refinement step ahead
    elif a == 1: # refinement. Shift data ahead and refine cell
      r[pos1+8:pos2+7,:]   = r[pos1+1:pos2,:]
      dx[pos1+8:pos2+7]    = dx[pos1+1:pos2]
      level[pos1+8:pos2+7] = level[pos1+1:pos2]
  
      r[pos1:pos1+8,:]   = r[pos1,:] + refine*dx[pos1]
      dx[pos1:pos1+8]    = dx[pos1]/2
      level[pos1:pos1+8] = level[pos1] + 1
      pos2 += 7 # end data index shifted ahead
    else:
      raise IOError('entries in amrtree can only be 0 or 1. What went wrong? %i' % a)

  assert pos2 == ndata, 'pos2 != ndata. %i != %i. something went wrong' % (pos2,ndata)

  if full is True:
    return r, dx, level
  else:
    return r

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- dust_density.inp and dust_density.binp #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_dust_density(modeldir,model,binary=False):
  """
  Function to write dust_density.inp or .binp given model directory and radmc3d model 
  object
  Input:
    modeldir : radmc3d model directory (type: str)
    model    : pyradmc3d model object. Should contain the method 'get_dust_density' which
               returns dust_density array for dust_density.inp
  Keywords:
    binary   : if true writes a binary file instead of an ascii file (Default: False)
  Returns:
    ndata    : number of cells
    ndust    : number of dust species
  """
  
  if not hasattr(model,'get_dust_density'):
    raise AttributeError("write_dust_density() -- model object does not have attribute 'get_dust_density'")
  
  dust_density = model.get_dust_density()
  shape        = dust_density.shape
  if len(shape) == 1:
    ndust = 1
    ndata = shape[0]
  elif len(shape) == 2:
    ndust        = shape[0]
    ndata        = shape[1]
    dust_density = dust_density.ravel()
  else:
    raise ValueError("write_dust_density() -- dust_density array must have shape (ndust,ndata) or (ndata,)")
    
  iformat  = 1
  bytesize = 8
  
  if binary:
    with open(os.path.join(modeldir,'dust_density.binp'),'wb') as f:
      np.array(iformat,dtype='i8').tofile(f)           # iformat
      np.array(bytesize,dtype='i8').tofile(f)          # bytesize of main data
      np.array(ndata,dtype='i8').tofile(f)             # number of data points
      np.array(ndust,dtype='i8').tofile(f)             # number of dust species
      np.asarray(dust_density,dtype='f8').tofile(f)    # dust densities
  else:
    with open(os.path.join(modeldir,'dust_density.inp'),'w') as f:
      print >> f, str(iformat)        # iformat
      print >> f, ndata               # number of data points
      print >> f, str(ndust)          # number of dust species
      np.savetxt(f,dust_density)      # dust densities
  
  return ndata, ndust

def read_dust_density(**kw):
  """
  Function to read dust_density.inp or .binp 
  object
  Input:
    None
  Keywords:
    modeldir     : radmc3d model directory (type: str)
    datadir      : alias for modeldir
  Returns:
    dust_density : array of dust densities with shape (ndust,ndata) if ndust > 1, or 
                   (ndata,) if ndust == 1
  """
  # ------- Keywords
  datadir    = kw.get('datadir','.')      # alias
  modeldir   = kw.get('modeldir',datadir) # directory to look in
  
  if os.path.isfile(os.path.join(modeldir,'dust_density.binp')):
    filename = os.path.join(modeldir,'dust_density.binp')
    binary = True
  elif os.path.isfile(os.path.join(modeldir,'dust_density.inp')):
    filename = os.path.join(modeldir,'dust_density.inp')
    binary = False
  else:
    raise IOError("read_dust_density() -- Cannot find dust_density.inp or .binp in directory %s" % modeldir)
  
  # ------- Read file
  if binary:
    with open(filename,mode='rb') as f:
      iformat, bytesize, ndata, ndust = np.fromfile(f,dtype='i8',count=4)
      densities = np.fromfile(f,dtype='f%i' % bytesize)
  else:
    with open(filename,mode='r') as f:
      iformat      = int(f.readline().rstrip('\n'))
      ndata        = int(f.readline().rstrip('\n')) #number of data points
      ndust        = int(f.readline().rstrip('\n')) #number of species
      densities    = np.genfromtxt(f,unpack=True)
  
  # ------- Check data
  if iformat != 1:
    raise ValueError("Only iformat == 1 supported at present")
  if ndata*ndust != len(densities):
    raise ValueError("number of data points in file do not match")
  
  return densities.reshape(ndust,ndata)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- dust_temperature.dat or .bdat -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_dust_temperature(dust_temperatures,**kw):
  """
  Function to write dust_temperature.dat or .bdat given model directory and temperature list
  object
  Input:
    modeldir : radmc3d model directory (type: str)
    model    : list of temperatures
  Keywords:
    binary   : if true writes a binary file instead of an ascii file (Default: False)
  Returns:
    None
  """
  
  # ------- Keywords
  datadir    = kw.get('datadir','.')      # alias
  modeldir   = kw.get('modeldir',datadir) # directory to look in
  filename   = kw.get('filename','dust_temperature')
  binary     = kw.get('binary',False)
  ndust      = kw.get('ndust',1)
  
  ndata = int(dust_temperatures.size/ndust)
  
  iformat  = 1
  bytesize = 8
  
  root, ext = os.path.splitext(filename)
  
  if ext == 'bdat':
    binary = True
  elif ext == 'dat':
    binary = False
  elif ext == '':
    pass
  else:
    raise ValueError('wrong Extention on file. Must be .dat or .bdat %s' % filename)
  
  if binary:
    with open(os.path.join(modeldir,root+'.bdat'),'wb') as f:
      np.array(iformat,dtype='i8').tofile(f)           # iformat
      np.array(bytesize,dtype='i8').tofile(f)          # bytesize of main data
      np.array(ndata,dtype='i8').tofile(f)             # number of data points
      np.array(ndust,dtype='i8').tofile(f)             # number of dust species
      np.asarray(dust_temperatures,dtype='f8').tofile(f) # dust temperatures
  else:
    with open(os.path.join(modeldir,root+'.dat'),'w') as f:
      print >> f, str(iformat)        # iformat
      print >> f, ndata               # number of data points
      print >> f, str(ndust)          # number of dust species
      np.savetxt(f,dust_temperatures) # dust temperatures

def read_dust_temperature(**kw):
  """
  Read dust_temperature.dat or dust_temperature.bdat file produced by radmc-3d
  """
  # ------- Keywords
  datadir    = kw.get('datadir','.')      # alias
  modeldir   = kw.get('modeldir',datadir) # directory to look in
  filename   = kw.get('filename','dust_temperature')
  testfile   = kw.get('testfile',False) # test if file is corrupted. Returns True if good
  
  #root, ext = os.path.splitext(filename)
  if filename[-4:] == '.dat':
    root = filename[:-4]
  elif filename[-5:] == '.bdat':
    root = filename[:-5]
  else:
    root = filename
  
  if os.path.isfile(os.path.join(modeldir,root+'.dat')):
    binary = False
    filename = root+'.dat'
  elif os.path.isfile(os.path.join(modeldir,root+'.bdat')):
    binary = True
    filename = root+'.bdat'
  else:
    raise IOError("could not find %s .dat or .bdat" % root)
  
  # ------- Read file
  if binary:
    with open(os.path.join(modeldir,filename),mode='rb') as f:
      iformat, bytesize, ndata, ndust = np.fromfile(f,dtype='i8',count=4)
      if testfile:
        current_pos = f.tell() # current position in bytes
        f.seek(0,2) # go to end of file to determine file size
        file_size = f.tell() # file size in bytes
        valid = (bytesize*ndata*ndust) == (file_size - current_pos)
      else:
        temperatures = np.fromfile(f,dtype='f%i' % bytesize)
  else:
    with open(os.path.join(modeldir,filename),mode='r') as f:
      iformat      = int(f.readline().rstrip('\n'))
      ndata        = int(f.readline().rstrip('\n')) #number of data points
      ndust        = int(f.readline().rstrip('\n')) #number of species
      if testfile:
        for i,l in enumerate(f):
          pass
        valid = ndata*ndust == (i+1)
      else:
        temperatures = np.genfromtxt(f,unpack=True)
  
  if testfile:
    if iformat != 1:
      return False
    else:
      return valid
  
  # ------- Check data
  if iformat != 1:
    raise ValueError("Only iformat == 1 supported at present")
  if ndata*ndust != len(temperatures):
    raise ValueError("number of data points in file do not match")
  
  return temperatures
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- stars.inp -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_stars(modeldir,model,lamb,flux=None):
  """
  Function to write stars.inp file
  Input:
    modeldir : radmc3d model directory (type: str)
    model    : pyradmc3d model object. Should contain the attributes onList, massList,
               temperatureList, luminosityList and positionList containing information
               about the star(s) in the model.
    lamb     : array of wavelenghts. Must correspond to the wavelength grid in
               wavelength_micron.inp
    flux     : array of fluxes for the radiating source. Must correspond to the
               wavelength grid in wavelength_micron.inp. Furthermore, the units of the
               flux must be erg cm**-2 s**-1 Hz**-1.
  Keywords:
    None
  Returns:
    None
  """
  iformat = 2
  nstar   = model.onList.sum()
  nlam    = lamb.size

  if flux is not None:
    assert (flux.size == nlam), "The flux must be given on the same wavelength grid as the opacity!"
  
  with open(os.path.join(modeldir,'stars.inp'),'w') as f:
    print >> f, str(iformat)          # iformat
    print >> f, str(nstar), str(nlam) # nstars nlambda
    for i in xrange(len(model.onList)): # only include stars that are emitting
      if model.onList[i] == True:
        radius = np.sqrt(model.luminosityList[i]) / (model.temperatureList[i]/5778.)**2 * co.rsun2cm
        print >> f, radius, model.massList[i], model.positionList[i,0], model.positionList[i,1], model.positionList[i,2]
    np.savetxt(f, lamb)
    for i in xrange(len(model.onList)):
      if model.onList[i] == True:
        if flux is None:
          print >> f, -model.temperatureList[i]
        else:
          np.savetxt(f, flux)

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- external_source.inp -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_external_source(modeldir,lamb):
  iformat = 2
  nlam    = len(lamb)
  
  data_dir, void = os.path.split(__file__)
  
  # read interstellar radiation field from Black et. al. 1994
  black_1994_isrf = np.loadtxt(os.path.join(data_dir,'datafiles','isrf_black_1994.csv'),delimiter=',')
  lamb_black = co.c_in_um_s/black_1994_isrf[:,0][::-1] # wavelengths black 1994 (converted from frequencies)
  isrf_black = black_1994_isrf[:,1][::-1] # isrf flux black 1994
  
  # interpolate to put on same wavelength grid as wavelength_micron.inp
  isrf_flux = 10**np.interp(np.log10(lamb),np.log10(lamb_black),np.log10(isrf_black))
  
  with open(os.path.join(modeldir,'external_source.inp'),'w') as f:
    print >> f, str(iformat)
    print >> f, str(nlam)
    np.savetxt(f,lamb)
    np.savetxt(f,isrf_flux)

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- aperture_info.inp -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_aperture_info(modeldir,lambdas,radii):
  iformat = 1
  nlam    = len(lambdas)
  
  with open(os.path.join(modeldir,'aperture_info.inp'),'w') as f:
    print >> f, str(iformat)
    print >> f, str(nlam)
    for l,r in zip(lambdas,radii):
      print >> f, l, r
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- lines.inp -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_lines_inp(modeldir,molecules):
  """
  Function to copy molecule_*.inp files to model directory and write lines.inp.
  Note that so far only LTE mode, and molecule_*.inp files in the 'leiden' format are 
  supported.
  Input:
    modeldir  : radmc3d model directory (type: str)
    molecules : Must be either a string (for molecule) or a sequence of strings (for 
                multiple molecules) giving the path to the molecules_*.inp files which are
                then copied to the model directory
  Keywords:
    None
  Returns:
    names    : list molecule names
  """
  iformat = 2
  iduma   = 0
  idumb   = 0
  ncol    = 0 # only LTE so far
  nmol    = len(molecules)
  
  # ------- iterate over molecules and copy the molecule_*.inp files to the modeldir
  names = []
  for molecule in molecules:
    if not os.path.isfile(molecule):
      raise ValueError("write_lines_inp. Could not find molecule file %s" % molecule)
    _, filename = os.path.split(molecule)
    copyfile(molecule,os.path.join(modeldir,filename))
    try:
      names.append(re.search('molecule_(.+?).inp',filename).group(1))
    except:
      raise ValueError("write_lines_inp -- molecule file (%s) does not appear to follow molecule_*.inp naming convension" % filename)
  
  # ------- write lines.inp
  with open(os.path.join(modeldir,'lines.inp'),'w') as f:
    print >> f, str(iformat)
    print >> f, str(nmol)     # number of molecules
    for name in names:
      print >> f, name, 'leiden', str(iduma), str(idumb), str(ncol)
  
  return names

def read_lines_inp(modeldir):
  """
  Function to read lines.inp
  Input:
    modeldir : radmc3d model directory (type: str)
  Keywords:
    None
  Returns:
    mol_list : list of dictionaries containing the data from lines.inp for the individual 
               molecules
  """
  
  mol_list = []
  with open(os.path.join(modeldir,'lines.inp'),'r') as f:
    iformat      = int(f.readline().rstrip('\n'))
    nmol         = int(f.readline().rstrip('\n'))
    for i in range(nmol):
      molname, method, iduma, idumb, ncol = f.readline().rstrip('\n').split()
      iduma, idumb, ncol = int(iduma), int(idumb), int(ncol)
      if ncol > 0:
        colpartner = [f.readline().rstrip('\n') for i in range(ncol)]
        mol_list.append({'molname':molname,'method':method,'iduma':iduma,'idumb':idumb,'ncol':ncol,'colpartner':colpartner})    
      else:
        mol_list.append({'molname':molname,'method':method,'iduma':iduma,'idumb':idumb,'ncol':ncol})
  
  return mol_list
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- numberdens_*molecule*.inp -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_molecule_density(modeldir,mol_name,numberdens,binary=False):
  """
  Function to write numberdens_*.inp og .binp files for radmc3d 
  object
  Input:
    modeldir   : radmc3d model directory (type: str)
    mol_name   : Name of molecule (type: str)
    numberdnes : Array of number densities (particles/cm^3) of molecule
  Keywords:
    binary     : if true writes a binary file instead of an ascii file (Default: False)
  Returns:
    None
  """
  iformat  = 1
  ndata    = numberdens.size
  bytesize = 8
  
  if binary:
    with open(os.path.join(modeldir,'numberdens_%s.binp' % mol_name),'wb') as f:
      np.array(iformat,dtype='i8').tofile(f)
      np.array(bytesize,dtype='i8').tofile(f)
      np.array(ndata,dtype='i8').tofile(f)
      np.array(numberdens,dtype='f8').tofile(f)
  else:
    with open(os.path.join(modeldir,'numberdens_%s.inp' % mol_name),'w') as f:
      print >> f, str(iformat)
      print >> f, str(ndata)
      np.savetxt(f,numberdens)

def read_molecule_density(modeldir,mol_name):
  """
  Function to read numberdens_*.inp or .binp 
  object
  Input:
    modeldir     : radmc3d model directory (type: str)
    mol_name     : Name of molecule (type: str)
  Keywords:
    None
  Returns:
    numberdens   : array of number densities of molecule of shape (ndata,)
  """
  
  if os.path.isfile(os.path.join(modeldir,'numberdens_%s.binp' % mol_name)):
    filename = os.path.join(modeldir,'numberdens_%s.binp' % mol_name)
    binary = True
  elif os.path.isfile(os.path.join(modeldir,'numberdens_%s.inp' % mol_name)):
    filename = os.path.join(modeldir,'numberdens_%s.inp' % mol_name)
    binary = False
  else:
    raise IOError("read_molecule_density() -- Cannot find numberdens_%s.inp or .binp in directory %s" % (mol_name,modeldir))
  
  # ------- Read file
  if binary:
    with open(filename,mode='rb') as f:
      iformat, bytesize, ndata = np.fromfile(f,dtype='i8',count=3)
      numberdens = np.fromfile(f,dtype='f%i' % bytesize)
  else:
    with open(filename,mode='r') as f:
      iformat      = int(f.readline().rstrip('\n'))
      ndata        = int(f.readline().rstrip('\n')) #number of data points
      numberdens   = np.genfromtxt(f,unpack=True)
  
  return numberdens
  
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- gas_velocity.inp #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_gas_velocity(modeldir,model,binary=False):
  """
  Function to write gas_velocity.inp or .binp given model directory and radmc3d model 
  object
  Input:
    modeldir : radmc3d model directory (type: str)
    model    : pyradmc3d model object. Should contain the method 'get_gas_velocity' which
               gives the gas velocity field for gas_velocity.inp or .binp
  Keywords:
    binary   : if true writes a binary file instead of an ascii file (Default: False)
  Returns:
    None
  """
  
  if not hasattr(model,'get_gas_velocity'):
    raise AttributeError("write_gas_velocity() -- model object does not have attribute 'get_gas_velocity'")
  
  gas_velocity = model.get_gas_velocity()
  
  iformat  = 1
  ndata    = gas_velocity.shape[0]
  bytesize = 8
  
  assert gas_velocity.shape == (ndata,3), "write_gas_velocity() -- something wrong with shape. (ndata,3) is expected"
  
  if binary:
    with open(os.path.join(modeldir,'gas_velocity.binp'),'wb') as f:
      np.array(iformat,dtype='i8').tofile(f)
      np.array(bytesize,dtype='i8').tofile(f)
      np.array(ndata,dtype='i8').tofile(f)
      np.array(gas_velocity.ravel(),dtype='f8').tofile(f)
  else:
    with open(os.path.join(modeldir,'gas_velocity.inp'),'w') as f:
      print >> f, str(iformat)
      print >> f, str(ndata)
      np.savetxt(f,gas_velocity)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#- microturbulence.inp -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def write_microturbulence(modeldir,nleaf,vmic,binary=False):
  iformat = 1
  bytesize = 8
  
  vmic_array = np.ones(nleaf,dtype=np.float)*vmic
  
  if binary:
    with open(os.path.join(modeldir,'microturbulence.binp'),'wb') as f:
      np.array(iformat,dtype='i8').tofile(f)
      np.array(bytesize,dtype='i8').tofile(f)
      np.array(nleaf,dtype='i8').tofile(f)
      np.array(vmic_array,dtype='f8').tofile(f)
  else:
    with open(os.path.join(modeldir,'microturbulence.inp'),'w') as f:
      print >> f, str(iformat)
      print >> f, str(nleaf)
      np.savetxt(f,vmic_array)
