#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

import numpy as np
import constants as co

import os

try:
  from astropy.io import fits as pyfits
except:
  import pyfits

from scipy.integrate import simps, cumtrapz
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- TOP LEVEL READING ROUTINES (ASCII FITS COMBI) -#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def readsed(*args,**kw):
  """
  Top level sed reading routine
  """
  # ------- Handle arguments
  if len(args) == 0:
    filename = 'spectrum.fits'
  elif len(args) == 1:
    filename = args[0]
  else:
    raise IOError("readfiles.py. readsed. Wrong number of inputs")
  
  root, extension = os.path.splitext(filename) # check extension of file
  if extension not in ['.out','.fits','']:
    raise ValueError("the extension of the filename should be .out, .fits or empty. It's %s" % extension)
  if extension == '':
    extension = '.fits'
    filename += extension
  if extension == '.fits':
    return readfitssed(filename,**kw)
  elif extension == '.out':
    return readasciised(filename,**kw)

def readspectrum(*args,**kw):
  """
  read spectrum.out produced by radmc3d
  (wrapper for readsed)
  """
  return readsed(*args,**kw)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- READ ASCII FILES #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def readasciised(*args,**kw):
  """
  Read spectrum.out file produced by radmc-3d
  Input:
    filename : filename to be read (default: spectrum.out)
  Keywords:
    dpc      : Distance to object in parsec (default: 150 parsec)
  Returns:
    dictionary with keys:
      nlam   : Number of wavelength points
      lambda : wavelengths in micrometer
      flux   : Fluxes in Jansky
      Tbol   : Bolometric temperature in K
      cumsed : Normalised cumulative integral of SED. Callable. Example: cumsed(350) gives
               the fraction of the flux coming from wavelengts longer than 350 micrometer
      filename : name of original file
      dpc      : distance in parsec to source
  """
  # ------- Handle arguments
  if len(args) == 0:
    filename = 'spectrum.out'
  elif len(args) == 1:
    filename = args[0]
  else:
    raise IOError("readfiles.py. readsed. Wrong number of inputs")
  
  # ------- Keywords
  dpc = kw.get('dpc',150.) #distance to object in parsec
  calc_tracers = kw.get('calc_tracers',True)

  # ------- Sanity checks
  if not os.path.exists(filename):
    raise IOError("%s does not exist or you do not have access to it." % filename)

  # ------- Read file
  f = open(filename,mode='r')
  iformat = int(f.readline().rstrip('\n'))
  nlam    = int(f.readline().rstrip('\n'))
  lambdas, flux = np.genfromtxt(f,unpack=True)
  f.close()
  
  # ------- Check data
  if iformat != 1:
    raise ValueError("Only iformat == 1 supported at present")
    
  # ------- Convert flux to Jansky at reference distance
  flux = flux * 1e23/dpc**2
  
  # ------- Bolometric Temperature, and Cumulative SED
  if calc_tracers:
    Tbol        = tbol(lambdas,flux)
    cumsed      = cumulativesed(lambdas,flux,dpc)
    logLsub     = np.log10(cumsed(350))
    logLbol     = np.log10(cumsed(lambdas[0]))
    logLsubLbol = logLsub - logLbol
    
    return {'nlam':nlam,'lambda':lambdas,'flux':flux,'Tbol':Tbol,'cumsed':cumsed,'logLsubLbol':logLsubLbol,'logLbol':logLbol,\
            'filename':filename,'dpc':dpc}
  else:
    return {'nlam':nlam,'lambda':lambdas,'flux':flux,'filename':filename,'dpc':dpc}

def readasciispectrum(*args,**kw):
  """
  read spectrum.out produced by radmc3d
  (wrapper for readsed)
  """
  return readasciised(*args,**kw)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- READ/WRITE FITS FILES -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def readfitssed(*args,**kw):
  """
  Read radmc3d spectrum fits file
  Input:
    filename : filename to be read (default: spectrum.fits)
  Keywords:
    dpc      : Distance to object in parsec (default: 150 parsec)
  Returns:
    dictionary with keys:
      nlam   : Number of wavelength points
      lambda : wavelengths in micrometer
      flux   : Fluxes in Jansky
      Tbol   : Bolometric temperature in K
      cumsed : Normalised cumulative integral of SED. Callable. Example: cumsed(350) gives
               the fraction of the flux coming from wavelengts longer than 350 micrometer
      filename : name of original file
      dpc      : distance in parsec to source
  """
  # ------- Handle arguments
  if len(args) == 0:
    filename = 'spectrum.fits'
  elif len(args) == 1:
    filename = args[0]
  else:
    raise IOError("readfiles.py. readfitssed. Wrong number of inputs")
  
  # ------- Keywords
  dpc = kw.get('dpc',150.) #distance to object in parsec

  # ------- Read fits file
  hdulist = pyfits.open(filename,memmap=False)
  
  unitkms = True if hdulist[1].header['ttype1'] == 'vel' else False
  
  if unitkms:
    velocities = hdulist[1].data['vel']
  else:
    lambdas = hdulist[1].data['lambda']
  flux    = hdulist[1].data['flux']
  
  temperature = hdulist[0].header['temp1'] if 'temp1' in hdulist[0].header else None
  luminosity  = hdulist[0].header['lum1']  if 'lum1'  in hdulist[0].header else None
  
  nlam    = hdulist[1].header['naxis2']
  flux   *= hdulist[1].header['dpc']**2/dpc**2
  
  # ------- Close fits file
  hdulist.close()
  
  # ------- Bolometric Temperature, and Cumulative SED
  if not unitkms:
    Tbol        = tbol(lambdas,flux)
    cumsed      = cumulativesed(lambdas,flux,dpc)
    logLsub     = np.log10(cumsed(350))
    logLbol     = np.log10(cumsed(lambdas[0]))
    logLsubLbol = logLsub - logLbol
    return {'nlam':nlam,'lambda':lambdas,'flux':flux,'Tbol':Tbol,'cumsed':cumsed,'logLsubLbol':logLsubLbol,'logLbol':logLbol,\
            'filename':filename,'dpc':dpc,'temperature':temperature,'luminosity':luminosity}
  else:
    return {'nlam':nlam,'vel':velocities,'flux':flux,'filename':filename,'dpc':dpc,\
            'temperature':temperature,'luminosity':luminosity}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def writefitssed(sed,**kw):
  """
  Write sed fits file
  """
  # ------- Keywords
  dpc      = kw.get('dpc',150.) #distance to object in parsec
  logfile  = kw.get('logfile','log.fits') # logfile filename
  filename = kw.get('filename',None)
  extrahead = kw.get('extraheader',[])
  unitkms   = kw.get('unitkms',False)
  
  # ------- Sed file
  if isinstance(sed,str):
    if filename is None:
      filename = os.path.splitext(sed)[0] + '.fits'
    sed = readasciised(sed,dpc=dpc,calc_tracers=False)
  elif isinstance(sed,dict):
    dpc = sed['dpc']
  else:
    raise ValueError("writefitssed() * input image must be either string or dictionary")
  
  filename = 'spectrum.fits' if filename is None else filename
  
  # -------- Primary logfile
  if os.path.exists(logfile):
    logfile = pyfits.open(logfile)
    sinklog = logfile[1] if len(logfile) == 2 else None
    logfile = logfile[0]
  else:
    print "writefitsimage() * Warning * Couldn't find logfile %s. Putting bare header into primary HDU" % logfile
    logfile = pyfits.PrimaryHDU(None)
    sinklog = None
  
  fitsimages = [logfile] # start list of fitsimages
  
  # ------- Make table data
  if unitkms:
    median_lambda = np.median(sed['lambda'])
    velocities    = co.c_in_km_s * (np.array(sed['lambda']) - median_lambda)/median_lambda
    c1      = pyfits.Column(name='vel',unit='km/s',format='D',array=velocities)
  else:
    c1      = pyfits.Column(name='lambda',unit='um',format='E',array=sed['lambda'])
  c2        = pyfits.Column(name='flux',unit='Jy',format='D',array=sed['flux'])
  #fitstable = pyfits.new_table([c1, c2])
  fitstable = pyfits.BinTableHDU.from_columns([c1,c2])
  
  # -------- Start writing header
  fitstable.header['FILENAME'] = filename
  
  fitstable.header['DPC'] = sed['dpc']
  fitstable.header.comments['DPC'] = "Distance to source in Parsec"

  # ------- More header
  for header in extrahead:
    if len(header) == 2:
      key, value = header
      fitstable.header[key] = value
    elif len(header) == 3:
      key, value, comment = header
      fitstable.header[key] = value
      fitstable.header.comments[key] = comment
    else:
      raise ValueError("extra header must consist of key, value og key, value, comment.")
    
  fitsimages.append(fitstable)
  
  if sinklog is not None:
    fitsimages.append(sinklog)
  
  # ------- Save file
  hdulist = pyfits.HDUList(fitsimages)
  hdulist.writeto(filename)
  
  return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- SED/SPECTRUM UTILITY FUNCTIONS  -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def tbol(lamb,flux):
  """
  Calculate bolometric Temperature (as defined in Andre et. al. (1993)).
  Takes wavelengths in micron and fluxes in Jy as input.
  """
  # ------- Convert to frequency
  freq = co.c_in_um_s/lamb
  freq_r, flux_r = freq[::-1], flux[::-1]
  
  # ------- Calculate Tbol
  return 1.25e-11 * simps(freq_r*flux_r,x=freq_r) / simps(flux_r,x=freq_r)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def LsmmLbol(lamb,flux):
  """
  Calculate Lsmm/Lbol (as defined in Andre et. al. (1993)).
  Takes wavelengths in micron and fluxes in Jy as input.
  """
  # ------- Convert to frequency
  freq = co.c_in_um_s/lamb
  freq_r, flux_r = freq[::-1], flux[::-1]
  # ------- Integrate
  ci = cumtrapz(flux_r,x=freq_r,initial=0) # integrate
  ci = ci/ci[-1]
  
  cs = cumulativesed(lamb,flux,150.)
  
  return cs(350.)/cs(lamb[0])
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def cumulativesed(lamb,flux,dpc):
  """
  Cumulative SED
  lamb in micro metres
  flux in Jy
  dpc in parsec
  Returned function gives luminosity emitted above said wavelength (in solar luminosities)
  """
  # ------- Convert to frequency
  c    = 2.99792458e14 # um/s
  Lsun = 3.83e33 # erg/s
  freq = c/lamb
  freq_r, flux_r = freq[::-1], 1e-23*flux[::-1] # convert from Jy to erg/s/cm^2/Hz
  # ------- Integrate
  ci = cumtrapz(flux_r,x=freq_r,initial=0) # integrate
  ci = ci[::-1]#/ci[-1]
  Lbol = 4*np.pi*(dpc*co.pc2cm)**2*ci/Lsun
  return lambda x: np.exp(interp1d(np.asarray([np.log(l) if l > 0 else -np.inf for l in lamb]),np.asarray([np.log(Lb) if Lb > 0 else -np.inf for Lb in Lbol]),kind='linear')(np.log(x))) # interpolate. lambda made to 32-bit. 64 gives a singular matrix error for reasons I do not understand
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def integrate_sed(sed):
  """
  Integrate SED to get Lbol in Lsun
  """
  
  # ------- Convert to frequency
  c    = 2.99792458e14 # um/s
  Lsun = 3.83e33 # erg/s
  freq = c/sed['lambda']
  freq_r, flux_r = freq[::-1], 1e-23*sed['flux'][::-1] # convert to erg/s/cm2/Hz
  
  Lbol = 4*np.pi*(sed['dpc']*co.pc2cm)**2*simps(flux_r,x=freq_r)/Lsun
  
  return Lbol
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def fit_blackbody(*args):
  """
  Fitting blackbody to spectrum
  """
  # ------- Handle Arguments
  if len(args) == 1:
    lamb = args[0]['lambda']
    flux = args[0]['flux']
  elif len(args) == 2:
    lamb = args[0]
    flux = args[1]
  else:
    raise IOError("utils.py. fit_blackbody. Wrong number of inputs")
  
  # ------- Guess at Starting Parameters
  c    = 2.99792458e14 # um/s
  freq = c/lamb
  freq_r, flux_r = freq[::-1], flux[::-1]
  A = simps(flux_r,x=freq_r)
  T = tbol(lamb,flux)
  
  # ------- Fit
  popt, pcov = curve_fit(blackbody,lamb,flux,p0=[A,T])
  yfit = blackbody(lamb,popt[0],popt[1])
  return {'lambda':lamb,'flux':yfit,'popt':popt,'pcov':pcov} 