#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

import numpy as np

import os

try:
  from astropy.io import fits as pyfits
except:
  import pyfits

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- TOP LEVEL READING ROUTINES (ASCII FITS COMBI) -#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def readtausurf(*args):
  """
  Top level tausurface reading routine
  """
  # ------- Handle arguments
  if len(args) == 0:
    filename = 'tausurf.out'
  elif len(args) == 1:
    filename = args[0]
  else:
    raise IOError("readfiles.py. readtausurf. Wrong number of inputs")
  
  root, extension = os.path.splitext(filename) # check extension of file
  if extension not in ['.out','.fits','']:
    raise ValueError("the extension of the filename should be .out, .fits or empty. It's %s" % extension)
  if extension == '':
    extension = '.fits'
    filename += extension
  if extension == '.fits':
    return readfitstausurf(filename)
  elif extension == '.out':
    return readasciitausurf(filename)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- READ ASCII FILES #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def readasciitausurf(*args):
  """
  Read image.out file produced by radmc3d for tausurface command
  Input:
    filename : filename to be read (default: image.out)
  Returns:
    dictionary with keys:
      tausurf : distance in cm
      pixnum  : [nx, ny]. Number of pixels in x and y
      pixsize : [sx, sy]. Pixel size in cm
      nlam    : number of wavelengths in image
      lambdas : wavelenghts in image
      xrange  : x coordinates of image in cm
      yrange  : y coordinates of image in cm
      filename : filename of source file
  """
  # ------- Handle arguments
  if len(args) == 0:
    filename = 'image.out'
  elif len(args) == 1:
    filename = args[0]
  else:
    raise IOError("readfiles.py. readasciitausurf. Wrong number of inputs")
    
  # ------- Read file
  f = open(filename,mode='r')
  iformat = int(f.readline().rstrip('\n'))
  nx, ny  = map(int,f.readline().rstrip('\n').split()) #number of pixels
  nlam    = int(f.readline().rstrip('\n')) #number of lambdas
  sx, sy  = map(float,f.readline().rstrip('\n').split()) #pixel size in cm
  lambdas = [float(f.readline().rstrip('\n')) for i in range(nlam)] #lambdas in um
  lambdas = lambdas[0] if nlam == 1 else lambdas
  data    = np.genfromtxt(f,unpack=True)
  f.close()
    
  # ------- Check data
  if iformat != 1:
    raise ValueError("Only iformat == 1 supported at present")
  
  # ------- Reshape data
  data    = np.asarray(data,dtype=np.float)
  data    = np.squeeze(np.reshape(data,(nlam,ny,nx)))
  
  # ------- Write x and y coordinates
  xrange  = sx*(nx-1)
  yrange  = sy*(ny-1)
  xrange  = np.linspace(-xrange/2.,xrange/2.,num=nx)
  yrange  = np.linspace(-yrange/2.,yrange/2.,num=ny)
  
  return {'tausurf':data,'pixnum':[nx,ny],'pixsize':[sx,sy],'nlam':nlam,\
          'lambdas':lambdas,'xrange':xrange,'yrange':yrange,'filename':filename}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#- READ/WRITE FITS FILES -#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def writefitstausurf(image,**kw):
  """
  Write image fits file tausurface
  """
  # ------- Keywords
  logfile  = kw.get('logfile','log.fits') # logfile filename
  filename = kw.get('filename',None)
  extrahead = kw.get('extraheader',[])
  
  # ------- Image file
  if isinstance(image,str):
    if filename is None:
      filename = os.path.splitext(image)[0] + '.fits'
    image = readasciitausurf(image)
  else:
    raise ValueError("writefitstausurf() * input image must be either string or dictionary")
  
  filename = 'tausurf.fits' if filename is None else filename
  
  # -------- Primary logfile
  if os.path.exists(logfile):
    logfile = pyfits.open(logfile)[0]
  else:
    print "writefitstausurf() * Warning * Couldn't find logfile %s. Putting bare header into primary HDU" % logfile
    logfile = pyfits.PrimaryHDU(None)
  
  fitsimages = [logfile] # start list of fitsimages
  nlam = image['nlam']
  
  for i in range(nlam):
    # -------- New fits image
    if nlam == 1:
      fitsimage = pyfits.ImageHDU(image['tausurf'])
    else:
      fitsimage = pyfits.ImageHDU(np.asarray(image['tausurf'][i]))

    # -------- Start writing header
    fitsimage.header['FILENAME'] = filename
    fitsimage.header['BUNIT']  = 'CM'
    fitsimage.header['BZERO']  = 0.
    fitsimage.header['BSCALE'] = 1.

    fitsimage.header['CUNIT1'] = 'cm'
    fitsimage.header['CDELT1'] = image['pixsize'][0]
    fitsimage.header['CRPIX1'] = 1
    fitsimage.header['CRVAL1'] = image['xrange'][0]

    fitsimage.header['CUNIT2'] = 'cm'
    fitsimage.header['CDELT2'] = image['pixsize'][1]
    fitsimage.header['CRPIX2'] = 1
    fitsimage.header['CRVAL2'] = image['yrange'][0]

    fitsimage.header['lambda'] = image['lambdas'] if nlam == 1 else image['lambdas'][i]
    fitsimage.header.comments['lambda'] = "Image wavelength in micrometer"

    # ------- More header
    for header in extrahead:
      if len(header) == 2:
        key, value = header
        fitsimage.header[key] = value
      elif len(header) == 3:
        key, value, comment = header
        fitsimage.header[key] = value
        fitsimage.header.comments[key] = comment
      else:
        raise ValueError("extra header must consist of key, value og key, value, comment.")
    
    fitsimages.append(fitsimage)
  
  # ------- Save file
  hdulist = pyfits.HDUList(fitsimages)
  hdulist.writeto(filename)
  
  return 0