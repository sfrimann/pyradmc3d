#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

import numpy as np
import os
import constants

from scipy.interpolate import griddata
from scipy.integrate import simps
from scipy.optimize import fmin

def temperature_profile(r,opac,luminosity=1.,temperature=1000.):
  """
  Optically thin temperature profile
  """
  
  temps, pmo = planck_mean_opacity(opac)
  
  rstar = np.sqrt(luminosity) / (temperature/5778.)**2 * constants.rsun2cm
  
  def rootFun(tguess,*arg):
    distance = arg[0]
    
    pmo_tstar  = 10**np.interp(np.log10(temperature),np.log10(temps),np.log10(pmo))
    pmo_tguess = 10**np.interp(np.log10(tguess),np.log10(temps),np.log10(pmo))
    
    rt = tguess - np.sqrt(rstar/2./distance/constants.au2cm) * (pmo_tstar/pmo_tguess)**0.25 * temperature
    return rt**2
  
  tdust = np.empty_like(r)
  warn  = np.empty_like(r)
  
  for i,rr in enumerate(r):
    result = fmin(rootFun,40.,args=(rr,),disp=0,full_output=1)
    tdust[i] = result[0]
    warn[i] = result[-1]
  
  return tdust, warn

def planck_mean_opacity(opac):
  """
  Calculate planck mean opacity given opacity dictionary
  """
  
  ofac = 10.
  tempmin, tempmax, ntemp = 10., 10000., 1000
  
  sb_constant = 5.6704e-5 # erg cm-2 s-1 K-4
  
  temps = np.logspace(np.log10(tempmin),np.log10(tempmax),num=ntemp)
  
  lamb      = opac['lambda']
  kappa_abs = opac['kappa_abs']
  
  new_lamb = np.logspace(np.log10(lamb[0]),np.log10(lamb[-1]),num=int(ofac*lamb.size))
  new_freq = constants.c_in_um_s/new_lamb
  new_kappa_abs = 10**np.interp(np.log10(new_lamb),np.log10(lamb),np.log10(kappa_abs))
  
  pmo = []
  for temp in temps:
    planckkappa = planck(new_lamb,temp,jansky=False)*new_kappa_abs
    pmo.append(simps(planckkappa[::-1],new_freq[::-1])/(temp**4*sb_constant/np.pi))
  
  pmo = np.asarray(pmo)
  
  return temps, pmo

def thin_envelope_profile(r,lum=1.,beta=1.7):
  """
  Temperature profile of optically thin envelope from Chandler and Richer (2000)
  r is distance in AU
  lum is luminosity in Lsun
  beta is beta parameter (opacity propto nu^beta)
  """
  q = 2./(4+beta)
  return 60. * (r/(2.e15*constants.m2au))**(-q) * (lum/1.e5)**(q/2.)

def luminosity_model(facc,age,mass,logdmdt,returnTe=False):
  """
  """
  age_in_year = age*constants.s2yr
  mass_in_msun = mass*constants.g2msun
  logdmdt_in_msun_per_year = logdmdt + np.log10(constants.g2msun/constants.s2yr)
  
  radius_in_rsun = 2.5#stellar_radius(mass_in_msun)
  Lphot, Te = pms_luminosity(mass_in_msun,age_in_year)
  Lacc  = 7.8*mass_in_msun/0.25*10**logdmdt_in_msun_per_year/2.5e-6*2.5/radius_in_rsun
  
  if returnTe is True:
    return Lphot + facc*Lacc, Te
  else:
    return Lphot + facc*Lacc

def pms_luminosity(mass_in,age_in):
  """
  Model grid from D'Antona & Mazzitelli (1994).
  Opacities from Alexander (1989)
  Follow Myers (1998) and add 1e5 yr to model ages
  input mass in Msun. Age in yr
  Returns Lphot in Lsun
  """
  
  module_dir, void = os.path.split(__file__)
#   mass, time, loglum = np.loadtxt(os.path.join(pms_dir,'pms_model.txt'),unpack=True)
  mass, logtime, loglum, logTe = [], [], [], []
  m = None
  with open(os.path.join(module_dir,'datafiles','DAntona1998.txt')) as d1:
    for line in d1:
      if line[:8] == '#M/Msun=':
        m = float(line[13:17])
      elif line[0] == '#':
        continue
      else:
        l = line.split()
        mass.append(m)
        logtime.append(float(l[1]))
        loglum.append(float(l[2]))
        logTe.append(float(l[3]))
  with open(os.path.join(module_dir,'datafiles','DAntona1997.txt')) as d1:
    for line in d1:
      if line[:8] == '#M/Msun=':
        m = float(line[12:17])
      elif line[0] == '#':
        continue
      else:
        l = line.split()
        mass.append(m)
        logtime.append(float(l[1]))
        loglum.append(float(l[2]))
        logTe.append(float(l[3]))
  
  mass, logtime, loglum, logTe = np.asarray(mass), np.asarray(logtime), np.asarray(loglum), np.asarray(logTe)
  
  logmass, logtime = np.log10(mass), np.log10(10**logtime+1e5)
  logmass_in, logage_in = np.log10(mass_in), np.log10(age_in)
  
  loglum_out = griddata(np.vstack((logmass,logtime)).T,loglum,(logmass_in,logage_in))
  logTe_out  = griddata(np.vstack((logmass,logtime)).T,logTe,(logmass_in,logage_in))
  
  if np.isfinite(loglum_out).all(): # no points fall outside grid
    return 10**loglum_out, 10**logTe_out
  
  loglum_out[np.isnan(loglum_out)] = griddata(np.vstack((logmass,logtime)).T,loglum,(logmass_in[np.isnan(loglum_out)],logage_in[np.isnan(loglum_out)]),method='nearest')
  logTe_out[np.isnan(logTe_out)] = griddata(np.vstack((logmass,logtime)).T,logTe,(logmass_in[np.isnan(logTe_out)],logage_in[np.isnan(logTe_out)]),method='nearest')
  
  index = np.where(logage_in < logtime.min())
  loglum_out[index] = loglum_out[index] + 5*logage_in[index] - 5*logtime.min()
  
  return 10**loglum_out, 10**logTe_out

def stellar_radius(mass):
  """
  Steller radius as function of mass (in Msun) from Palla and Stahler 1991
  This is a simplified result from Vorobyov and Basu 2010
  """
  if isinstance(mass, (int,float)):
    if mass <= 0.4:
      return 2.5
    elif mass > 0.4 and mass <= 1.:
      return 2.5 + 2.5/0.6*(mass - 0.4)
    else:
      return 5.
  else:
    return np.select([mass <= 0.4,(mass > 0.4) & (mass <= 1.),mass > 1],[2.5,2.5 + 2.5/0.6*(mass - 0.4),5.])
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def stellar_flux(lamb,temp,luminosity=5.,dpc=150.):
  """
  return stellar blackbody
  """
  
  # ------- Constants
  rsun = 2.253962e-8 # sun radius in parsec
  
  rstar = rsun * np.sqrt(luminosity) / (temp/5778.)**2 # stellar radius in parsec
  
  return np.pi*rstar**2/dpc**2 * planck(lamb,temp,jansky=True)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def planck(lamb,temp,jansky=True):
  """
  Planck's Law in cgs
  Input:
    lamb  :  Input wavelengths in micrometer
    temp  :  Temperature in Kelvin
  Keywords:
    jansky  :  True/False. Return flux in Jansky. Else cgs.
  Returns:
    B_nu  :  Planck function in cgs units (erg/(s rad**2 cm**2 Hz)) or Jy
  """
  
  # ------- Constants
  c = 2.99792458e10 # cm/s
  h = 6.6261e-27 # erg s
  k = 1.3807e-16 # erg/K
  
  factor = 1e23 if jansky is True else 1.
  
  return factor * 2*h*c/(1e-4*lamb)**3 / (np.exp(h*c/(1e-4*k*temp*lamb)) - 1)
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-

def opac_file(opac):
  opac_dir, void = os.path.split(__file__)
  if opac == 'oh5':
    opac_string = 'dustkappa_oh5.inp'
    opac_file   = os.path.join(opac_dir,'datafiles',opac_string)
  else:
    raise IOError("utils.py. I don't recognize opacity keyword %s" % opac)
  
  return opac_file
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def _rotcoord(data,axis,inverse=False):
  """
  Rotate coordinates so that new z axis coincides with new axis.
  The coordinate system is orientated so that the new x axis is pointing towards the
  ascending node.
  I use Euler angles. See e.g. description on http://en.wikipedia.org/wiki/Euler_angles
  Calling Sequence:
    _rotcoord(data,axis)
  Input:
    data           :  data coordinates in cartesian coordinatex (ndata,3)
    axis           :  direction of new z axis
  Returns:
    reoriented coordinates
  """
  
  # Euler angles
  
  if len(axis) != 3:
    raise ValueError('New z-axis must contain three elements')
  
  unit_axis = np.asarray(axis)/np.linalg.norm(axis)
  
  alpha, beta, gamma = np.arctan2(unit_axis[0],-unit_axis[1]), np.arccos(unit_axis[2]), 0.
  
  # Trigonometry
  s1, c1 = np.sin(alpha), np.cos(alpha)
  s2, c2 = np.sin(beta) , np.cos(beta)
  s3, c3 = np.sin(gamma), np.cos(gamma)
  
  # Rotation Matrix
  R = np.array([[c1*c3-c2*s1*s3, -c1*s3-c2*c3*s1,  s1*s2 ],\
                [c3*s1+c1*c2*s3,  c1*c2*c3-s1*s3, -c1*s2 ],\
                [s2*s3         ,  c3*s2         ,  c2    ]])
  
  if inverse is True:
    return np.dot(R,data)
  else:
    return np.dot(data,R)